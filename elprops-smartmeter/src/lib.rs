//! ECHONET Lite properties for low-voltage electric energy meters.

use std::fmt;
use std::io;

use anyhow::anyhow;
use thiserror::Error;

use echonet_lite::packet::Object;
use echonet_lite::property::{DecodableProperty, EncodableProperty, PropertyCode};

#[derive(Debug, Error)]
#[error(transparent)]
pub struct PropertyError {
    inner: anyhow::Error,
}

impl PropertyError {
    fn with_message(msg: impl fmt::Display) -> Self {
        Self {
            inner: anyhow!("{}", msg),
        }
    }

    fn with_unexpected_prop_code_message(
        epc: PropertyCode,
        range: impl fmt::Display,
        prop_desc: impl fmt::Display,
    ) -> Self {
        Self::with_message(format_args!(
            "expected property code in range {} for {}, but got {:?}",
            range, prop_desc, epc
        ))
    }

    fn with_value_out_of_range_message(
        v: impl fmt::Display,
        range: impl fmt::Display,
        prop_desc: impl fmt::Display,
    ) -> Self {
        Self::with_message(format_args!(
            "expected property code in range {} for {}, but got {}",
            range, prop_desc, v
        ))
    }

    fn with_different_length_message(
        v: &[u8],
        expected_len: usize,
        prop_desc: impl fmt::Display,
    ) -> Self {
        Self::with_message(format_args!(
            "expected length {} for {}, but got [u8; {}] (data={:02x?})",
            expected_len,
            prop_desc,
            v.len(),
            v
        ))
    }

    fn bail_if_length_is_different(
        v: &[u8],
        expected_len: usize,
        prop_desc: impl fmt::Display,
    ) -> Result<(), Self> {
        if v.len() != expected_len {
            Err(Self::with_different_length_message(
                v,
                expected_len,
                prop_desc,
            ))
        } else {
            Ok(())
        }
    }
}

impl From<io::Error> for PropertyError {
    fn from(e: io::Error) -> Self {
        Self {
            inner: anyhow::Error::new(e).context("failed to write ECHONET Lite property"),
        }
    }
}

/// Returns `true` if the date is valid.
#[must_use]
fn validate_ym1d1(year: u16, month1: u8, mday1: u8) -> bool {
    const MAX_MDAYS: [u8; 12] = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    if !(1..=12).contains(&month1) {
        return false;
    }
    if (month1 != 2) || (mday1 != 29) {
        // Not a valid 02-29 of a leap year.
        return mday1 < MAX_MDAYS[usize::from(month1) - 1];
    }
    if year % 400 == 0 {
        true
    } else if year % 100 == 0 {
        false
    } else {
        year % 4 == 0
    }
}

/// ECHONET property code relevant to smartmeter.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum KnownPropertyCode {
    OperationStatus = 0x80,
    InstallationLocation = 0x81,
    StandardVersion = 0x82,
    FaultStatus = 0x88,
    ManufacturerCode = 0x8A,
    SerialNumber = 0x8D,
    CurrentTime = 0x97,
    CurrentDate = 0x98,
    StatusChangeAnnouncementPropertyMap = 0x9D,
    SetPropertyMap = 0x9E,
    GetPropertyMap = 0x9F,
    CumulativeEnergyCoefficient = 0xD3,
    InstanceList = 0xD5,
    SelfNodeInstanceList = 0xD6,
    CumulativeEnergyEffectiveDigits = 0xD7,
    CumulativeEnergyNormal = 0xE0,
    CumulativeEnergyUnit = 0xE1,
    CumulativeEnergyNormalHistory1 = 0xE2,
    CumulativeEnergyReverse = 0xE3,
    CumulativeEnergyReverseHistory1 = 0xE4,
    CumulativeEnergyHistoryDay1 = 0xE5,
    InstantEnergy = 0xE7,
    InstantCurrents = 0xE8,
    CumulativeEnergyFixedTimeNormal = 0xEA,
    CumulativeEnergyFixedTimeReverse = 0xEB,
    CumulativeEnergyHistory2 = 0xEC,
    CumulativeEnergyHistoryDay2 = 0xED,
}

impl KnownPropertyCode {
    #[must_use]
    pub fn from_generic(v: PropertyCode) -> Option<Self> {
        let code = match v.raw() {
            0x80 => Self::OperationStatus,
            0x81 => Self::InstallationLocation,
            0x82 => Self::StandardVersion,
            0x88 => Self::FaultStatus,
            0x8A => Self::ManufacturerCode,
            0x8D => Self::SerialNumber,
            0x97 => Self::CurrentTime,
            0x98 => Self::CurrentDate,
            0x9D => Self::StatusChangeAnnouncementPropertyMap,
            0x9E => Self::SetPropertyMap,
            0x9F => Self::GetPropertyMap,
            0xD3 => Self::CumulativeEnergyCoefficient,
            0xD5 => Self::InstanceList,
            0xD6 => Self::SelfNodeInstanceList,
            0xD7 => Self::CumulativeEnergyEffectiveDigits,
            0xE0 => Self::CumulativeEnergyNormal,
            0xE1 => Self::CumulativeEnergyUnit,
            0xE2 => Self::CumulativeEnergyNormalHistory1,
            0xE3 => Self::CumulativeEnergyReverse,
            0xE4 => Self::CumulativeEnergyReverseHistory1,
            0xE5 => Self::CumulativeEnergyHistoryDay1,
            0xE7 => Self::InstantEnergy,
            0xE8 => Self::InstantCurrents,
            0xEA => Self::CumulativeEnergyFixedTimeNormal,
            0xEB => Self::CumulativeEnergyFixedTimeReverse,
            0xEC => Self::CumulativeEnergyHistory2,
            0xED => Self::CumulativeEnergyHistoryDay2,
            _ => return None,
        };
        Some(code)
    }

    /// Returns the raw proprety code value.
    #[inline]
    #[must_use]
    pub fn raw(self) -> u8 {
        self as u8
    }

    /// Returns the typed generic proprety code value.
    #[inline]
    #[must_use]
    pub fn to_generic(self) -> PropertyCode {
        PropertyCode::from_raw(self.raw()).expect("valid property code should never be 0")
    }
}

impl PartialEq<PropertyCode> for KnownPropertyCode {
    #[inline]
    fn eq(&self, other: &PropertyCode) -> bool {
        self.raw() == other.raw()
    }
}

impl PartialEq<KnownPropertyCode> for PropertyCode {
    #[inline]
    fn eq(&self, other: &KnownPropertyCode) -> bool {
        self.raw() == other.raw()
    }
}

impl From<KnownPropertyCode> for PropertyCode {
    #[inline]
    fn from(v: KnownPropertyCode) -> Self {
        v.to_generic()
    }
}

/// Supported properties.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum SupportedProperty {
    OperationStatus(OperationStatus),
    CumulativeEnergyCoefficient(CumulativeEnergyCoefficient),
    InstanceList(InstanceList),
    CumulativeEnergyEffectiveDigits(CumulativeEnergyEffectiveDigits),
    CumulativeEnergyNormal(CumulativeEnergyNormal),
    CumulativeEnergyUnit(CumulativeEnergyUnit),
    CumulativeEnergyNormalHistory1(CumulativeEnergyNormalHistory1),
    CumulativeEnergyReverse(CumulativeEnergyReverse),
    CumulativeEnergyReverseHistory1(CumulativeEnergyReverseHistory1),
    CumulativeEnergyHistoryDay1(CumulativeEnergyHistoryDay1),
    InstantEnergy(InstantEnergy),
    InstantCurrents(InstantCurrents),
    CumulativeEnergyFixedTimeNormal(CumulativeEnergyFixedTimeNormal),
    CumulativeEnergyFixedTimeReverse(CumulativeEnergyFixedTimeReverse),
    CumulativeEnergyHistory2(CumulativeEnergyHistory2),
    CumulativeEnergyHistoryDay2(CumulativeEnergyHistoryDay2),
}

macro_rules! impl_enum_variant_conv {
    ($ty:ty, $variant:ident) => {
        impl From<$variant> for $ty {
            #[inline]
            fn from(v: $variant) -> Self {
                Self::$variant(v)
            }
        }
    };
}

impl_enum_variant_conv!(SupportedProperty, OperationStatus);
impl_enum_variant_conv!(SupportedProperty, CumulativeEnergyCoefficient);
impl_enum_variant_conv!(SupportedProperty, InstanceList);
impl_enum_variant_conv!(SupportedProperty, CumulativeEnergyEffectiveDigits);
impl_enum_variant_conv!(SupportedProperty, CumulativeEnergyNormal);
impl_enum_variant_conv!(SupportedProperty, CumulativeEnergyUnit);
impl_enum_variant_conv!(SupportedProperty, CumulativeEnergyNormalHistory1);
impl_enum_variant_conv!(SupportedProperty, CumulativeEnergyReverse);
impl_enum_variant_conv!(SupportedProperty, CumulativeEnergyReverseHistory1);
impl_enum_variant_conv!(SupportedProperty, CumulativeEnergyHistoryDay1);
impl_enum_variant_conv!(SupportedProperty, InstantEnergy);
impl_enum_variant_conv!(SupportedProperty, InstantCurrents);
impl_enum_variant_conv!(SupportedProperty, CumulativeEnergyFixedTimeNormal);
impl_enum_variant_conv!(SupportedProperty, CumulativeEnergyFixedTimeReverse);
impl_enum_variant_conv!(SupportedProperty, CumulativeEnergyHistory2);
impl_enum_variant_conv!(SupportedProperty, CumulativeEnergyHistoryDay2);

impl DecodableProperty for SupportedProperty {
    type Error = PropertyError;

    fn decode_edt(epc: PropertyCode, edt: &[u8]) -> Result<Self, PropertyError> {
        let prop_code = KnownPropertyCode::from_generic(epc).ok_or_else(|| {
            PropertyError::with_message(format_args!("unknown ECHONET property code {epc:?}"))
        })?;
        match prop_code {
            KnownPropertyCode::OperationStatus => {
                OperationStatus::decode_edt(epc, edt).map(Into::into)
            }
            KnownPropertyCode::CumulativeEnergyCoefficient => {
                CumulativeEnergyCoefficient::decode_edt(epc, edt).map(Into::into)
            }
            KnownPropertyCode::InstanceList | KnownPropertyCode::SelfNodeInstanceList => {
                InstanceList::decode_edt(epc, edt).map(Into::into)
            }
            KnownPropertyCode::CumulativeEnergyEffectiveDigits => {
                CumulativeEnergyEffectiveDigits::decode_edt(epc, edt).map(Into::into)
            }
            KnownPropertyCode::CumulativeEnergyNormal => {
                CumulativeEnergyNormal::decode_edt(epc, edt).map(Into::into)
            }
            KnownPropertyCode::CumulativeEnergyUnit => {
                CumulativeEnergyUnit::decode_edt(epc, edt).map(Into::into)
            }
            KnownPropertyCode::CumulativeEnergyNormalHistory1 => {
                CumulativeEnergyNormalHistory1::decode_edt(epc, edt).map(Into::into)
            }
            KnownPropertyCode::CumulativeEnergyReverse => {
                CumulativeEnergyReverse::decode_edt(epc, edt).map(Into::into)
            }
            KnownPropertyCode::CumulativeEnergyReverseHistory1 => {
                CumulativeEnergyReverseHistory1::decode_edt(epc, edt).map(Into::into)
            }
            KnownPropertyCode::CumulativeEnergyHistoryDay1 => {
                CumulativeEnergyHistoryDay1::decode_edt(epc, edt).map(Into::into)
            }
            KnownPropertyCode::InstantEnergy => InstantEnergy::decode_edt(epc, edt).map(Into::into),
            KnownPropertyCode::InstantCurrents => {
                InstantCurrents::decode_edt(epc, edt).map(Into::into)
            }
            KnownPropertyCode::CumulativeEnergyFixedTimeNormal => {
                CumulativeEnergyFixedTimeNormal::decode_edt(epc, edt).map(Into::into)
            }
            KnownPropertyCode::CumulativeEnergyFixedTimeReverse => {
                CumulativeEnergyFixedTimeReverse::decode_edt(epc, edt).map(Into::into)
            }
            KnownPropertyCode::CumulativeEnergyHistory2 => {
                CumulativeEnergyHistory2::decode_edt(epc, edt).map(Into::into)
            }
            KnownPropertyCode::CumulativeEnergyHistoryDay2 => {
                CumulativeEnergyHistoryDay2::decode_edt(epc, edt).map(Into::into)
            }
            _ => Err(PropertyError::with_message(format_args!(
                "unsupported ECHONET property {:?} (code={:?}, edt={:02x?})",
                prop_code, epc, edt
            ))),
        }
    }
}

trait Property: Sized {
    /// The raw value for the ECHONET property code.
    const EPC: u8;
}

/// Measured value, except for cumulative amounts of electric energy.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum MeasuredValue<T> {
    Available(T),
    PositiveOverflow,
    NegativeOverflow,
    NoData,
}

impl<T> MeasuredValue<T> {
    #[must_use]
    pub fn into_available(self) -> Option<T> {
        match self {
            Self::Available(v) => Some(v),
            _ => None,
        }
    }

    pub fn map<U, F>(self, f: F) -> MeasuredValue<U>
    where
        F: FnOnce(T) -> U,
    {
        match self {
            Self::Available(v) => MeasuredValue::Available(f(v)),
            Self::PositiveOverflow => MeasuredValue::PositiveOverflow,
            Self::NegativeOverflow => MeasuredValue::NegativeOverflow,
            Self::NoData => MeasuredValue::NoData,
        }
    }
}

impl MeasuredValue<i16> {
    #[must_use]
    fn from_raw_i16(v: i16) -> Self {
        match v as u16 {
            0x7ffe => Self::NoData,
            0x7fff => Self::PositiveOverflow,
            0x8000 => Self::NegativeOverflow,
            _ => Self::Available(v),
        }
    }
}

/// Raw (unscaled, without coefficient) value for amount of electric energy.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct RawEnergyValue(u32);

impl RawEnergyValue {
    /// Returns the raw value.
    ///
    /// Note that `0xffff_fffe` indicates the value is not present.
    #[inline]
    #[must_use]
    pub fn raw(self) -> u32 {
        self.0
    }

    /// Creates a value with no data.
    #[inline]
    #[must_use]
    const fn no_data() -> Self {
        Self(0xffff_fffe)
    }

    /// Returns the uncorrected value.
    ///
    /// Note that `0xffff_fffe` indicates the value is not present.
    #[inline]
    #[must_use]
    pub fn uncorrected_value(self) -> Option<u32> {
        if self.0 == 0xffff_fffe {
            None
        } else {
            Some(self.0)
        }
    }

    #[must_use]
    pub fn cumulative_to_watt_hour(
        self,
        coefficient: Option<CumulativeEnergyCoefficient>,
        unit: CumulativeEnergyUnit,
        effective_digits: CumulativeEnergyEffectiveDigits,
    ) -> Option<f64> {
        let effective = self.uncorrected_value()? % effective_digits.modulus();
        Some(
            f64::from(effective)
                * f64::from(coefficient.map_or(1, |v| v.value()))
                * unit.scale_to_watt_hour(),
        )
    }

    #[must_use]
    pub fn cumulative_to_joules(
        self,
        coefficient: Option<CumulativeEnergyCoefficient>,
        unit: CumulativeEnergyUnit,
        effective_digits: CumulativeEnergyEffectiveDigits,
    ) -> Option<f64> {
        let effective = self.uncorrected_value()? % effective_digits.modulus();
        Some(
            f64::from(effective)
                * f64::from(coefficient.map_or(1, |v| v.value()))
                * unit.scale_to_joules(),
        )
    }

    #[must_use]
    pub fn to_watt_hour(
        self,
        coefficient: Option<CumulativeEnergyCoefficient>,
        unit: CumulativeEnergyUnit,
    ) -> Option<f64> {
        self.uncorrected_value().map(|uncorrected| {
            f64::from(uncorrected)
                * f64::from(coefficient.map_or(1, |v| v.value()))
                * unit.scale_to_watt_hour()
        })
    }

    #[must_use]
    pub fn to_joules(
        self,
        coefficient: Option<CumulativeEnergyCoefficient>,
        unit: CumulativeEnergyUnit,
    ) -> Option<f64> {
        self.uncorrected_value().map(|uncorrected| {
            f64::from(uncorrected)
                * f64::from(coefficient.map_or(1, |v| v.value()))
                * unit.scale_to_joules()
        })
    }

    fn parse_echonet_edt<T: fmt::Display>(edt: &[u8], prop_desc: T) -> Result<Self, PropertyError> {
        let v = <&[u8; 4]>::try_from(edt)
            .map_err(|_| {
                PropertyError::with_different_length_message(
                    edt,
                    4,
                    format!("raw energy value of {}", prop_desc),
                )
            })
            .copied()
            .map(u32::from_be_bytes)?;

        Self::from_raw(v, format!("raw energy value of {}", prop_desc))
    }

    fn from_raw(raw: u32, prop_desc: impl fmt::Display) -> Result<Self, PropertyError> {
        // `2_147_483_645` is `0x7fff_fffd`.
        if !matches!(raw, 0..=99_999_999 | 0xffff_fffe) {
            return Err(PropertyError::with_value_out_of_range_message(
                raw,
                "0..=99_999_999 | 0xffff_fffe",
                format!("raw energy value of {}", prop_desc),
            ));
        }
        Ok(Self(raw))
    }
}

/// Operation status.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct OperationStatus(u8);

impl OperationStatus {
    /// Returns the operation status.
    #[inline]
    #[must_use]
    pub fn is_operational(self) -> bool {
        // ON is 0x30, OFF is 0x31.
        self.0 == 0x30
    }
}

impl Property for OperationStatus {
    const EPC: u8 = 0x80;
}

impl DecodableProperty for OperationStatus {
    type Error = PropertyError;

    fn decode_edt(_epc: PropertyCode, edt: &[u8]) -> Result<Self, PropertyError> {
        PropertyError::bail_if_length_is_different(edt, 1, "property `OperationStatus`")?;
        let v = edt[0];
        match v {
            0x30 | 0x31 => Ok(Self(v)),
            _ => Err(PropertyError::with_message(format_args!(
                "unexpected `OperationStatus` value: edt={v:02x?}"
            ))),
        }
    }
}

/// Instance list.
#[derive(Clone, PartialEq, Eq)]
pub struct InstanceList(Vec<[u8; 3]>);

impl fmt::Debug for InstanceList {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_set().entries(self.instances()).finish()
    }
}

impl InstanceList {
    /// Returns the instance list.
    #[inline]
    pub fn instances(&self) -> impl Iterator<Item = Object> + Send + Sync + '_ {
        self.0.iter().copied().map(Object::from)
    }

    /// Returns `true` if the list contains the given instance.
    #[inline]
    #[must_use]
    pub fn contains(&self, instance: Object) -> bool {
        self.0.contains(&instance.to_array())
    }
}

impl Property for InstanceList {
    const EPC: u8 = 0xd5;
}

impl DecodableProperty for InstanceList {
    type Error = PropertyError;

    fn decode_edt(epc: PropertyCode, edt: &[u8]) -> Result<Self, PropertyError> {
        if !matches!(epc.raw(), 0xd5 | 0xd6) {
            return Err(PropertyError::with_unexpected_prop_code_message(
                epc,
                "`0xd5 | 0xd6`",
                "property `InstanceList`",
            ));
        }
        if edt.is_empty() {
            return Err(PropertyError::with_message(format_args!(
                "expected length 3n+1 for property `InstanceList`, but got empty EDT"
            )));
        }
        let num_entries = edt[0];
        PropertyError::bail_if_length_is_different(
            edt,
            usize::from(num_entries) * 3 + 1,
            "property `InstanceList`",
        )?;

        // NOTE: `<&[T]>::array_windows()` is unstable as of Rust 1.62.
        let entries = edt[1..]
            .windows(3)
            .map(|elem| [elem[0], elem[1], elem[2]])
            .collect::<Vec<_>>();

        Ok(Self(entries))
    }
}

/// Coefficient for cumulative amount of electric energy.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct CumulativeEnergyCoefficient(u32);

impl CumulativeEnergyCoefficient {
    /// Returns the coefficient value.
    #[inline]
    #[must_use]
    pub fn value(self) -> u32 {
        self.0
    }
}

impl Property for CumulativeEnergyCoefficient {
    const EPC: u8 = 0xd3;
}

impl DecodableProperty for CumulativeEnergyCoefficient {
    type Error = PropertyError;

    fn decode_edt(_epc: PropertyCode, edt: &[u8]) -> Result<Self, PropertyError> {
        let v = <&[u8; 4]>::try_from(edt)
            .map_err(|_| {
                PropertyError::with_different_length_message(
                    edt,
                    4,
                    "property `CumulativeEnergyCoefficient`",
                )
            })
            .copied()
            .map(u32::from_be_bytes)?;

        if !(0..=999_999).contains(&v) {
            return Err(PropertyError::with_value_out_of_range_message(
                v,
                "`0..=999_999`",
                "property `CumulativeEnergyCoefficient`",
            ));
        }
        Ok(Self(v))
    }
}

/// Number of effective digits for cumulative amount of electric energy.
///
/// Note that the effective digits is for base 10.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct CumulativeEnergyEffectiveDigits(u8);

impl CumulativeEnergyEffectiveDigits {
    /// Returns the effective digits in base 10.
    #[inline]
    #[must_use]
    pub fn value(self) -> u8 {
        self.0
    }

    /// Returns the modulus to get the effective value.
    #[inline]
    #[must_use]
    pub fn modulus(self) -> u32 {
        match self.0 {
            1 => 10,
            2 => 100,
            3 => 1_000,
            4 => 10_000,
            5 => 100_000,
            6 => 1_000_000,
            7 => 10_000_000,
            8 => 100_000_000,
            v => unreachable!(
                "unsupported value {v:#04x} for cumulative energy effective digits must be rejected"
            ),
        }
    }
}

impl Property for CumulativeEnergyEffectiveDigits {
    const EPC: u8 = 0xd7;
}

impl DecodableProperty for CumulativeEnergyEffectiveDigits {
    type Error = PropertyError;

    fn decode_edt(_epc: PropertyCode, edt: &[u8]) -> Result<Self, PropertyError> {
        PropertyError::bail_if_length_is_different(
            edt,
            1,
            "property `CumulativeEnergyEffectiveDigits`",
        )?;
        let v = edt[0];
        match v {
            1..=8 => Ok(Self(v)),
            _ => Err(PropertyError::with_value_out_of_range_message(
                v,
                "`1..=8`",
                "property `CumulativeEnergyEffectiveDigits`",
            )),
        }
    }
}

/// Measured cumulative amount of electric energy (normal direction).
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct CumulativeEnergyNormal(RawEnergyValue);

impl CumulativeEnergyNormal {
    /// Returns the coefficient value.
    #[inline]
    #[must_use]
    pub fn value(self) -> RawEnergyValue {
        self.0
    }

    /// Creates a value with no data.
    #[inline]
    #[must_use]
    const fn no_data() -> Self {
        Self(RawEnergyValue::no_data())
    }

    /// Returns the corrected electric energy in watt hour (Wh).
    #[inline]
    #[must_use]
    pub fn to_watt_hour(
        self,
        coefficient: Option<CumulativeEnergyCoefficient>,
        unit: CumulativeEnergyUnit,
        effective_digits: CumulativeEnergyEffectiveDigits,
    ) -> Option<f64> {
        self.value()
            .cumulative_to_watt_hour(coefficient, unit, effective_digits)
    }

    /// Returns the corrected electric energy in Joules (J).
    #[inline]
    #[must_use]
    pub fn to_joules(
        self,
        coefficient: Option<CumulativeEnergyCoefficient>,
        unit: CumulativeEnergyUnit,
        effective_digits: CumulativeEnergyEffectiveDigits,
    ) -> Option<f64> {
        self.value()
            .cumulative_to_joules(coefficient, unit, effective_digits)
    }
}

impl Property for CumulativeEnergyNormal {
    const EPC: u8 = 0xe0;
}

impl DecodableProperty for CumulativeEnergyNormal {
    type Error = PropertyError;

    fn decode_edt(_epc: PropertyCode, edt: &[u8]) -> Result<Self, PropertyError> {
        let v = RawEnergyValue::parse_echonet_edt(edt, "property `CumulativeEnergyNormal`")?;
        Ok(Self(v))
    }
}

/// Unit for cumulative amounts of electric energy (normal and reverse directions)
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct CumulativeEnergyUnit(u8);

impl CumulativeEnergyUnit {
    /// Returns the raw value.
    #[inline]
    #[must_use]
    pub fn raw(self) -> u8 {
        self.0
    }

    /// Returns the scale to multiply.
    #[must_use]
    pub fn scale_to_kilowatt_hour(self) -> f64 {
        match self.0 {
            0x00 => 1.0,
            0x01 => 0.1,
            0x02 => 0.01,
            0x03 => 0.001,
            0x04 => 0.000_1,
            0x0a => 10.0,
            0x0b => 100.0,
            0x0c => 1_000.0,
            0x0d => 10_000.0,
            v => unreachable!(
                "unsupported value {v:#04x} for cumulative energy unit must be rejected"
            ),
        }
    }

    /// Returns the scale to multiply.
    #[must_use]
    pub fn scale_to_watt_hour(self) -> f64 {
        match self.0 {
            0x00 => 1_000.0,
            0x01 => 100.0,
            0x02 => 10.0,
            0x03 => 1.0,
            0x04 => 0.1,
            0x0a => 10_000.0,
            0x0b => 100_000.0,
            0x0c => 1_000_000.0,
            0x0d => 10_000_000.0,
            v => unreachable!(
                "unsupported value {v:#04x} for cumulative energy unit must be rejected"
            ),
        }
    }

    /// Returns the scale to multiply.
    #[must_use]
    pub fn scale_to_joules(self) -> f64 {
        // 1 [W*h] = 3600 [W*s] = 3600 [J]
        match self.0 {
            0x00 => 3_600_000.0,
            0x01 => 360_000.0,
            0x02 => 36_000.0,
            0x03 => 3_600.0,
            0x04 => 360.0,
            0x0a => 36_000_000.0,
            0x0b => 360_000_000.0,
            0x0c => 3_600_000_000.0,
            0x0d => 36_000_000_000.0,
            v => unreachable!(
                "unsupported value {v:#04x} for cumulative energy unit must be rejected"
            ),
        }
    }
}

impl Property for CumulativeEnergyUnit {
    const EPC: u8 = 0xe1;
}

impl DecodableProperty for CumulativeEnergyUnit {
    type Error = PropertyError;

    fn decode_edt(_epc: PropertyCode, edt: &[u8]) -> Result<Self, PropertyError> {
        PropertyError::bail_if_length_is_different(edt, 1, "property `CumulativeEnergyUnit`")?;
        let v = edt[0];
        if !matches!(v, 0x00..=0x04 | 0x0a..=0x0d) {
            return Err(PropertyError::with_value_out_of_range_message(
                format_args!("{v:#04x}"),
                "`0x00..=0x04 | 0x0a..=0x0d`",
                "property `CumulativeEnergyUnit`",
            ));
        }
        Ok(Self(v))
    }
}

/// Historical data of measured cumulative amounts of electric energy 1 (normal direction).
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CumulativeEnergyNormalHistory1 {
    day: CumulativeEnergyHistoryDay1,
    data: Box<[CumulativeEnergyNormal; 48]>,
}

impl Property for CumulativeEnergyNormalHistory1 {
    const EPC: u8 = 0xe2;
}

impl DecodableProperty for CumulativeEnergyNormalHistory1 {
    type Error = PropertyError;

    fn decode_edt(_epc: PropertyCode, edt: &[u8]) -> Result<Self, PropertyError> {
        PropertyError::bail_if_length_is_different(
            edt,
            194,
            "property `CumulativeEnergyNormalHistory1`",
        )?;

        let day = CumulativeEnergyHistoryDay1::from_raw(
            edt[0],
            "property `CumulativeEnergyNormalHistory1`",
        )?;

        let mut data = Box::new([CumulativeEnergyNormal::no_data(); 48]);
        // NOTE: `<&[T]>::array_windows()` is unstable as of Rust 1.62.
        for (i, elem) in edt[1..].windows(4).enumerate() {
            let elem = RawEnergyValue::from_raw(
                u32::from_be_bytes([elem[0], elem[1], elem[2], elem[3]]),
                format_args!("property `CumulativeEnergyNormalHistory1`"),
            )?;
            let elem = CumulativeEnergyNormal(elem);
            data[i] = elem;
        }

        Ok(Self { day, data })
    }
}

/// Measured cumulative amount of electric energy (reverse direction).
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct CumulativeEnergyReverse(RawEnergyValue);

impl CumulativeEnergyReverse {
    /// Returns the coefficient value.
    #[inline]
    #[must_use]
    pub fn value(self) -> RawEnergyValue {
        self.0
    }

    /// Creates a value with no data.
    #[inline]
    #[must_use]
    const fn no_data() -> Self {
        Self(RawEnergyValue::no_data())
    }

    /// Returns the corrected electric energy in watt hour (Wh).
    #[inline]
    #[must_use]
    pub fn to_watt_hour(
        self,
        coefficient: Option<CumulativeEnergyCoefficient>,
        unit: CumulativeEnergyUnit,
        effective_digits: CumulativeEnergyEffectiveDigits,
    ) -> Option<f64> {
        self.value()
            .cumulative_to_watt_hour(coefficient, unit, effective_digits)
    }

    /// Returns the corrected electric energy in watt hour (Wh).
    #[inline]
    #[must_use]
    pub fn to_joules(
        self,
        coefficient: Option<CumulativeEnergyCoefficient>,
        unit: CumulativeEnergyUnit,
        effective_digits: CumulativeEnergyEffectiveDigits,
    ) -> Option<f64> {
        self.value()
            .cumulative_to_joules(coefficient, unit, effective_digits)
    }
}

impl Property for CumulativeEnergyReverse {
    const EPC: u8 = 0xe3;
}

impl DecodableProperty for CumulativeEnergyReverse {
    type Error = PropertyError;

    fn decode_edt(_epc: PropertyCode, edt: &[u8]) -> Result<Self, PropertyError> {
        let v = RawEnergyValue::parse_echonet_edt(edt, "property `CumulativeEnergyReverse`")?;
        Ok(Self(v))
    }
}

/// Historical data of measured cumulative amounts of electric energy 1 (reverse direction).
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CumulativeEnergyReverseHistory1 {
    day: CumulativeEnergyHistoryDay1,
    data: Box<[CumulativeEnergyReverse; 48]>,
}

impl Property for CumulativeEnergyReverseHistory1 {
    const EPC: u8 = 0xe4;
}

impl DecodableProperty for CumulativeEnergyReverseHistory1 {
    type Error = PropertyError;

    fn decode_edt(_epc: PropertyCode, edt: &[u8]) -> Result<Self, PropertyError> {
        PropertyError::bail_if_length_is_different(
            edt,
            194,
            "property `CumulativeEnergyReverseHistory1`",
        )?;

        let day = CumulativeEnergyHistoryDay1::from_raw(
            edt[0],
            "property `CumulativeEnergyReverseHistory1`",
        )?;

        let mut data = Box::new([CumulativeEnergyReverse::no_data(); 48]);
        // NOTE: `<&[T]>::array_windows()` is unstable as of Rust 1.62.
        for (i, elem) in edt[1..].windows(4).enumerate() {
            let elem = RawEnergyValue::from_raw(
                u32::from_be_bytes([elem[0], elem[1], elem[2], elem[3]]),
                format_args!("property `CumulativeEnergyReverseHistory1`"),
            )?;
            let elem = CumulativeEnergyReverse(elem);
            data[i] = elem;
        }

        Ok(Self { day, data })
    }
}

/// Day for which the historical data of measured cumulative amounts of electric
/// energy is to be derived 1.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct CumulativeEnergyHistoryDay1(u8);

impl CumulativeEnergyHistoryDay1 {
    /// Returns the value.
    ///
    /// `0` means today, `1` means yesterday, `n` means n days before today.
    // `0xff` is the initial value.
    #[inline]
    #[must_use]
    pub fn days_back(self) -> Option<u8> {
        if self.0 != 0xff {
            Some(self.0)
        } else {
            None
        }
    }

    fn from_raw(raw: u8, prop_desc: impl fmt::Display) -> Result<Self, PropertyError> {
        match raw {
            0..=99 | 0xff => Ok(Self(raw)),
            _ => Err(PropertyError::with_value_out_of_range_message(
                raw,
                "`0..=99 | 0xff`",
                prop_desc,
            )),
        }
    }
}

impl Property for CumulativeEnergyHistoryDay1 {
    const EPC: u8 = 0xe5;
}

impl DecodableProperty for CumulativeEnergyHistoryDay1 {
    type Error = PropertyError;

    fn decode_edt(_epc: PropertyCode, edt: &[u8]) -> Result<Self, PropertyError> {
        PropertyError::bail_if_length_is_different(
            edt,
            1,
            "property `CumulativeEnergyHistoryDay1`",
        )?;
        Self::from_raw(edt[0], "property `CumulativeEnergyHistoryDay1`")
    }
}

impl EncodableProperty for CumulativeEnergyHistoryDay1 {
    fn encode<W: io::Write>(&self, writer: &mut W) -> io::Result<usize> {
        writer.write_all(&[Self::EPC, self.0]).map(|()| 1)
    }
}

/// Measured instantaneous electric energy.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct InstantEnergy(i32);

impl InstantEnergy {
    /// Returns the value in watt if available.
    #[inline]
    #[must_use]
    pub fn watt(self) -> Option<i32> {
        if (-2_147_483_647..=2_147_483_645).contains(&self.0) {
            Some(self.0)
        } else {
            None
        }
    }

    /// Returns `true` if the value overflowed to positive.
    #[inline]
    #[must_use]
    pub fn is_overflowed_positive(self) -> bool {
        self.0 as u32 == 0x7fff_ffff
    }

    /// Returns `true` if the value overflowed to negative.
    #[inline]
    #[must_use]
    pub fn is_overflowed_negative(self) -> bool {
        self.0 as u32 == 0x8000_0000
    }

    /// Returns `true` if no data is available.
    #[inline]
    #[must_use]
    pub fn is_no_data(self) -> bool {
        self.0 as u32 == 0x7fff_fffe
    }
}

impl Property for InstantEnergy {
    const EPC: u8 = 0xe7;
}

impl DecodableProperty for InstantEnergy {
    type Error = PropertyError;

    fn decode_edt(_epc: PropertyCode, edt: &[u8]) -> Result<Self, PropertyError> {
        let v = <&[u8; 4]>::try_from(edt)
            .map_err(|_| {
                PropertyError::with_different_length_message(edt, 4, "property `InstantEnergy`")
            })
            .copied()
            .map(i32::from_be_bytes)?;

        // `2_147_483_645` is `0x7fff_fffd`.
        if !(-2_147_483_647..=2_147_483_645).contains(&v) {
            return Err(PropertyError::with_value_out_of_range_message(
                v,
                "`-2_147_483_647..=2_147_483_645`",
                "property `InstantEnergy`",
            ));
        }
        Ok(Self(v))
    }
}

/// Measured instantaneous currents.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct InstantCurrents {
    // Unit is 0.1A.
    // `0x7ffe` for no data.
    // `0x7fff` for positive overflow.
    // `0x8000` for negative overflow.
    r: i16,
    // Unit is 0.1A.
    // `0x7ffe` for no data, including single-phase 2-wire system.
    // `0x7fff` for positive overflow.
    // `0x8000` for negative overflow.
    t: i16,
}

impl InstantCurrents {
    /// Returns the currents for R phase in 0.1A.
    #[inline]
    #[must_use]
    pub fn r_ampere_x10(self) -> MeasuredValue<i16> {
        MeasuredValue::from_raw_i16(self.r)
    }

    /// Returns the currents for R phase in Ampere.
    #[inline]
    #[must_use]
    pub fn r_ampere(self) -> MeasuredValue<f64> {
        self.r_ampere_x10().map(|v| f64::from(v) / 10.0)
    }

    /// Returns the currents for T phase in 0.1A.
    #[inline]
    #[must_use]
    pub fn t_ampere_x10(self) -> MeasuredValue<i16> {
        MeasuredValue::from_raw_i16(self.t)
    }

    /// Returns the currents for T phase in Ampere.
    #[inline]
    #[must_use]
    pub fn t_ampere(self) -> MeasuredValue<f64> {
        self.t_ampere_x10().map(|v| f64::from(v) / 10.0)
    }
}

impl Property for InstantCurrents {
    const EPC: u8 = 0xe8;
}

impl DecodableProperty for InstantCurrents {
    type Error = PropertyError;

    fn decode_edt(_epc: PropertyCode, edt: &[u8]) -> Result<Self, PropertyError> {
        PropertyError::bail_if_length_is_different(edt, 4, "property `InstantCurrents`")?;

        let r = u16::from_be_bytes([edt[0], edt[1]]) as i16;
        let t = u16::from_be_bytes([edt[2], edt[3]]) as i16;

        Ok(Self { r, t })
    }
}

/// Cumulative amounts of electric energy measured at fixed time (normal direction).
#[derive(Clone, Copy, PartialEq, Eq)]
pub struct CumulativeEnergyFixedTimeNormal {
    year: u16,
    month1: u8,
    mday1: u8,
    hour: u8,
    minute: u8,
    second: u8,
    measured: CumulativeEnergyNormal,
}

impl fmt::Debug for CumulativeEnergyFixedTimeNormal {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("CumulativeEnergyFixedTimeNormal")
            .field(
                "date",
                &format_args!("{:04}-{:02}-{:02}", self.year, self.month1, self.mday1),
            )
            .field(
                "time",
                &format_args!("{:02}:{:02}:{:02}", self.hour, self.minute, self.second),
            )
            .field("measured", &self.measured)
            .finish()
    }
}

impl CumulativeEnergyFixedTimeNormal {
    /// Returns the value in watt.
    #[inline]
    #[must_use]
    pub fn measured(&self) -> CumulativeEnergyNormal {
        self.measured
    }

    /// Returns the tuple of year, month1, and mday1.
    #[inline]
    #[must_use]
    pub fn date(&self) -> (u16, u8, u8) {
        (self.year, self.month1, self.mday1)
    }

    /// Returns the tuple of hour, minute, and second.
    #[inline]
    #[must_use]
    pub fn time(&self) -> (u8, u8, u8) {
        (self.hour, self.minute, self.second)
    }

    fn from_raw(
        year: u16,
        month1: u8,
        mday1: u8,
        hour: u8,
        minute: u8,
        second: u8,
        measured: u32,
    ) -> Result<Self, PropertyError> {
        if !validate_ym1d1(year, month1, mday1) {
            return Err(PropertyError::with_message(format_args!(
                "invalid date {:04}-{:02}-{:02} for property `CumulativeEnergyFixedTimeNormal`",
                year, month1, mday1
            )));
        }
        if (hour >= 24) || (minute >= 60) || (second >= 60) {
            return Err(PropertyError::with_message(format_args!(
                "invalid time {:02}:{:02}:{:02} for property `CumulativeEnergyFixedTimeNormal`",
                hour, minute, second
            )));
        }
        let measured = CumulativeEnergyNormal(RawEnergyValue::from_raw(
            measured,
            "property `CumulativeEnergyFixedTimeNormal`",
        )?);

        Ok(Self {
            year,
            month1,
            mday1,
            hour,
            minute,
            second,
            measured,
        })
    }
}

impl Property for CumulativeEnergyFixedTimeNormal {
    const EPC: u8 = 0xea;
}

impl DecodableProperty for CumulativeEnergyFixedTimeNormal {
    type Error = PropertyError;

    fn decode_edt(_epc: PropertyCode, edt: &[u8]) -> Result<Self, PropertyError> {
        PropertyError::bail_if_length_is_different(
            edt,
            11,
            "property `CumulativeEnergyFixedTimeNormal`",
        )?;
        let year = u16::from_be_bytes([edt[0], edt[1]]);
        let month1 = edt[2];
        let mday1 = edt[3];
        let hour = edt[4];
        let minute = edt[5];
        let second = edt[6];
        let measured = u32::from_be_bytes([edt[7], edt[8], edt[9], edt[10]]);

        Self::from_raw(year, month1, mday1, hour, minute, second, measured)
    }
}

/// Cumulative amounts of electric energy measured at fixed time (reverse direction).
#[derive(Clone, Copy, PartialEq, Eq)]
pub struct CumulativeEnergyFixedTimeReverse {
    year: u16,
    month1: u8,
    mday1: u8,
    hour: u8,
    minute: u8,
    second: u8,
    measured: CumulativeEnergyReverse,
}

impl fmt::Debug for CumulativeEnergyFixedTimeReverse {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("CumulativeEnergyFixedTimeReverse")
            .field(
                "date",
                &format_args!("{:04}-{:02}-{:02}", self.year, self.month1, self.mday1),
            )
            .field(
                "time",
                &format_args!("{:02}:{:02}:{:02}", self.hour, self.minute, self.second),
            )
            .field("measured", &self.measured)
            .finish()
    }
}

impl CumulativeEnergyFixedTimeReverse {
    /// Returns the value in watt.
    #[inline]
    #[must_use]
    pub fn measured(&self) -> CumulativeEnergyReverse {
        self.measured
    }

    /// Returns the tuple of year, month1, and mday1.
    #[inline]
    #[must_use]
    pub fn date(&self) -> (u16, u8, u8) {
        (self.year, self.month1, self.mday1)
    }

    /// Returns the tuple of hour, minute, and second.
    #[inline]
    #[must_use]
    pub fn time(&self) -> (u8, u8, u8) {
        (self.hour, self.minute, self.second)
    }

    fn from_raw(
        year: u16,
        month1: u8,
        mday1: u8,
        hour: u8,
        minute: u8,
        second: u8,
        measured: u32,
    ) -> Result<Self, PropertyError> {
        if !validate_ym1d1(year, month1, mday1) {
            return Err(PropertyError::with_message(format_args!(
                "invalid date {:04}-{:02}-{:02} for property `CumulativeEnergyFixedTimeReverse`",
                year, month1, mday1
            )));
        }
        if (hour >= 24) || (minute >= 60) || (second >= 60) {
            return Err(PropertyError::with_message(format_args!(
                "invalid time {:02}:{:02}:{:02} for property `CumulativeEnergyFixedTimeReverse`",
                hour, minute, second
            )));
        }
        let measured = CumulativeEnergyReverse(RawEnergyValue::from_raw(
            measured,
            "property `CumulativeEnergyFixedTimeReverse`",
        )?);

        Ok(Self {
            year,
            month1,
            mday1,
            hour,
            minute,
            second,
            measured,
        })
    }
}

impl Property for CumulativeEnergyFixedTimeReverse {
    const EPC: u8 = 0xeb;
}

impl DecodableProperty for CumulativeEnergyFixedTimeReverse {
    type Error = PropertyError;

    fn decode_edt(_epc: PropertyCode, edt: &[u8]) -> Result<Self, PropertyError> {
        PropertyError::bail_if_length_is_different(
            edt,
            11,
            "property `CumulativeEnergyFixedTimeNormal`",
        )?;
        let year = u16::from_be_bytes([edt[0], edt[1]]);
        let month1 = edt[2];
        let mday1 = edt[3];
        let hour = edt[4];
        let minute = edt[5];
        let second = edt[6];
        let measured = u32::from_be_bytes([edt[7], edt[8], edt[9], edt[10]]);

        Self::from_raw(year, month1, mday1, hour, minute, second, measured)
    }
}

/// Historical data of measured cumulative amounts of electric energy 2
/// (normal and reverse directions).
#[derive(Clone, PartialEq, Eq)]
pub struct CumulativeEnergyHistory2 {
    year: u16,
    month1: u8,
    mday1: u8,
    hour: u8,
    minute: u8,
    entries: Vec<(CumulativeEnergyNormal, CumulativeEnergyReverse)>,
}

impl fmt::Debug for CumulativeEnergyHistory2 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("CumulativeEnergyHistory2")
            .field(
                "date",
                &format_args!("{:04}-{:02}-{:02}", self.year, self.month1, self.mday1),
            )
            .field(
                "time",
                &format_args!("{:02}:{:02}:00", self.hour, self.minute),
            )
            .field("entries", &self.entries)
            .finish()
    }
}

impl Property for CumulativeEnergyHistory2 {
    const EPC: u8 = 0xec;
}

impl DecodableProperty for CumulativeEnergyHistory2 {
    type Error = PropertyError;

    fn decode_edt(_epc: PropertyCode, edt: &[u8]) -> Result<Self, PropertyError> {
        const HEADER_SIZE: usize = 7;
        const ENTRY_SIZE: usize = 8;

        if (edt.len() - HEADER_SIZE) % ENTRY_SIZE != 0 {
            return Err(PropertyError::with_message(
                    format_args!("expected length 8n+7 for property `CumulativeEnergyHistory2`, but got [u8; {}] (data={:02x?})", edt.len(), edt),
                    ));
        }

        let year = u16::from_be_bytes([edt[0], edt[1]]);
        let month1 = edt[2];
        let mday1 = edt[3];
        let hour = edt[4];
        let minute = edt[5];
        let num_entries = edt[6];

        if !validate_ym1d1(year, month1, mday1) {
            return Err(PropertyError::with_message(format_args!(
                "invalid date {:04}-{:02}-{:02} for property `CumulativeEnergyHistory2`",
                year, month1, mday1
            )));
        }
        if (hour >= 24) || !matches!(minute, 0 | 30) {
            return Err(PropertyError::with_message(format_args!(
                "invalid time {:02}:{:02} for property `CumulativeEnergyHistory2`",
                hour, minute
            )));
        }
        if !(1..=12).contains(&num_entries) {
            return Err(PropertyError::with_value_out_of_range_message(
                num_entries,
                "`1..=12`",
                "property `CumulativeEnergyHistory2`",
            ));
        }
        let num_entries = usize::from(num_entries);

        let expected_len = ENTRY_SIZE * num_entries;
        if (edt.len() - HEADER_SIZE) != expected_len {
            PropertyError::bail_if_length_is_different(
                edt,
                expected_len,
                format_args!(
                    "property `CumulativeEnergyHistory2` with num_entries={}",
                    num_entries
                ),
            )?;
        }

        let mut entries = Vec::with_capacity(num_entries);
        // NOTE: `<&[T]>::array_windows()` is unstable as of Rust 1.62.
        for entry in edt[HEADER_SIZE..].windows(8) {
            let energy_normal = CumulativeEnergyNormal(RawEnergyValue::from_raw(
                u32::from_be_bytes([entry[0], entry[1], entry[2], entry[3]]),
                format_args!("property `CumulativeEnergyHistory2`"),
            )?);
            let energy_reverse = CumulativeEnergyReverse(RawEnergyValue::from_raw(
                u32::from_be_bytes([entry[5], entry[6], entry[7], entry[8]]),
                format_args!("property `CumulativeEnergyHistory2`"),
            )?);
            entries.push((energy_normal, energy_reverse));
        }

        Ok(Self {
            year,
            month1,
            mday1,
            hour,
            minute,
            entries,
        })
    }
}

/// Day for which the historical data of measured cumulative amounts of electric
/// energy is to be retrieved 2.
#[derive(Clone, Copy, PartialEq, Eq)]
pub struct CumulativeEnergyHistoryDay2 {
    year: u16,
    month1: u8,
    mday1: u8,
    hour: u8,
    minute: u8,
    num_entries: u8,
}

impl fmt::Debug for CumulativeEnergyHistoryDay2 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("CumulativeEnergyHistory2")
            .field(
                "date",
                &format_args!("{:04}-{:02}-{:02}", self.year, self.month1, self.mday1),
            )
            .field(
                "time",
                &format_args!("{:02}:{:02}:00", self.hour, self.minute),
            )
            .field("num_entries", &self.num_entries)
            .finish()
    }
}

impl CumulativeEnergyHistoryDay2 {
    /// Returns the value in watt.
    #[inline]
    #[must_use]
    pub fn num_entries_raw(&self) -> u8 {
        self.num_entries
    }

    /// Returns the tuple of year, month1, and mday1.
    #[inline]
    #[must_use]
    pub fn date(&self) -> (u16, u8, u8) {
        (self.year, self.month1, self.mday1)
    }

    /// Returns the tuple of hour and minute.
    #[inline]
    #[must_use]
    pub fn time(&self) -> (u8, u8) {
        (self.hour, self.minute)
    }

    fn from_raw(
        year: u16,
        month1: u8,
        mday1: u8,
        hour: u8,
        minute: u8,
        num_entries: u8,
    ) -> Result<Self, PropertyError> {
        if !validate_ym1d1(year, month1, mday1) {
            return Err(PropertyError::with_message(format_args!(
                "invalid date {:04}-{:02}-{:02} for property `CumulativeEnergyHistoryDay2`",
                year, month1, mday1
            )));
        }
        if (hour >= 24) || !matches!(minute, 0 | 30) {
            return Err(PropertyError::with_message(format_args!(
                "invalid time {:02}:{:02} for property `CumulativeEnergyHistoryDay2`",
                hour, minute
            )));
        }
        if !(1..=12).contains(&num_entries) {
            return Err(PropertyError::with_value_out_of_range_message(
                num_entries,
                "`1..=12`",
                "property `CumulativeEnergyHistoryDay2`",
            ));
        }

        Ok(Self {
            year,
            month1,
            mday1,
            hour,
            minute,
            num_entries,
        })
    }
}

impl Property for CumulativeEnergyHistoryDay2 {
    const EPC: u8 = 0xed;
}

impl DecodableProperty for CumulativeEnergyHistoryDay2 {
    type Error = PropertyError;

    fn decode_edt(_epc: PropertyCode, edt: &[u8]) -> Result<Self, PropertyError> {
        PropertyError::bail_if_length_is_different(
            edt,
            7,
            "property `CumulativeEnergyHistoryDay2`",
        )?;
        let year = u16::from_be_bytes([edt[0], edt[1]]);
        let month1 = edt[2];
        let mday1 = edt[3];
        let hour = edt[4];
        let minute = edt[5];
        let num_entries = edt[6];

        Self::from_raw(year, month1, mday1, hour, minute, num_entries)
    }
}

impl EncodableProperty for CumulativeEnergyHistoryDay2 {
    fn encode<W: io::Write>(&self, writer: &mut W) -> io::Result<usize> {
        writer.write_all(&[Self::EPC])?;
        writer.write_all(&self.year.to_be_bytes())?;
        writer.write_all(&[
            self.month1,
            self.mday1,
            self.hour,
            self.minute,
            self.num_entries,
        ])?;
        Ok(8)
    }
}
