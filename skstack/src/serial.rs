//! Wi-SUN adapter through a serial port.

use std::fmt;

use bytes::BytesMut;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio_serial::SerialPortBuilderExt as _;

use crate::error::{CommandError, Result, SerialError};

const RECV_BUF_CAPACITY: usize = 512;

#[derive(Debug)]
pub struct SkSerialPort {
    stream: tokio_serial::SerialStream,
    buf_recv: BytesMut,
    buf_send: Vec<u8>,
}

/// Device management.
impl SkSerialPort {
    pub(super) fn open<S: Into<String>>(path: S, baudrate: u32) -> Result<Self> {
        let path = path.into();
        let mut stream = tokio_serial::new(path.clone(), baudrate)
            .open_native_async()
            .map_err(|source| SerialError::new_open(source, path.as_str().into(), baudrate))?;
        #[cfg(feature = "tracing")]
        tracing::debug!("opened serial port {path:?}");

        // Prevent other processes to change echoback mode.
        #[cfg(unix)]
        stream
            .set_exclusive(true)
            .map_err(|e| SerialError::new_open(e, path.as_str().into(), baudrate))?;

        let this = Self {
            stream,
            buf_recv: BytesMut::with_capacity(2048),
            buf_send: Vec::with_capacity(256),
        };

        Ok(this)
    }

    pub async fn disable_echoback(&mut self) -> Result<()> {
        const COMMAND: &str = "SKSREG SFE 0\r\n";
        self.write_str(COMMAND);
        self.send_buf_all()
            .await
            .map_err(|e| SerialError::new_send(e, Some("SKSREG".into())))?;
        // Do not use `read_command_result_empty()` as it may echoback the command.
        self.read_command_result_text()
            .await
            .map_err(|e| e.and_serial_command_name("SKSREG"))?;
        #[cfg(feature = "tracing")]
        tracing::trace!("successfully disabled echoback");
        Ok(())
    }

    pub async fn flush(&mut self) -> std::io::Result<()> {
        self.stream.flush().await
    }
}

/// Read / recv.
impl SkSerialPort {
    /// Receives more than 1 byte from the device, and push it to the recv buffer.
    ///
    /// Note that this won't be `Ready` until the steram closes or new data is
    /// read. If you would like the function to return Ok(0) when no new data is
    /// received, use `tokio::time::timeout()`.
    pub async fn recv_more(&mut self) -> std::io::Result<usize> {
        loop {
            let n_read = self.stream.read_buf(&mut self.buf_recv).await?;
            if n_read > 0 {
                return Ok(n_read);
            }
            // Increase capacity.
            let mut new_buf =
                BytesMut::with_capacity(RECV_BUF_CAPACITY.max(self.buf_recv.len() * 2));
            new_buf.extend_from_slice(&self.buf_recv);
            self.buf_recv = new_buf;
        }
    }

    /// Reads a line.
    pub async fn read_line(&mut self) -> std::io::Result<BytesMut> {
        loop {
            match self.buf_recv.windows(2).position(|w| w == b"\r\n") {
                Some(line_content_end) => {
                    let line_len = line_content_end + 2;
                    return Ok(self.buf_recv.split_to(line_len));
                }
                None => {
                    self.recv_more().await?;
                }
            }
        }
    }

    /// Reads a line body, i.e. a line excluding the trailing newline.
    pub async fn read_line_body(&mut self) -> std::io::Result<BytesMut> {
        let mut line = self.read_line().await?;
        debug_assert!(line.ends_with(b"\r\n"));
        line.truncate(line.len() - 2);
        Ok(line)
    }

    /// Returns the recv buffer.
    #[inline]
    #[must_use]
    pub fn buf_recv(&self) -> &[u8] {
        &self.buf_recv[..]
    }

    /// Drops the prefix of the recv buffer.
    ///
    /// # Panics
    ///
    /// Panics if `discard_len > self.buf_recv().len()`.
    #[inline]
    pub fn discard_prefix_of_buf_recv(&mut self, discard_len: usize) {
        let _ = self.buf_recv.split_to(discard_len);
    }
}

/// Write / send.
impl SkSerialPort {
    /// Removes all data from the send buffer.
    pub fn clear_send_buf(&mut self) {
        self.buf_send.clear()
    }

    /// Pushes the text to the write queue.
    pub fn write_fmt<T: fmt::Display>(&mut self, content: T) -> fmt::Result {
        use std::io::Write;

        write!(&mut self.buf_send, "{}", content).map_err(|_| fmt::Error)
    }

    /// Pushes the text to the write queue.
    #[inline]
    pub fn write_str(&mut self, s: &str) {
        self.write_bytes(s.as_bytes())
    }

    /// Pushes the text to the write queue.
    pub fn write_bytes(&mut self, s: &[u8]) {
        self.buf_send.extend_from_slice(s);
    }

    /// Writes an entire buffer.
    ///
    /// Note that this does not flush, but this guarantees all contents to be written.
    pub async fn send_buf_all(&mut self) -> std::io::Result<usize> {
        if self.buf_send.is_empty() {
            return Ok(0);
        }
        let len = self.buf_send.len();
        #[cfg(feature = "tracing")]
        tracing::trace!("dispatching command \"{}\"", {
            // Do not print arguments of the command since it can contain credentials.
            let end = self
                .buf_send
                .iter()
                .position(|b| b.is_ascii_whitespace())
                .unwrap_or(self.buf_send.len());
            self.buf_send[..end].escape_ascii()
        });
        self.stream.write_all(&self.buf_send).await?;
        self.clear_send_buf();
        self.flush().await?;
        #[cfg(feature = "tracing")]
        tracing::trace!("flushed send buffer for command execution");
        Ok(len)
    }
}

impl SkSerialPort {
    /// Removes the first empty line from the recv buffer.
    pub async fn read_empty_line(&mut self) -> Result<()> {
        let mut cursor = 0;
        while self.buf_recv.len() < 2 {
            self.recv_more()
                .await
                .map_err(|e| SerialError::new_recv(e, None))?;
        }
        if self.buf_recv.starts_with(b"\r\n") {
            let _ = self.buf_recv.split_to(2);
            return Ok(());
        }
        loop {
            match self.buf_recv[cursor..]
                .windows(2)
                .position(|w| w == b"\r\n")
            {
                Some(line_content_end) => {
                    let line_len = line_content_end + 2;
                    if self.buf_recv[cursor..].starts_with(b"\r\n") {
                        let rest = self.buf_recv.split_off(cursor);
                        if rest.len() == line_len {
                            return Ok(());
                        }
                        self.buf_recv.extend_from_slice(&rest[line_len..]);
                        return Ok(());
                    }
                    cursor += line_len;
                }
                None => {
                    self.recv_more()
                        .await
                        .map_err(|e| SerialError::new_recv(e, None))?;
                }
            }
        }
    }

    /// Reads a single `OK` or `FAIL` result without any content.
    ///
    /// Interrupting content before the result line will be preserved in the buffer.
    // FIXME: `FAIL ERxx\r\n` can follow `\r\nOk\r\n`. WOW! THEN WHAT WAS OK?
    pub async fn read_command_result_empty(&mut self) -> Result<()> {
        let mut cursor = 0;
        loop {
            match self.buf_recv[cursor..]
                .windows(2)
                .position(|w| w == b"\r\n")
            {
                Some(line_content_end) => {
                    let line_len = line_content_end + 2;
                    if self.buf_recv[cursor..].starts_with(b"OK\r\n") {
                        let rest = self.buf_recv.split_off(cursor);
                        if rest.len() == line_len {
                            return Ok(());
                        }
                        let mut new_buf = BytesMut::with_capacity(RECV_BUF_CAPACITY);
                        new_buf.extend_from_slice(&self.buf_recv);
                        new_buf.extend_from_slice(&rest[line_len..]);
                        self.buf_recv = new_buf;
                        return Ok(());
                    }
                    if self.buf_recv[cursor..].starts_with(b"FAIL ") {
                        let mut rest = self.buf_recv.split_off(cursor);
                        let result_line = rest.split_to(line_len);
                        self.buf_recv.extend_from_slice(&rest);
                        return Err(CommandError::from_result_line(
                            &result_line[..line_content_end],
                        )
                        .into());
                    }
                    cursor += line_len;
                }
                None => {
                    self.recv_more()
                        .await
                        .map_err(|e| SerialError::new_recv(e, None))?;
                }
            }
        }
    }

    /// Reads a command result.
    pub async fn read_command_result_text(&mut self) -> Result<BytesMut> {
        let mut cursor = 0;
        loop {
            match self.buf_recv[cursor..]
                .windows(2)
                .position(|w| w == b"\r\n")
            {
                Some(line_content_end) => {
                    let line_len = line_content_end + 2;
                    if self.buf_recv[cursor..].starts_with(b"OK\r\n") {
                        let mut content = self.buf_recv.split_to(cursor + line_len);
                        content.truncate(cursor);
                        return Ok(content);
                    }
                    if self.buf_recv[cursor..].starts_with(b"FAIL ") {
                        let content = self.buf_recv.split_to(cursor + line_len);
                        return Err(
                            CommandError::from_result_line(&content[..line_content_end]).into()
                        );
                    }
                    cursor += line_len;
                }
                None => {
                    self.recv_more()
                        .await
                        .map_err(|e| SerialError::new_recv(e, None))?;
                }
            }
        }
    }
}
