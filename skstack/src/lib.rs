//! SKSTACK IP support through serial port.

mod error;
pub mod event;
mod serial;

use std::fmt;
use std::net::Ipv6Addr;

pub use crate::error::{CommandError, Error, Result, SerialError};
use crate::event::EventNotificationCode;
pub use crate::event::{
    Event, EventKind, EventNotification, EventNotificationContent, EventPanDesc, EventRxUdp,
    UdpSendCompleteStatus,
};
pub use crate::serial::SkSerialPort;

#[derive(Debug, Clone, Copy)]
struct ExpandedIpv6Addr<'a>(&'a Ipv6Addr);

impl<'a> ExpandedIpv6Addr<'a> {
    #[inline]
    #[must_use]
    fn new(addr: &'a Ipv6Addr) -> Self {
        Self(addr)
    }
}

impl fmt::Display for ExpandedIpv6Addr<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let segments = self.0.segments();
        write!(
            f,
            "{:04X}:{:04X}:{:04X}:{:04X}:{:04X}:{:04X}:{:04X}:{:04X}",
            segments[0],
            segments[1],
            segments[2],
            segments[3],
            segments[4],
            segments[5],
            segments[6],
            segments[7]
        )
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum UdpSendSecurity {
    /// Always send plaintext.
    ForcePlaintext = 0,
    /// Always send encrypted data.
    ///
    /// If encrypted communication is not possible, data will not be sent.
    ForceSecure = 1,
    /// Prefer encrypted transmission, but if not possible, send plaintext.
    PreferSecure = 2,
}

impl UdpSendSecurity {
    #[inline]
    #[must_use]
    fn raw(self) -> u8 {
        self as u8
    }
}

#[derive(Debug)]
enum PortState {
    /// The protocol is in error state.
    Error,
    /// Normal state.
    Normal,
}

#[derive(Debug)]
pub struct SkPort {
    port: SkSerialPort,
    state: PortState,
}

impl SkPort {
    // Using `Into<String>` instead of `AsRef<Path>` because
    // `tokio_serial::new()` requires this.
    pub fn open<S: Into<String>>(path: S, baudrate: u32) -> Result<Self> {
        let port = serial::SkSerialPort::open(path.into(), baudrate)?;
        Ok(Self {
            port,
            state: PortState::Normal,
        })
    }

    #[inline]
    #[must_use]
    pub fn is_failed(&self) -> bool {
        matches!(self.state, PortState::Error)
    }

    // TODO: remove later.
    #[inline]
    #[must_use]
    pub fn wisun_port(&mut self) -> &mut SkSerialPort {
        &mut self.port
    }

    /// Receives an event.
    ///
    /// If the event is not completely received, returns `Ok(None)`.
    ///
    /// This function is cancel-safe. If cancelled, no data in the recv buffer
    /// will be is consumed.
    pub async fn recv_event(&mut self) -> Result<Event> {
        if self.is_failed() {
            return Err(Error::AlreadyFailed);
        }

        loop {
            match parse_prefix(self.port.buf_recv()) {
                Ok(Some((consumed_size, ev))) => {
                    self.port.discard_prefix_of_buf_recv(consumed_size);
                    return Ok(ev);
                }
                Ok(None) => {
                    self.port
                        .recv_more()
                        .await
                        .map_err(|e| SerialError::new_recv(e, None))?;
                    continue;
                }
                Err(e) => {
                    self.state = PortState::Error;
                    return Err(e.into());
                }
            }
        }
    }

    /// Sets the register `u8` value by sending `SKSREG` command.
    pub async fn set_register_u8(&mut self, register_name: &str, value: u8) -> Result<()> {
        if !register_name.bytes().all(|b| b.is_ascii_alphanumeric()) {
            return Err(CommandError::new_invalid_argument(
                "expected register name in ASCII alphanumeric",
            )
            .and_command_name("SKSREG")
            .into());
        }

        let wisun_port = self.wisun_port();
        wisun_port
            .write_fmt(format_args!("SKSREG {register_name} {value:02X}\r\n"))
            .expect("formatting here should never fail");
        wisun_port
            .send_buf_all()
            .await
            .map_err(|e| SerialError::new_send(e, Some("SKSREG".into())))?;
        wisun_port
            .read_command_result_empty()
            .await
            .map_err(|e| e.and_serial_command_name(format!("SKSREG {register_name}")))?;
        #[cfg(feature = "tracing")]
        tracing::trace!("set_register_u8() succeeded");

        Ok(())
    }

    /// Sets the register `u16` value by sending `SKSREG` command.
    pub async fn set_register_u16(&mut self, register_name: &str, value: u16) -> Result<()> {
        if !register_name.bytes().all(|b| b.is_ascii_alphanumeric()) {
            return Err(CommandError::new_invalid_argument(
                "expected register name in ASCII alphanumeric",
            )
            .and_command_name("SKSREG")
            .into());
        }

        let wisun_port = self.wisun_port();
        wisun_port
            .write_fmt(format_args!("SKSREG {register_name} {value:04X}\r\n"))
            .expect("formatting here should never fail");
        wisun_port
            .send_buf_all()
            .await
            .map_err(|e| SerialError::new_send(e, Some("SKSREG".into())))?;
        wisun_port
            .read_command_result_empty()
            .await
            .map_err(|e| e.and_serial_command_name(format!("SKSREG {register_name}")))?;
        #[cfg(feature = "tracing")]
        tracing::trace!("set_register_u16() succeeded");

        Ok(())
    }

    /// Connect to the server as PANA client using `SKJOIN` command.
    // TODO: Make cancellation safe?
    pub async fn join(&mut self, ipv6_addr: Ipv6Addr) -> Result<()> {
        let wisun_port = self.wisun_port();

        // Start PANA auth.
        wisun_port
            .write_fmt(format_args!(
                "SKJOIN {}\r\n",
                ExpandedIpv6Addr::new(&ipv6_addr)
            ))
            .expect("formatting here should never fail");
        wisun_port
            .send_buf_all()
            .await
            .map_err(|e| SerialError::new_send(e, Some("SKJOIN".into())))?;
        let _ = wisun_port
            .read_command_result_text()
            .await
            .map_err(|e| e.and_serial_command_name("SKJOIN"))?;

        // Wait for the auth result.
        loop {
            match self
                .recv_event()
                .await
                .map_err(|e| e.and_serial_command_name("SKJOIN"))?
            {
                #[cfg(feature = "tracing")]
                Event::Event(EventNotification {
                    content: EventNotificationContent::UdpSendComplete(status),
                    ..
                }) => {
                    if status == UdpSendCompleteStatus::Failed {
                        tracing::warn!("UDP send error reported");
                    }
                    // Just a notification. Ignore.
                }
                Event::Event(EventNotification {
                    content: EventNotificationContent::PanaStartComplete,
                    ..
                }) => {
                    // Successfully completed.
                    return Ok(());
                }
                Event::Event(EventNotification {
                    content: EventNotificationContent::PanaStartError,
                    ..
                }) => {
                    // Auth failed.
                    return Err(CommandError::new_pana_auth_failed()
                        .and_command_name("SKJOIN")
                        .into());
                }
                _ => {
                    // Ignore other events.
                    // The program will receive some `ERXUDP` events (i.e. will
                    // receive some UDP data), but it is not relevant to the
                    // ECHONET Lite application. I think it would be packets
                    // used to starct PANA connection, but I'm not sure.
                }
            }
        }
    }

    pub async fn rejoin(&mut self) -> Result<()> {
        let wisun_port = self.wisun_port();

        #[cfg(feature = "tracing")]
        tracing::debug!("start PANA re-auth");
        // Start PANA auth.
        wisun_port.write_str("SKREJOIN\r\n");
        wisun_port
            .send_buf_all()
            .await
            .map_err(|e| SerialError::new_send(e, Some("SKREJOIN".into())))?;
        let _ = wisun_port
            .read_command_result_text()
            .await
            .map_err(|e| e.and_serial_command_name("SKREJOIN"))?;

        // Wait for the auth result.
        loop {
            match self.recv_event().await? {
                #[cfg(feature = "tracing")]
                Event::Event(EventNotification {
                    content: EventNotificationContent::UdpSendComplete(status),
                    ..
                }) => {
                    if status == UdpSendCompleteStatus::Failed {
                        tracing::warn!("UDP send error reported");
                    }
                    // Just a notification. Ignore.
                }
                Event::Event(EventNotification {
                    content: EventNotificationContent::PanaStartComplete,
                    ..
                }) => {
                    // Successfully completed.
                    return Ok(());
                }
                Event::Event(EventNotification {
                    content: EventNotificationContent::PanaStartError,
                    ..
                }) => {
                    // Auth failed.
                    return Err(CommandError::new_pana_auth_failed()
                        .and_command_name("SKREJOIN")
                        .into());
                }
                _ => {
                    // Ignore other events.
                    // The program will receive some `ERXUDP` events (i.e. will
                    // receive some UDP data), but it is not relevant to the
                    // ECHONET Lite application. I think it would be packets
                    // used to starct PANA connection, but I'm not sure.
                }
            }
        }
    }

    /// Scans the channel using `SKSCAN` command.
    ///
    /// If `channel_map` is None, `0xffff_ffff` is used as the default.
    // TODO: Make cancellation safe?
    pub async fn scan_active(
        &mut self,
        channel_map: Option<u32>,
        duration_exp: u8,
    ) -> Result<Vec<EventPanDesc>> {
        // 2: Active scan with IE (Information Elements).
        const MODE: u8 = 2;

        if duration_exp > 14 {
            return Err(CommandError::new_invalid_argument(
                "duration should be less than or equal to 14",
            )
            .into());
        }
        let channel_map = channel_map.unwrap_or(!0);

        let wisun_port = self.wisun_port();
        wisun_port
            .write_fmt(format_args!(
                "SKSCAN {MODE:X} {channel_map:08X} {duration_exp:X}\r\n"
            ))
            .expect("formatting here should never fail");
        wisun_port
            .send_buf_all()
            .await
            .map_err(|e| SerialError::new_send(e, Some("SKSCAN".into())))?;
        wisun_port
            .read_command_result_empty()
            .await
            .map_err(|e| e.and_serial_command_name("SKSCAN"))?;
        #[cfg(feature = "tracing")]
        tracing::debug!("scanning smartmeters");

        let mut entries = Vec::with_capacity(1);
        loop {
            match self.recv_event().await? {
                Event::Event(EventNotification {
                    content: EventNotificationContent::ActiveScanComplete,
                    ..
                }) => {
                    break;
                }
                Event::Event(EventNotification {
                    content: EventNotificationContent::BeaconReceived,
                    ..
                }) => {
                    // PanDesc event follows.
                    let pandesc = match self.recv_event().await? {
                        Event::PanDesc(v) => v,
                        ev => {
                            return Err(CommandError::new_operation_failed(format!(
                                "expected PanDesc but received unexpected event {ev:?}"
                            ))
                            .into())
                        }
                    };
                    entries.push(pandesc)
                }
                ev => {
                    return Err(CommandError::new_operation_failed(format!(
                        "received unexpected event during scanning: (ev={ev:?})"
                    ))
                    .into())
                }
            };
        }

        Ok(entries)
    }

    /// Registers PSK generated from the given password, using `SKSETPWD` command.
    pub async fn set_password(&mut self, password: &str) -> Result<()> {
        if !password.bytes().all(|b| b.is_ascii_alphanumeric()) {
            // Do not leak the credential through error message.
            return Err(CommandError::new_invalid_argument(
                "password may be syntactically invalid",
            )
            .into());
        }

        let wisun_port = self.wisun_port();
        wisun_port
            .write_fmt(format_args!(
                "SKSETPWD {:X} {}\r\n",
                password.len(),
                password
            ))
            .expect("formatting here should never fail");
        wisun_port
            .send_buf_all()
            .await
            .map_err(|e| SerialError::new_send(e, Some("SKSETPWD".into())))?;
        wisun_port
            .read_command_result_empty()
            .await
            .map_err(|e| e.and_serial_command_name("SKSETPWD"))?;

        Ok(())
    }

    /// Registers Route-B ID generated from the given ID, using `SKSETRBID` command.
    pub async fn set_route_b_id(&mut self, auth_id: &str) -> Result<()> {
        if auth_id.len() != 32 {
            return Err(CommandError::new_invalid_argument("auth ID may be too short").into());
        }
        if !auth_id.bytes().all(|b| b.is_ascii_alphanumeric()) {
            return Err(
                CommandError::new_invalid_argument("auth ID may be syntactically invalid").into(),
            );
        }

        let wisun_port = self.wisun_port();
        wisun_port
            .write_fmt(format_args!("SKSETRBID {auth_id}\r\n"))
            .expect("formatting here should never fail");
        wisun_port
            .send_buf_all()
            .await
            .map_err(|e| SerialError::new_send(e, Some("SKSETRBID".into())))?;
        wisun_port
            .read_command_result_empty()
            .await
            .map_err(|e| e.and_serial_command_name("SKSETRBID"))?;

        Ok(())
    }

    /// Calculates IPv6 Link Local Address generated from the MAC address.
    pub async fn conv_mac_addr_to_lla64(&mut self, eui64_addr: u64) -> Result<Ipv6Addr> {
        let wisun_port = self.wisun_port();
        wisun_port
            .write_fmt(format_args!("SKLL64 {eui64_addr:016X}\r\n"))
            .expect("formatting here should never fail");
        wisun_port
            .send_buf_all()
            .await
            .map_err(|e| SerialError::new_send(e, Some("SKLL64".into())))?;

        let result = wisun_port
            .read_line_body()
            .await
            .map_err(|e| SerialError::new_recv(e, Some("SKLL64".into())))?;
        let s = std::str::from_utf8(&result).map_err(|_| {
            CommandError::new_invalid_argument(format!(
                "expected SKLL64 result to be ASCII-only, but got \"{}\"",
                result.escape_ascii()
            ))
        })?;
        let v6addr = s.parse::<Ipv6Addr>().map_err(|_| {
            CommandError::new_invalid_argument(format!(
                "failed to parse IPv6 address \"{}\"",
                result.escape_ascii()
            ))
        })?;

        Ok(v6addr)
    }

    /// Sends a data over UDP using `SKSENDTO` command.
    pub async fn send_udp(
        &mut self,
        handle: u64,
        dest: Ipv6Addr,
        port: u16,
        security: UdpSendSecurity,
        data: &[u8],
    ) -> Result<()> {
        if data.len() > 0xffff {
            return Err(CommandError::new_invalid_argument(format!(
                "data should be shorter than 65536 bytes but got {} bytes",
                data.len()
            ))
            .into());
        }

        let wisun_port = self.wisun_port();
        wisun_port
            .write_fmt(format_args!(
                "SKSENDTO {:X} {} {:04X} {:X} {:04X} ",
                handle,
                ExpandedIpv6Addr::new(&dest),
                port,
                security.raw(),
                data.len()
            ))
            .expect("formatting here should never fail");
        wisun_port.write_bytes(data);
        wisun_port.write_str("\r\n");
        wisun_port
            .send_buf_all()
            .await
            .map_err(|e| SerialError::new_send(e, Some("SKSENDTO".into())))?;
        // `SKSENDTO` command returns `\r\nOK\r\n`. Remember truncating the
        // leading `\r\n`.
        wisun_port
            .read_empty_line()
            .await
            .map_err(|e| e.and_serial_command_name("SKSENDTO"))?;
        wisun_port
            .read_command_result_empty()
            .await
            .map_err(|e| e.and_serial_command_name("SKSENDTO"))?;

        Ok(())
    }
}

fn parse_prefix(recv_buf: &[u8]) -> std::result::Result<Option<(usize, Event)>, CommandError> {
    let (kind, args, rest) = {
        let (first_line_body, rest) = match recv_buf.windows(2).position(|w| w == b"\r\n") {
            Some(body_end) => (&recv_buf[..body_end], &recv_buf[(body_end + 2)..]),
            None => return Ok(None),
        };
        let (args, kind) = EventKind::strip_name(first_line_body).ok_or_else(|| {
            CommandError::new_operation_failed(format!(
                "failed to parse event: first line is \"{}\"",
                first_line_body.escape_ascii()
            ))
        })?;
        (kind, args.strip_prefix(b" ").unwrap_or(args), rest)
    };

    let (rest, ev) = match kind {
        EventKind::RxUdp => {
            let ev = parse_rxudp_args(args)?;
            (rest, Event::RxUdp(ev))
        }
        EventKind::PanDesc => {
            // `EPANDESC` event does not have args.
            // Parse the following lines.
            match parse_pandesc_body(rest) {
                Ok(Some((rest, ev))) => (rest, Event::PanDesc(ev)),
                Ok(None) => return Ok(None),
                Err(e) => return Err(e),
            }
        }
        EventKind::Event => {
            let ev = parse_event_notification_args(args)?;
            (rest, Event::Event(ev))
        }
        _ => {
            return Err(CommandError::new_operation_failed(format!(
                "received unsupported event {}",
                kind.name()
            )))
        }
    };

    Ok(Some((recv_buf.len() - rest.len(), ev)))
}

fn parse_as_ipv6_addr(str_bytes: &[u8]) -> Option<Ipv6Addr> {
    let addr = std::str::from_utf8(str_bytes).ok()?;
    addr.parse::<Ipv6Addr>().ok()
}

fn parse_as_u16(str_bytes: &[u8]) -> Option<u16> {
    let s = std::str::from_utf8(str_bytes).ok()?;
    u16::from_str_radix(s, 16).ok()
}

fn parse_rxudp_args(args: &[u8]) -> std::result::Result<EventRxUdp, CommandError> {
    // `ERXUDP <sender_ipv6addr> <receiver_ipv6addr> <sender_port> <receiver_port> <sender_lla> <is_secured> <data_len> <data><CRLF>`
    let mut fields = args.split(|b| b.is_ascii_whitespace());

    let sender_addr = fields.next().ok_or_else(|| {
        CommandError::new_operation_failed("failed to get sender IPv6 addr from an ERXUDP event")
    })?;
    let sender_addr = parse_as_ipv6_addr(sender_addr)
        .ok_or_else(|| CommandError::new_operation_failed("failed to get sender IPv6 addr"))?;

    let receiver_addr = fields.next().ok_or_else(|| {
        CommandError::new_operation_failed("failed to get receiver IPv6 addr from an ERXUDP event")
    })?;
    let receiver_addr = parse_as_ipv6_addr(receiver_addr)
        .ok_or_else(|| CommandError::new_operation_failed("failed to get receiver IPv6 addr"))?;

    let sender_port = fields.next().ok_or_else(|| {
        CommandError::new_operation_failed("failed to get sender IPv6 port from an ERXUDP event")
    })?;
    let sender_port = parse_as_u16(sender_port).ok_or_else(|| {
        CommandError::new_operation_failed(format!(
            "failed to get sender port: expected port but got \"{}\"",
            sender_port.escape_ascii()
        ))
    })?;

    let receiver_port = fields.next().ok_or_else(|| {
        CommandError::new_operation_failed("failed to get receiver IPv6 port from an ERXUDP event")
    })?;
    let receiver_port = parse_as_u16(receiver_port).ok_or_else(|| {
        CommandError::new_operation_failed(format!(
            "failed to get receiver port: expected port but got \"{}\"",
            receiver_port.escape_ascii()
        ))
    })?;

    // Skip "Sender LLA" field.
    let _ = fields.next();
    // Skip "Secured" field.
    let _ = fields.next();

    let data_len = fields.next().ok_or_else(|| {
        CommandError::new_operation_failed("failed to get data length from an ERXUDP event")
    })?;
    let data_len = parse_as_u16(data_len).map(usize::from).ok_or_else(|| {
        CommandError::new_operation_failed(format!(
            "failed to get data length: expected length but got \"{}\"",
            data_len.escape_ascii()
        ))
    })?;

    let data_encoded = fields.next().ok_or_else(|| {
        CommandError::new_operation_failed("failed to get data body from an ERXUDP event")
    })?;
    if data_encoded.len() != data_len * 2 {
        return Err(CommandError::new_operation_failed(
            "data length mismatch for ERXUDP event",
        ));
    }
    if data_encoded.iter().any(|&b| !b.is_ascii_hexdigit()) {
        return Err(CommandError::new_operation_failed(format!(
            "invalid data in ERXUDP event: data = \"{}\"",
            data_encoded.escape_ascii()
        )));
    }
    let mut data = Vec::with_capacity(data_len);
    data.extend(data_encoded.chunks_exact(2).map(|chunk| {
        let upper = match chunk[0] {
            v @ b'0'..=b'9' => v - b'0',
            v @ b'A'..=b'F' => v - b'A' + 10,
            v @ b'a'..=b'f' => v - b'a' + 10,
            _ => unreachable!(),
        };
        let lower = match chunk[1] {
            v @ b'0'..=b'9' => v - b'0',
            v @ b'A'..=b'F' => v - b'A' + 10,
            v @ b'a'..=b'f' => v - b'a' + 10,
            _ => unreachable!(),
        };
        upper << 4 | lower
    }));

    Ok(EventRxUdp {
        sender: (sender_addr, sender_port),
        receiver: (receiver_addr, receiver_port),
        data,
    })
}

fn parse_pandesc_body(
    recv_buf: &[u8],
) -> std::result::Result<Option<(&[u8], EventPanDesc)>, CommandError> {
    // Example content:
    //  ```
    //  EPANDESC
    //    Channel:XX
    //    Channel Page:XX
    //    Pan ID:XXXX
    //    Addr:XXXXXXXXXXXXXXXX
    //    LQI:XX
    //    PairID:XXXXXXXX
    //  ```

    // Take 6 lines.
    let pandesc_end = match recv_buf
        .iter()
        .enumerate()
        .filter(|(_i, &b)| b == b'\n')
        .nth(5)
    {
        Some((i, _b)) => i,
        None => return Ok(None),
    };
    let (content, rest) = recv_buf.split_at(pandesc_end + 1);
    let content = std::str::from_utf8(content).map_err(|_| {
        CommandError::new_operation_failed(format!(
            "failed to parse the EPANDESC event: body = \"{}\"",
            content.escape_ascii()
        ))
    })?;

    let mut channel = None;
    let mut channel_page = None;
    let mut pan_id = None;
    let mut addr = None;
    let mut lqi = None;
    let mut pair_id = None;

    for line in content.lines().map(|l| l.trim()) {
        let (key, value) = match line.find(':') {
            Some(pos) => (&line[..pos], &line[(pos + 1)..]),
            None => {
                return Err(CommandError::new_operation_failed(format!(
                    "failed to parse a line in EPANDESC event: line = {line:?}"
                )))
            }
        };

        match key {
            "Channel" => {
                channel = Some(u8::from_str_radix(value, 16).map_err(|_| {
                    CommandError::new_operation_failed(format!(
                        "failed to parse channel ({value:?})"
                    ))
                })?);
            }
            "Channel Page" => {
                channel_page = Some(u8::from_str_radix(value, 16).map_err(|_| {
                    CommandError::new_operation_failed(format!(
                        "failed to parse channel page ({value:?})"
                    ))
                })?);
            }
            "Pan ID" => {
                pan_id = Some(u16::from_str_radix(value, 16).map_err(|_| {
                    CommandError::new_operation_failed(format!(
                        "failed to parse pan ID ({value:?})"
                    ))
                })?);
            }
            "Addr" => {
                addr = Some(u64::from_str_radix(value, 16).map_err(|_| {
                    CommandError::new_operation_failed(format!(
                        "failed to parse responder addr ({value:?})"
                    ))
                })?);
            }
            "LQI" => {
                lqi = Some(u8::from_str_radix(value, 16).map_err(|_| {
                    CommandError::new_operation_failed(format!("failed to parse LQI ({value:?})"))
                })?);
            }
            "PairID" => {
                pair_id = Some(
                    u64::from_str_radix(value, 16)
                        .map_err(|_| {
                            CommandError::new_operation_failed(format!(
                                "failed to parse LQI ({value:?})"
                            ))
                        })?
                        .to_be_bytes(),
                );
            }
            _ => {}
        }
    }

    let channel = channel
        .ok_or_else(|| CommandError::new_operation_failed("channel not found in EPANDESC data"))?;
    let channel_page = channel_page.ok_or_else(|| {
        CommandError::new_operation_failed("channel page not found in EPANDESC data")
    })?;
    let pan_id = pan_id
        .ok_or_else(|| CommandError::new_operation_failed("pan ID not found in EPANDESC data"))?;
    let addr = addr.ok_or_else(|| {
        CommandError::new_operation_failed("responder addr not found in EPANDESC data")
    })?;
    let lqi =
        lqi.ok_or_else(|| CommandError::new_operation_failed("LQI not found in EPANDESC data"))?;
    let pair_id = pair_id
        .ok_or_else(|| CommandError::new_operation_failed("pair ID not found in EPANDESC data"))?;

    Ok(Some((
        rest,
        EventPanDesc {
            channel,
            channel_page,
            pan_id,
            addr,
            lqi,
            pair_id,
        },
    )))
}

fn parse_event_notification_args(
    args: &[u8],
) -> std::result::Result<EventNotification, CommandError> {
    // `EVENT <code> <sender> <param><CRLF>`
    let args = std::str::from_utf8(args).map_err(|_| {
        CommandError::new_operation_failed(format!(
            "failed to parse args of EVENT event (\"{}\")",
            args.escape_ascii()
        ))
    })?;
    let mut fields = args.split_ascii_whitespace();

    let code = fields.next().ok_or_else(|| {
        CommandError::new_operation_failed("failed to get event code from EVENT event")
    })?;
    let code = u8::from_str_radix(code, 16).map_err(|_| {
        CommandError::new_operation_failed(format!("failed to parse event code ({code:?})"))
    })?;
    let code = EventNotificationCode::from_code(code).ok_or_else(|| {
        CommandError::new_operation_failed(format!("unknown event code ({code:?})"))
    })?;

    let sender = fields.next().ok_or_else(|| {
        CommandError::new_operation_failed("failed to get sender addr from EVENT event")
    })?;
    let sender = sender.parse::<Ipv6Addr>().map_err(|_| {
        CommandError::new_operation_failed(format!("failed to parse sender addr ({sender:?})"))
    })?;

    let content = match code {
        EventNotificationCode::NeighborSolicitationReceived => {
            EventNotificationContent::NeighborSolicitationReceived
        }
        EventNotificationCode::NeighborAdvertisementReceived => {
            EventNotificationContent::NeighborAdvertisementReceived
        }
        EventNotificationCode::EchoRequestReceived => EventNotificationContent::EchoRequestReceived,
        EventNotificationCode::EnergyDetectionScanComplete => {
            EventNotificationContent::EnergyDetectionScanComplete
        }
        EventNotificationCode::BeaconReceived => EventNotificationContent::BeaconReceived,
        EventNotificationCode::UdpSendComplete => {
            let param = fields.next().ok_or_else(|| {
                CommandError::new_operation_failed("failed to get param from EVENT event")
            })?;
            let param = u8::from_str_radix(param, 16).map_err(|_| {
                CommandError::new_operation_failed(format!("failed to param ({param:?})"))
            })?;
            let param = UdpSendCompleteStatus::from_code(param).ok_or_else(|| {
                CommandError::new_operation_failed(
                    "unknown UDP send complete event param ({param:?})",
                )
            })?;

            EventNotificationContent::UdpSendComplete(param)
        }
        EventNotificationCode::ActiveScanComplete => EventNotificationContent::ActiveScanComplete,
        EventNotificationCode::PanaStartError => EventNotificationContent::PanaStartError,
        EventNotificationCode::PanaStartComplete => EventNotificationContent::PanaStartComplete,
        EventNotificationCode::PanaCloseRequested => EventNotificationContent::PanaCloseRequested,
        EventNotificationCode::PanaCloseComplete => EventNotificationContent::PanaCloseComplete,
        EventNotificationCode::PanaCloseTimeout => EventNotificationContent::PanaCloseTimeout,
        EventNotificationCode::SessionExpired => EventNotificationContent::SessionExpired,
        EventNotificationCode::Arib108SendRestricted => {
            EventNotificationContent::Arib108SendRestricted
        }
        EventNotificationCode::Arib108SendRestrictionLifted => {
            EventNotificationContent::Arib108SendRestrictionLifted
        }
    };

    Ok(EventNotification { content, sender })
}
