//! Event types.

use std::fmt;
use std::net::Ipv6Addr;

// Event kind for SKSTACK IP.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum EventKind {
    /// UDP received.
    RxUdp,
    /// TCP received.
    RxTcp,
    /// ICMP Echo reply received.
    Pong,
    /// Tcp connection changed.
    Tcp,
    /// Available IPv6 addr notified.
    Addr,
    /// IPv6 entries in neighbor cache notified.
    Neighbor,
    /// PAN descriptions notified.
    PanDesc,
    /// ED scan result.
    EdScan,
    /// List of listening ports.
    Port,
    /// List of TCP handles.
    Handle,
    /// Event received.
    Event,
}

impl EventKind {
    const ALL: [Self; 11] = [
        Self::RxUdp,
        Self::RxTcp,
        Self::Pong,
        Self::Tcp,
        Self::Addr,
        Self::Neighbor,
        Self::PanDesc,
        Self::EdScan,
        Self::Port,
        Self::Handle,
        Self::Event,
    ];

    #[must_use]
    pub(crate) fn strip_name(name: &[u8]) -> Option<(&[u8], Self)> {
        Self::ALL.into_iter().find_map(|kind| {
            name.strip_prefix(kind.name().as_bytes())
                .map(|rest| (rest, kind))
        })
    }

    #[must_use]
    pub fn name(self) -> &'static str {
        match self {
            Self::RxUdp => "ERXUDP",
            Self::RxTcp => "ERXTCP",
            Self::Pong => "EPONG",
            Self::Tcp => "ETCP",
            Self::Addr => "EADDR",
            Self::Neighbor => "ENEIGHBOR",
            Self::PanDesc => "EPANDESC",
            Self::EdScan => "EEDSCAN",
            Self::Port => "EPORT",
            Self::Handle => "EHANDLE",
            Self::Event => "EVENT",
        }
    }
}

// Event type for SKSTACK IP.
#[derive(Debug, Clone)]
#[allow(clippy::enum_variant_names)] // for `EventNotification`.
pub enum Event {
    /// UDP received.
    // Some fields omitted.
    RxUdp(EventRxUdp),
    /// PAN descriptions notified.
    PanDesc(EventPanDesc),
    /// Event received.
    Event(EventNotification),
}

// Some fields omitted.
#[derive(Clone)]
pub struct EventRxUdp {
    pub sender: (Ipv6Addr, u16),
    pub receiver: (Ipv6Addr, u16),
    pub data: Vec<u8>,
}

impl fmt::Debug for EventRxUdp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("EventRxUdp")
            .field(
                "sender",
                &(&self.sender.0, &format_args!("{:#06x}", self.sender.1)),
            )
            .field(
                "receiver",
                &(&self.receiver.0, &format_args!("{:#06x}", self.receiver.1)),
            )
            .field("data", &format_args!("{:02X?}", &self.data))
            .finish()
    }
}

#[derive(Debug, Clone, Copy)]
pub struct EventPanDesc {
    pub channel: u8,
    pub channel_page: u8,
    pub pan_id: u16,
    pub addr: u64,
    pub lqi: u8,
    pub pair_id: [u8; 8],
}

impl EventPanDesc {
    /// Returns the RSSI value corresponding to the LQI value.
    #[inline]
    #[must_use]
    pub fn rssi(&self) -> f64 {
        0.275 * f64::from(self.lqi) - 104.27
    }
}

#[derive(Debug, Clone)]
pub struct EventNotification {
    pub content: EventNotificationContent,
    pub sender: Ipv6Addr,
}

#[derive(Debug, Clone, Copy)]
pub(super) enum EventNotificationCode {
    NeighborSolicitationReceived = 0x01,
    NeighborAdvertisementReceived = 0x02,
    EchoRequestReceived = 0x05,
    EnergyDetectionScanComplete = 0x1f,
    BeaconReceived = 0x20,
    UdpSendComplete = 0x21,
    ActiveScanComplete = 0x22,
    PanaStartError = 0x24,
    PanaStartComplete = 0x25,
    PanaCloseRequested = 0x26,
    PanaCloseComplete = 0x27,
    PanaCloseTimeout = 0x28,
    SessionExpired = 0x29,
    Arib108SendRestricted = 0x32,
    Arib108SendRestrictionLifted = 0x33,
}

impl EventNotificationCode {
    const ALL: [Self; 15] = [
        Self::NeighborSolicitationReceived,
        Self::NeighborAdvertisementReceived,
        Self::EchoRequestReceived,
        Self::EnergyDetectionScanComplete,
        Self::BeaconReceived,
        Self::UdpSendComplete,
        Self::ActiveScanComplete,
        Self::PanaStartError,
        Self::PanaStartComplete,
        Self::PanaCloseRequested,
        Self::PanaCloseComplete,
        Self::PanaCloseTimeout,
        Self::SessionExpired,
        Self::Arib108SendRestricted,
        Self::Arib108SendRestrictionLifted,
    ];

    #[must_use]
    pub fn from_code(code: u8) -> Option<Self> {
        Self::ALL.into_iter().find(|&v| v as u8 == code)
    }
}

#[derive(Debug, Clone)]
pub enum EventNotificationContent {
    NeighborSolicitationReceived,
    NeighborAdvertisementReceived,
    EchoRequestReceived,
    EnergyDetectionScanComplete,
    BeaconReceived,
    UdpSendComplete(UdpSendCompleteStatus),
    ActiveScanComplete,
    PanaStartError,
    PanaStartComplete,
    PanaCloseRequested,
    PanaCloseComplete,
    PanaCloseTimeout,
    SessionExpired,
    Arib108SendRestricted,
    Arib108SendRestrictionLifted,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum UdpSendCompleteStatus {
    Succeeded = 0,
    Failed = 1,
    SentNeighborSolicitation = 2,
}

impl UdpSendCompleteStatus {
    #[inline]
    #[must_use]
    pub fn from_code(param: u8) -> Option<Self> {
        match param {
            0 => Some(Self::Succeeded),
            1 => Some(Self::Failed),
            2 => Some(Self::SentNeighborSolicitation),
            _ => None,
        }
    }
}
