//! Error types.

use std::fmt;
use std::io;
use std::path::PathBuf;

use thiserror::Error;

/// Result type.
pub type Result<T> = std::result::Result<T, Error>;

/// SKSTACK error.
#[derive(Debug, Error)]
pub enum Error {
    /// The port is already failed and unavailable.
    #[error("the serial port is already in failed state and is unavailable")]
    AlreadyFailed,
    /// Command is successfully submitted and then failed.
    #[error("SKSTACK command failed")]
    Command(#[from] CommandError),
    /// Lowlevel serial port I/O error.
    #[error("serial port error")]
    SerialIo(#[from] SerialError),
}

impl Error {
    pub(crate) fn and_serial_command_name(self, name: impl fmt::Display) -> Self {
        match self {
            Self::SerialIo(e) => Self::SerialIo(e.and_command_name(name.to_string())),
            e => e,
        }
    }
}

/// SKSTACK command error.
#[derive(Debug, Error)]
pub struct CommandError {
    /// Name of the failed command.
    command_name: Option<String>,
    /// Command error details.
    details: CommandErrorDetails,
}

impl CommandError {
    #[must_use]
    pub(crate) fn and_command_name(self, name: impl fmt::Display) -> Self {
        Self {
            command_name: Some(name.to_string()),
            details: self.details,
        }
    }

    /// Creates a new `CommandError` from the given command result line.
    ///
    /// Note that the line should not contain the newline as a suffix.
    #[must_use]
    pub(crate) fn from_result_line(line: &[u8]) -> Self {
        let details = match Self::get_code_from_result_line(line) {
            Some(code) => CommandErrorDetails::ErrorCode(code),
            None => CommandErrorDetails::UnknownResult(line.into()),
        };
        Self {
            command_name: None,
            details,
        }
    }

    #[must_use]
    fn get_code_from_result_line(line: &[u8]) -> Option<u8> {
        // 7 is `"FAIL ER".len()`.
        let code = line.get(7..)?;
        if code.iter().all(|b| b.is_ascii_digit()) {
            return None;
        }
        let code = std::str::from_utf8(code).ok()?;
        code.parse::<u8>().ok()
    }

    #[inline]
    #[must_use]
    pub(crate) fn new_invalid_argument(message: impl fmt::Display) -> Self {
        Self {
            command_name: None,
            details: CommandErrorDetails::InvalidArgument {
                message: message.to_string(),
            },
        }
    }

    #[inline]
    #[must_use]
    pub(crate) fn new_pana_auth_failed() -> Self {
        Self {
            command_name: None,
            details: CommandErrorDetails::PanaAuthFailed,
        }
    }

    #[inline]
    #[must_use]
    pub(crate) fn new_operation_failed(message: impl fmt::Display) -> Self {
        Self {
            command_name: None,
            details: CommandErrorDetails::OperationFailed {
                message: message.to_string(),
            },
        }
    }
}

impl fmt::Display for CommandError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &self.command_name {
            Some(command_name) => write!(f, "command {:?} failed: {}", command_name, self.details),
            None => write!(f, "command failed: {}", self.details),
        }
    }
}

#[derive(Debug, Error)]
enum CommandErrorDetails {
    /// Failed to submit command, and the result was parsable as expected.
    ErrorCode(u8),
    /// Failed to submit command, and the result was not parsable as expected.
    UnknownResult(Vec<u8>),
    /// Command was not submitted due to invalid argument.
    InvalidArgument { message: String },
    /// Command was successfully submitted and accepted, but PANA auth failed.
    PanaAuthFailed,
    /// Command was successfully submitted and accepted, but resulted in failure.
    OperationFailed { message: String },
}

impl fmt::Display for CommandErrorDetails {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::ErrorCode(code) => write!(f, "error code ER{code:02}"),
            Self::UnknownResult(line) => {
                write!(f, "unparsable command result \"{}\"", line.escape_ascii())
            }
            Self::InvalidArgument { message } => {
                write!(f, "invalid argument: {message}")
            }
            Self::PanaAuthFailed => {
                write!(f, "failed to complete PANA auth")
            }
            Self::OperationFailed { message } => {
                write!(f, "operation failed: {message}")
            }
        }
    }
}

/// Serial port I/O error.
#[derive(Debug, Error)]
#[error("{context}")]
pub struct SerialError {
    /// Inner error.
    ///
    /// This can be `std::io::Error` and `tokio_serial::Error`.
    source: Option<Box<dyn std::error::Error + Send + Sync + 'static>>,
    /// Context of the error.
    context: SerialErrorContext,
}

impl SerialError {
    #[inline]
    pub(crate) fn new_open(source: tokio_serial::Error, path: PathBuf, baudrate: u32) -> Self {
        Self {
            source: Some(source.into()),
            context: SerialErrorContext::Open { path, baudrate },
        }
    }

    #[inline]
    pub(crate) fn new_send(source: io::Error, command_name: Option<String>) -> Self {
        Self {
            source: Some(source.into()),
            context: SerialErrorContext::Send { command_name },
        }
    }

    #[inline]
    pub(crate) fn new_recv(source: io::Error, command_name: Option<String>) -> Self {
        Self {
            source: Some(source.into()),
            context: SerialErrorContext::Receive { command_name },
        }
    }

    pub(crate) fn and_command_name(self, name: String) -> Self {
        let Self {
            source,
            mut context,
        } = self;
        match context {
            SerialErrorContext::Send {
                ref mut command_name,
            } => *command_name = Some(name),
            SerialErrorContext::Receive {
                ref mut command_name,
            } => *command_name = Some(name),
            _ => {}
        };
        Self { source, context }
    }
}

#[derive(Debug, Error)]
pub(crate) enum SerialErrorContext {
    /// Cannot open the serial port.
    #[error("failed to open serial port {path} with baudrate {baudrate}")]
    Open { path: PathBuf, baudrate: u32 },
    /// Cannot write to or flush the stream.
    #[error("failed to write to or flush the stream")]
    Send { command_name: Option<String> },
    /// Cannot receive data from the stream.
    #[error("failed to read from the stream")]
    Receive { command_name: Option<String> },
}
