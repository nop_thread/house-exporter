//! Service.

use std::fmt;
use std::num::NonZeroU8;

/// Raw ECHONET Lite service code.
#[derive(Clone, Copy, PartialEq, Eq)]
pub struct ServiceCode(NonZeroU8);

impl fmt::Debug for ServiceCode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut d = f.debug_struct("ServiceCode");
        d.field("raw", &format_args!("{:#04X}", self.raw()));
        if let Some(known) = self.to_known_code() {
            d.field("known", &known);
            d.field("symbol", &known.spec_symbol());
        }
        d.finish()
    }
}

impl ServiceCode {
    #[inline]
    #[must_use]
    pub fn raw(self) -> u8 {
        self.0.get()
    }

    #[inline]
    #[must_use]
    pub fn from_raw(esv: u8) -> Option<Self> {
        // ESV is `0b01xxxxxx`.
        NonZeroU8::new(esv)
            .filter(|v| (v.get() & 0b_1100_0000) == 0b_0100_0000)
            .map(Self)
    }

    #[must_use]
    pub fn to_known_code(self) -> Option<KnownServiceCode> {
        let known = match self.raw() {
            0x60 => KnownServiceCode::WriteRequest(ConfirmationRequest::NotRequired),
            0x61 => KnownServiceCode::WriteRequest(ConfirmationRequest::Required),
            0x62 => KnownServiceCode::ReadRequest,
            0x63 => KnownServiceCode::NotifyRequest,
            0x6e => KnownServiceCode::WriteReadRequest,
            0x71 => KnownServiceCode::WriteSuccess,
            0x72 => KnownServiceCode::ReadSuccess,
            0x73 => KnownServiceCode::NotifySuccess(ConfirmationRequest::NotRequired),
            0x74 => KnownServiceCode::NotifySuccess(ConfirmationRequest::Required),
            0x7a => KnownServiceCode::NotifySuccessConfirmation,
            0x7e => KnownServiceCode::WriteReadSuccess,
            0x50 => KnownServiceCode::WriteFailure(ConfirmationRequest::NotRequired),
            0x51 => KnownServiceCode::WriteFailure(ConfirmationRequest::Required),
            0x52 => KnownServiceCode::ReadFailure,
            0x53 => KnownServiceCode::NotifyFailure,
            0x5e => KnownServiceCode::WriteReadFailure,
            _ => return None,
        };
        debug_assert_eq!(known.raw(), self.raw());
        Some(known)
    }
}

/// Necessity of confirmation.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ConfirmationRequest {
    /// Confirmation response is not required.
    NotRequired,
    /// Confirmation response is required.
    Required,
}

/// Known ECHONET Lite service code.
// NOTE:
// * `SNA` would stand for "Service Not Available".
// * `I` would stand for "Ignorable".
// * `C` would stand for "Confirmation".
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum KnownServiceCode {
    /// `SetI` (0x60) and `SetC` (0x61), write request.
    WriteRequest(ConfirmationRequest),
    /// `Get` (0x62), read request.
    ReadRequest,
    /// `INF_REQ` (0x63), information request.
    NotifyRequest,
    /// `SetGet` (0x6E), write and read request.
    WriteReadRequest,
    /// `Set_Res` (0x71), response for [`WriteRequest`][`Self::WriteRequest`].
    WriteSuccess,
    /// `GetRes` (0x72), response for [`ReadRequest`][`Self::ReadRequest`].
    ReadSuccess,
    /// `INF` (0x73) and `INFC` (0x74), response for [`NotifyRequest`][`Self::NotifyRequest`].
    NotifySuccess(ConfirmationRequest),
    /// `INFC_Res` (0x74), notification response for
    /// [`NotifyResponse`][`Self::NotifyResponse`]`(`[`Required`][`ConfirmationRequest::Required`]`)`.
    NotifySuccessConfirmation,
    /// `SetGet_Res` (0x75), response for [`WriteReadRequest`][`Self::WriteReadRequest`].
    WriteReadSuccess,
    /// `SetI_SNA` (0x50) and `SetC_SNA` (0x51), response for failed
    /// [`WriteRequest`][`Self::WriteRequest`].
    WriteFailure(ConfirmationRequest),
    /// `Get_SNA` (0x52), response for failed [`ReadRequest`][`Self::ReadRequest`].
    ReadFailure,
    /// `INF_SNA` (0x53), response for failed [`NotifyRequest`][`Self::NotifyRequest`].
    NotifyFailure,
    /// `SetGet_SNA` (0x5E), response for failed [`WriteReadRequest`][`Self::WriteReadRequest`].
    WriteReadFailure,
}

impl KnownServiceCode {
    #[must_use]
    pub fn spec_symbol(self) -> &'static str {
        match self {
            Self::WriteRequest(ConfirmationRequest::NotRequired) => "SetI",
            Self::WriteRequest(ConfirmationRequest::Required) => "SetC",
            Self::ReadRequest => "Get",
            Self::NotifyRequest => "INF_REQ",
            Self::WriteReadRequest => "SetGet",
            Self::WriteSuccess => "Set_Res",
            Self::ReadSuccess => "Get_Res",
            Self::NotifySuccess(ConfirmationRequest::NotRequired) => "INF",
            Self::NotifySuccess(ConfirmationRequest::Required) => "INFC",
            Self::NotifySuccessConfirmation => "INFC_Res",
            Self::WriteReadSuccess => "SetGet_Res",
            Self::WriteFailure(ConfirmationRequest::NotRequired) => "SetI_SNA",
            Self::WriteFailure(ConfirmationRequest::Required) => "SetC_SNA",
            Self::ReadFailure => "Get_SNA",
            Self::NotifyFailure => "INF_SNA",
            Self::WriteReadFailure => "SetGet_SNA",
        }
    }

    #[must_use]
    pub fn raw(self) -> u8 {
        match self {
            Self::WriteRequest(ConfirmationRequest::NotRequired) => 0x60,
            Self::WriteRequest(ConfirmationRequest::Required) => 0x61,
            Self::ReadRequest => 0x62,
            Self::NotifyRequest => 0x63,
            Self::WriteReadRequest => 0x6e,
            Self::WriteSuccess => 0x71,
            Self::ReadSuccess => 0x72,
            Self::NotifySuccess(ConfirmationRequest::NotRequired) => 0x73,
            Self::NotifySuccess(ConfirmationRequest::Required) => 0x74,
            Self::NotifySuccessConfirmation => 0x7a,
            Self::WriteReadSuccess => 0x7e,
            Self::WriteFailure(ConfirmationRequest::NotRequired) => 0x50,
            Self::WriteFailure(ConfirmationRequest::Required) => 0x51,
            Self::ReadFailure => 0x52,
            Self::NotifyFailure => 0x53,
            Self::WriteReadFailure => 0x5e,
        }
    }

    #[inline]
    #[must_use]
    pub fn to_generic(self) -> ServiceCode {
        self.into()
    }
}

impl From<KnownServiceCode> for ServiceCode {
    #[inline]
    fn from(v: KnownServiceCode) -> Self {
        Self::from_raw(v.raw()).expect("all known service codes are valid")
    }
}

impl PartialEq<KnownServiceCode> for ServiceCode {
    #[inline]
    fn eq(&self, other: &KnownServiceCode) -> bool {
        self.raw() == other.raw()
    }
}

impl PartialEq<ServiceCode> for KnownServiceCode {
    #[inline]
    fn eq(&self, other: &ServiceCode) -> bool {
        self.raw() == other.raw()
    }
}
