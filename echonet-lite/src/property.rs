//! Property.

use std::fmt;
use std::io;
use std::num::NonZeroU8;

use crate::error::{PropertyParseError, PropertyParseErrorKind};

use thiserror::Error;

pub trait DecodableProperty: Sized {
    /// Decode error.
    type Error: 'static + std::error::Error + Send + Sync;

    /// Decodes the raw binary data for the property, including property headers.
    ///
    /// Returns the consumed (parsed) length of bytes and the resulting object.
    fn parse_prop(input: &[u8]) -> Result<(usize, Self), PropertyParseError> {
        if input.len() < 2 {
            return Err(PropertyParseErrorKind::PropertyLessThan2Bytes.into());
        }
        let epc =
            PropertyCode::from_raw(input[0]).ok_or(PropertyParseErrorKind::InvalidPropertyCode)?;
        let pdc = usize::from(input[1]);
        let edt = &input[2..(pdc + 2)];
        Self::decode_edt(epc, edt)
            .map(|prop| (pdc + 2, prop))
            .map_err(PropertyParseError::new_invalid_edt)
    }

    /// Decodes the EDT (ECHONET Lite property value data).
    ///
    /// Note that EDT is at most 256 bytes long.
    fn decode_edt(epc: PropertyCode, edt: &[u8]) -> Result<Self, Self::Error>;
}

impl<T: DecodableProperty> DecodableProperty for Option<T> {
    type Error = T::Error;

    fn decode_edt(epc: PropertyCode, edt: &[u8]) -> Result<Self, Self::Error> {
        if edt.is_empty() {
            return Ok(None);
        }
        T::decode_edt(epc, edt).map(Some)
    }
}

pub trait EncodableProperty: Sized {
    /// Encodes and the writes the property.
    fn encode<W: io::Write>(&self, writer: &mut W) -> io::Result<usize>;
}

/// ECHONET property code.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct PropertyCode(NonZeroU8);

impl PropertyCode {
    #[inline]
    #[must_use]
    pub fn raw(self) -> u8 {
        self.0.get()
    }

    #[inline]
    #[must_use]
    pub fn from_raw(epc: u8) -> Option<Self> {
        // EPC is `0b1xxxxxxx`.
        NonZeroU8::new(epc).filter(|v| v.get() >= 0x80).map(Self)
    }
}

impl fmt::Debug for PropertyCode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "PropertyCode({:#02X})", self.raw())
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Error)]
#[error("expected empty property but got non-empty EDT (edt.len={edt_len})")]
pub struct EmptyPropertyError {
    edt_len: usize,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct EmptyProperty {
    /// EPC: ECHONET Lite property code.
    prop_code: PropertyCode,
}

impl EmptyProperty {
    #[inline]
    #[must_use]
    pub fn new(prop_code: PropertyCode) -> Self {
        Self { prop_code }
    }

    #[inline]
    #[must_use]
    pub fn prop_code(&self) -> PropertyCode {
        self.prop_code
    }

    #[inline]
    pub fn decode_as<T: DecodableProperty>(&self) -> Result<T, T::Error> {
        T::decode_edt(self.prop_code, &[])
    }
}

impl DecodableProperty for EmptyProperty {
    type Error = EmptyPropertyError;

    fn decode_edt(epc: PropertyCode, edt: &[u8]) -> Result<Self, Self::Error> {
        if !edt.is_empty() {
            return Err(EmptyPropertyError { edt_len: edt.len() });
        }
        Ok(Self { prop_code: epc })
    }
}

impl EncodableProperty for EmptyProperty {
    #[inline]
    fn encode<W: io::Write>(&self, writer: &mut W) -> io::Result<usize> {
        writer.write_all(&[self.prop_code.raw(), 0]).map(|()| 2)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct MaybeEmptyProperty<T> {
    prop_code: PropertyCode,
    prop: Option<T>,
}

impl<T> MaybeEmptyProperty<T> {
    #[inline]
    #[must_use]
    pub fn new(prop_code: PropertyCode, prop: Option<T>) -> Self {
        Self { prop_code, prop }
    }

    #[inline]
    #[must_use]
    pub fn empty(prop_code: PropertyCode) -> Self {
        Self {
            prop_code,
            prop: None,
        }
    }

    #[inline]
    #[must_use]
    pub fn with_value(prop_code: PropertyCode, prop: T) -> Self {
        Self {
            prop_code,
            prop: Some(prop),
        }
    }

    #[inline]
    #[must_use]
    pub fn prop_code(&self) -> PropertyCode {
        self.prop_code
    }

    #[inline]
    #[must_use]
    pub fn value(&self) -> Option<&T> {
        self.prop.as_ref()
    }

    #[inline]
    #[must_use]
    pub fn into_value(self) -> Option<T> {
        self.prop
    }
}

impl<T: DecodableProperty> DecodableProperty for MaybeEmptyProperty<T> {
    type Error = T::Error;

    fn decode_edt(epc: PropertyCode, edt: &[u8]) -> Result<Self, Self::Error> {
        if edt.is_empty() {
            return Ok(Self {
                prop_code: epc,
                prop: None,
            });
        }
        T::decode_edt(epc, edt).map(|prop| Self::with_value(epc, prop))
    }
}

impl<T: EncodableProperty> EncodableProperty for MaybeEmptyProperty<T> {
    #[inline]
    fn encode<W: io::Write>(&self, writer: &mut W) -> io::Result<usize> {
        match &self.prop {
            Some(prop) => prop.encode(writer),
            None => writer.write_all(&[self.prop_code.raw(), 0]).map(|()| 2),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct UnprocessedProperty<'a> {
    /// EPC: ECHONET Lite property code.
    prop_code: PropertyCode,
    /// EDT: ECHONET Lite value data.
    ///
    /// Note that this value implicitly provides PDC (property data counter) as
    /// `self.edt.len()`.
    data: &'a [u8],
}

impl<'a> UnprocessedProperty<'a> {
    #[inline]
    #[must_use]
    pub fn new(prop_code: PropertyCode, data: &'a [u8]) -> Self {
        Self { prop_code, data }
    }

    #[inline]
    #[must_use]
    pub fn prop_code(&self) -> PropertyCode {
        self.prop_code
    }

    #[inline]
    #[must_use]
    pub fn data(&self) -> &'a [u8] {
        self.data
    }

    #[inline]
    pub fn decode_as<T: DecodableProperty>(&self) -> Result<T, T::Error> {
        T::decode_edt(self.prop_code, self.data)
    }

    /// Returns `true` if the data is empty.
    #[inline]
    #[must_use]
    pub fn is_empty(&self) -> bool {
        self.data.is_empty()
    }
}
