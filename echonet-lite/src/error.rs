//! Error types.

use std::io;

use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("I/O error")]
    Io(#[from] io::Error),
    #[error("packet error")]
    Packet(#[from] PacketError),
    #[error("frame error")]
    Frame(#[from] FrameError),
}

impl From<PacketErrorKind> for Error {
    #[inline]
    fn from(kind: PacketErrorKind) -> Self {
        Self::Packet(kind.into())
    }
}

impl From<FrameErrorKind> for Error {
    #[inline]
    fn from(kind: FrameErrorKind) -> Self {
        Self::Frame(kind.into())
    }
}

#[derive(Debug, Error)]
#[error("{kind}")]
pub struct PropertyParseError {
    kind: PropertyParseErrorKind,
    source: Option<Box<dyn std::error::Error + Send + Sync + 'static>>,
}

impl PropertyParseError {
    #[inline]
    #[must_use]
    pub(crate) fn new_invalid_edt(
        source: impl Into<Box<dyn std::error::Error + Send + Sync + 'static>>,
    ) -> Self {
        Self {
            kind: PropertyParseErrorKind::InvalidEdt,
            source: Some(source.into()),
        }
    }
}

impl From<PropertyParseErrorKind> for PropertyParseError {
    #[inline]
    fn from(kind: PropertyParseErrorKind) -> Self {
        Self { kind, source: None }
    }
}

#[derive(Debug, Clone, Error)]
pub(crate) enum PropertyParseErrorKind {
    #[error("property should be longer than or equal to 2 bytes")]
    PropertyLessThan2Bytes,
    #[error("invalid property code")]
    InvalidPropertyCode,
    #[error("failed to parse EDT (ECHONET Lite property data)")]
    InvalidEdt,
}

#[derive(Debug, Error)]
#[error("{kind}")]
pub struct PacketError {
    kind: PacketErrorKind,
}

impl From<PacketErrorKind> for PacketError {
    #[inline]
    fn from(kind: PacketErrorKind) -> Self {
        Self { kind }
    }
}

#[derive(Debug, Clone, Error)]
pub(crate) enum PacketErrorKind {
    #[error("invalid packet header")]
    InvalidHeader,
    #[error("too many properties")]
    TooManyProperties,
    #[error("unexpected packet end")]
    UnexpectedBodyEnd,
}

#[derive(Debug, Error)]
#[error("{kind}")]
pub struct FrameError {
    kind: FrameErrorKind,
}

impl From<FrameErrorKind> for FrameError {
    #[inline]
    fn from(kind: FrameErrorKind) -> Self {
        Self { kind }
    }
}

#[derive(Debug, Clone, Error)]
pub(crate) enum FrameErrorKind {
    #[error("extra data follows")]
    ExtraDataFollows,
    #[error("invalid packet header")]
    InvalidHeader,
    #[error("unexpected frame format")]
    UnexpectedFormat,
}
