//! ECHONET Lite packet.

use std::fmt;

use crate::error::{PacketError, PacketErrorKind};
use crate::property::{PropertyCode, UnprocessedProperty};
use crate::service::ServiceCode;

/// Header of ECHONET Lite data with the [format 1 (specified message format)].
///
/// [`Format1`]: `PacketFormat::Format1`
#[derive(Clone, Copy, PartialEq, Eq)]
pub struct PacketHeader {
    /// SEOJ: Source ECHONET Lite object specification.
    source_obj: [u8; 3],
    /// DEOJ: Destination ECHONET Lite object specification.
    dest_obj: [u8; 3],
    /// ESV: ECHONET Lite service.
    service: ServiceCode,
    /// OPC: Object property counter.
    num_props: u8,
}

impl fmt::Debug for PacketHeader {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("PacketHeader")
            .field("source_obj", &self.source_object())
            .field("dest_obj", &self.dest_object())
            .field("service", &self.service)
            .field("num_props", &self.num_props)
            .finish()
    }
}

impl PacketHeader {
    /// Parses the packet header from EDATA (ECHONET Lite data), and returns the
    /// pair of consumed length and the header.
    ///
    /// Note that the consumed length will be always `8`.
    ///
    /// If the parsing failed, returns `None`. The reason for failure will be one of:
    ///
    /// * input is too short, or
    /// * ESV (ECHONET Lite service) value is invalid.
    pub fn parse_frame_data(input: &[u8]) -> Option<(usize, Self)> {
        const HEADER_LENGTH: usize = 8;

        let bytes = input.get(..HEADER_LENGTH)?;
        let source_obj = [bytes[0], bytes[1], bytes[2]];
        let dest_obj = [bytes[3], bytes[4], bytes[5]];
        let service = ServiceCode::from_raw(bytes[6])?;
        let num_props = bytes[7];

        Some((
            HEADER_LENGTH,
            Self {
                source_obj,
                dest_obj,
                service,
                num_props,
            },
        ))
    }
}

impl PacketHeader {
    /// Returns SEOJ, the source object.
    #[inline]
    #[must_use]
    pub fn source_object(&self) -> Object {
        Object::from(self.source_obj)
    }

    /// Returns DEOJ, the destination object.
    #[inline]
    #[must_use]
    pub fn dest_object(&self) -> Object {
        Object::from(self.dest_obj)
    }

    /// Returns ESV, the service code.
    #[inline]
    #[must_use]
    pub fn service(&self) -> ServiceCode {
        self.service
    }

    /// Returns OPC, the number of properties.
    #[inline]
    #[must_use]
    pub fn num_props(&self) -> u8 {
        self.num_props
    }
}

/// ECHONET Lite data with the [format 1 (specified message format)].
///
/// [`Format1`]: `PacketFormat::Format1`
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Packet<'a> {
    /// Packet header.
    header: PacketHeader,
    /// Packet body, i.e. an array of properties.
    props: &'a [u8],
}

impl<'a> Packet<'a> {
    pub fn parse_frame_data(frame_data: &'a [u8]) -> Result<(usize, Self), PacketError> {
        let (header_size, header) =
            PacketHeader::parse_frame_data(frame_data).ok_or(PacketErrorKind::InvalidHeader)?;
        let after_header = &frame_data[header_size..];

        let count = usize::from(header.num_props);
        let mut cursor = 0;
        for _ in 0..count {
            let opc = *after_header
                .get(cursor + 1)
                .ok_or(PacketErrorKind::UnexpectedBodyEnd)?;
            // 2: EPC (1 byte) and OPC (1 byte).
            cursor += 2 + usize::from(opc);
        }

        let this = Self {
            header,
            props: &after_header[..cursor],
        };
        Ok((header_size + cursor, this))
    }
}

impl Packet<'_> {
    /// Returns the packet header.
    #[inline]
    #[must_use]
    pub fn header(&self) -> PacketHeader {
        self.header
    }

    /// Returns SEOJ, the source object.
    #[inline]
    #[must_use]
    pub fn source_object(&self) -> Object {
        self.header.source_object()
    }

    /// Returns DEOJ, the destination object.
    #[inline]
    #[must_use]
    pub fn dest_object(&self) -> Object {
        self.header.dest_object()
    }

    /// Returns ESV, the service code.
    #[inline]
    #[must_use]
    pub fn service(&self) -> ServiceCode {
        self.header.service()
    }

    /// Returns OPC, the number of properties.
    #[inline]
    #[must_use]
    pub fn num_props(&self) -> u8 {
        self.header.num_props()
    }
}

impl<'a> Packet<'a> {
    #[inline]
    #[must_use]
    pub fn properties(&self) -> PropertiesIter<'a> {
        PropertiesIter::new(*self)
    }
}

#[derive(Debug, Clone)]
pub struct PropertiesIter<'a> {
    packet: Packet<'a>,
    cursor: usize,
}

impl<'a> PropertiesIter<'a> {
    #[inline]
    #[must_use]
    fn new(packet: Packet<'a>) -> Self {
        Self { packet, cursor: 0 }
    }
}

impl<'a> Iterator for PropertiesIter<'a> {
    /// EPC (ECHONET Lite property code) and EDT (ECHONET Lite value data).
    type Item = UnprocessedProperty<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.cursor >= self.packet.props.len() {
            return None;
        }

        // OPC.
        let prop_len = *self
            .packet
            .props
            .get(self.cursor + 1)
            .expect("should have been validated when `Packet` value is created");
        let prop_len = usize::from(prop_len);
        // EPC.
        let prop_code = PropertyCode::from_raw(self.packet.props[self.cursor])
            .expect("should have been validated when `Packet` value is created");
        // EDT.
        let data_start = self.cursor + 2;
        let data = self
            .packet
            .props
            .get(data_start..(data_start + prop_len))
            .expect("should have been validated when `Packet` value is created");
        // 2: EPC (1 byte) and OPC (1 byte).
        self.cursor += 2 + prop_len;

        Some(UnprocessedProperty::new(prop_code, data))
    }
}

/// ECHONET Lite object.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Object {
    /// X1: Class group code.
    class_group: u8,
    /// X2: Class code.
    class: u8,
    /// X3: Instance code.
    instance: u8,
}

impl fmt::Debug for Object {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Object[{:02X}, {:02X}, {:02X}]",
            self.class_group, self.class, self.instance
        )
    }
}

impl Object {
    /// Returns the class group code, class code, and instance code.
    ///
    /// In other words, returns the object `[X1. X2] [X3]` of ECHONET Lite
    /// protocol as the array `[x1, x2, x3]` of Rust.
    #[inline]
    #[must_use]
    pub const fn to_array(self) -> [u8; 3] {
        [self.class_group, self.class, self.instance]
    }

    pub fn raw(self) -> [u8; 3] {
        self.to_array()
    }

    #[inline]
    #[must_use]
    pub const fn from_array(v: [u8; 3]) -> Self {
        Self {
            class_group: v[0],
            class: v[1],
            instance: v[2],
        }
    }
}

impl From<[u8; 3]> for Object {
    #[inline]
    fn from(v: [u8; 3]) -> Self {
        Self::from_array(v)
    }
}

/// Known ECHONET Lite packet format.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[non_exhaustive]
pub enum PacketFormat {
    /// Specified message format.
    ///
    /// An alias [`SPECIFIED`][`Self::SPECIFIED`] is defined as this value.
    Format1,
    /// Arbitrary message format.
    ///
    /// An alias [`ARBITRARY`][`Self::ARBITRARY`] is defined as this value.
    Format2,
}

impl PacketFormat {
    /// Meaningful alias name of [`Format1`][`Self::Format1`], specified message format.
    pub const SPECIFIED: Self = Self::Format1;
    /// Meaningful alias name of [`Format2`][`Self::Format2`], arbitrary message format.
    pub const ARBITRARY: Self = Self::Format2;
}
