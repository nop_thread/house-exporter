//! ECHONET Lite frame.

use std::fmt;
use std::io;

use crate::error::FrameErrorKind;
use crate::error::{Error, PacketErrorKind};
use crate::packet::{Object, Packet, PacketFormat};
use crate::property::EncodableProperty;
use crate::service::ServiceCode;

/// ECHONET Lite frame header.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct FrameHeader {
    /// EHD1: ECHONET Lite message header 1.
    header1: FrameHeader1,
    /// EHD2: ECHONET Lite message header 2.
    header2: FrameHeader2,
    /// TID: Transaction ID.
    transaction_id: TransactionId,
}

impl FrameHeader {
    /// Parses the frame header, and returns the pair of consumed length and the header.
    ///
    /// Note that the consumed length will be always `4`.
    ///
    /// If the parsing failed, returns `None`. The reason for failure will be one of:
    ///
    /// * input is too short, or
    /// * EHD1 (ECHONET Lite message header 1) is invalid or unsupported, or
    /// * EHD2 (ECHONET Lite message header 2) is invalid or unsupported.
    pub fn parse(input: &[u8]) -> Option<(usize, Self)> {
        const HEADER_LENGTH: usize = 4;

        let bytes = input.get(..HEADER_LENGTH)?;
        let header1 = FrameHeader1::from_raw(bytes[0])?;
        let header2 = FrameHeader2::from_raw(bytes[1])?;
        let transaction_id = TransactionId(u16::from_be_bytes([bytes[2], bytes[3]]));

        Some((
            HEADER_LENGTH,
            Self {
                header1,
                header2,
                transaction_id,
            },
        ))
    }

    #[inline]
    #[must_use]
    pub fn packet_format(self) -> Option<PacketFormat> {
        self.header2.packet_format()
    }

    #[inline]
    #[must_use]
    pub fn transaction_id(self) -> TransactionId {
        self.transaction_id
    }
}

/// ECHONET Lite frame transaction ID.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TransactionId(u16);

impl TransactionId {
    #[inline]
    #[must_use]
    pub fn new(v: u16) -> Self {
        Self(v)
    }

    #[inline]
    #[must_use]
    pub fn raw(self) -> u16 {
        self.0
    }

    #[inline]
    #[must_use]
    pub fn wrapping_add(self, inc: u16) -> Self {
        Self(self.0.wrapping_add(inc))
    }
}

/// ECHONET Lite frame with a packet of [ECHONET Lite message format 1][`PacketFormat::Format1`].
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct FrameFormat1<'a> {
    header: FrameHeader,
    /// EDATA: ECHONET Lite data.
    packet: Packet<'a>,
}

impl<'a> FrameFormat1<'a> {
    pub fn parse_full(bytes: &'a [u8]) -> Result<Self, Error> {
        let (frame_size, this) = Self::parse(bytes)?;
        if frame_size != bytes.len() {
            return Err(FrameErrorKind::ExtraDataFollows.into());
        }
        Ok(this)
    }

    pub fn parse(bytes: &'a [u8]) -> Result<(usize, Self), Error> {
        let (header_size, header) =
            FrameHeader::parse(bytes).ok_or(FrameErrorKind::InvalidHeader)?;
        if header.packet_format() != Some(PacketFormat::Format1) {
            return Err(FrameErrorKind::UnexpectedFormat.into());
        }
        let frame_data = &bytes[header_size..];
        let (packet_size, packet) = Packet::parse_frame_data(frame_data)?;

        let this = Self { header, packet };
        Ok((header_size + packet_size, this))
    }

    #[inline]
    #[must_use]
    pub fn header(&self) -> FrameHeader {
        self.header
    }

    #[inline]
    #[must_use]
    pub fn transaction_id(&self) -> TransactionId {
        self.header.transaction_id
    }

    #[inline]
    #[must_use]
    pub fn packet(&self) -> &Packet<'a> {
        &self.packet
    }
}

/// EHD1: ECHONET Lite message header 1.
// This is currently (as of ver. 1.13, published 2018-07-06) fixed to
// `0b_0001_0000`, so this type does not need to contain `u8` at runtime for now.
#[derive(Default, Debug, Clone, Copy, PartialEq, Eq)]
struct FrameHeader1(());

impl FrameHeader1 {
    const VALUE: u8 = 0b_0001_0000;

    #[inline]
    #[must_use]
    pub fn new() -> Self {
        Self::default()
    }

    /// Creates a value from the given raw value.
    #[inline]
    #[must_use]
    pub fn from_raw(v: u8) -> Option<Self> {
        if v == Self::VALUE {
            Some(Self::new())
        } else {
            None
        }
    }

    /// Returns the raw value.
    #[inline]
    #[must_use]
    pub fn raw(self) -> u8 {
        Self::VALUE
    }
}

/// EHD2: ECHONET Lite message header 2.
// Currently (as of ver. 1.13, published 2018-07-06), only 0x81 and 0x82 is
// defined.
#[derive(Clone, Copy, PartialEq, Eq)]
struct FrameHeader2(u8);

impl fmt::Debug for FrameHeader2 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut d = f.debug_struct("FrameHeader2");
        match self.packet_format() {
            Some(format) => d.field("field", &format),
            None => d.field("raw", &format_args!("{:#04X}", self.raw())),
        };
        d.finish()
    }
}

impl FrameHeader2 {
    /// Creates a new `Header2` value.
    #[inline]
    #[must_use]
    pub fn new(pf: PacketFormat) -> Self {
        let raw = match pf {
            PacketFormat::Format1 => 0x81,
            PacketFormat::Format2 => 0x82,
        };
        Self(raw)
    }

    /// Creates a value from the given raw value.
    #[inline]
    #[must_use]
    pub fn from_raw(v: u8) -> Option<Self> {
        if matches!(v, 0x81 | 0x82) {
            Some(Self(v))
        } else {
            None
        }
    }

    /// Returns the raw value.
    #[inline]
    #[must_use]
    pub fn raw(self) -> u8 {
        self.0
    }

    #[inline]
    #[must_use]
    pub fn packet_format(self) -> Option<PacketFormat> {
        match self.0 {
            0x81 => Some(PacketFormat::Format1),
            0x82 => Some(PacketFormat::Format2),
            _ => None,
        }
    }
}

#[derive(Debug, Clone)]
pub struct FrameFormat1Builder {
    num_props: u8,
    buf: Vec<u8>,
}

impl FrameFormat1Builder {
    const OFFSET_NUM_PROPS: usize = 11;

    #[inline]
    #[must_use]
    pub fn new<S: Into<ServiceCode>>(
        transaction_id: TransactionId,
        source_obj: Object,
        dest_obj: Object,
        service: S,
    ) -> Self {
        let mut buf = Vec::with_capacity(12);
        let header1 = FrameHeader1::new();
        let header2 = FrameHeader2::new(PacketFormat::SPECIFIED);
        let num_props = 0;
        buf.push(header1.raw());
        buf.push(header2.raw());
        buf.extend_from_slice(&u16::to_be_bytes(transaction_id.raw()));
        buf.extend_from_slice(&source_obj.raw());
        buf.extend_from_slice(&dest_obj.raw());
        buf.push(service.into().raw());
        debug_assert_eq!(buf.len(), Self::OFFSET_NUM_PROPS);
        buf.push(num_props);
        debug_assert_eq!(buf.len(), 12);

        Self { num_props, buf }
    }

    #[inline]
    pub fn write_to<W: io::Write>(&self, writer: &mut W) -> io::Result<usize> {
        writer.write_all(&self.buf).map(|()| self.buf.len())
    }

    #[inline]
    #[must_use]
    pub fn len(&self) -> usize {
        self.buf.len()
    }

    #[inline]
    #[must_use]
    pub fn is_empty(&self) -> bool {
        self.num_props == 0
    }

    pub fn push_prop<T: EncodableProperty>(&mut self, prop: &T) -> Result<usize, Error> {
        let new_num_props = match self.num_props.checked_add(1) {
            Some(v) => v,
            None => return Err(PacketErrorKind::TooManyProperties.into()),
        };
        let old_len = self.buf.len();
        match prop.encode(&mut self.buf) {
            Ok(written) => {
                debug_assert_eq!(written, self.buf.len() - old_len);
                self.buf[Self::OFFSET_NUM_PROPS] = new_num_props;
                self.num_props = new_num_props;
                Ok(written)
            }
            Err(e) => {
                self.buf.truncate(old_len);
                Err(e.into())
            }
        }
    }
}
