//! Powermeter for HEMS (Home Energy Management System).

use once_cell::sync::Lazy;
use prometheus::GaugeVec;

const VARIABLE_LABELS: &[&str] = &["house"];

pub static PURCHASED_ENERGY_GAUGE: Lazy<GaugeVec> = Lazy::new(|| {
    let opts = prometheus::Opts::new("purchased_energy_joules_total", "Purchased energy")
        .namespace("house")
        .subsystem("electric_energy_contract");
    GaugeVec::new(opts, VARIABLE_LABELS).expect("failed to create purchased energy gauge")
});
pub static SOLD_ENERGY_GAUGE: Lazy<GaugeVec> = Lazy::new(|| {
    let opts = prometheus::Opts::new("sold_energy_joules_total", "Sold energy")
        .namespace("house")
        .subsystem("electric_energy_contract");
    GaugeVec::new(opts, VARIABLE_LABELS).expect("failed to create sold energy gauge")
});
pub static INSTANT_CONSUMED_ENERGY_GAUGE: Lazy<GaugeVec> = Lazy::new(|| {
    // This instant value cannot be accumulated precisely, so use Watts instead
    // of Joules.
    let opts = prometheus::Opts::new("instant_consumed_energy_watts", "Instant consumed energy")
        .namespace("house")
        .subsystem("electric_energy_contract");
    GaugeVec::new(opts, VARIABLE_LABELS).expect("failed to create instant consumed energy gauge")
});
pub static INSTANT_CONSUMED_CURRENT_PHASE_R_GAUGE: Lazy<GaugeVec> = Lazy::new(|| {
    let opts = prometheus::Opts::new(
        "instant_consumed_current_phase_r_amperes",
        "Instant consumed phase-R current",
    )
    .namespace("house")
    .subsystem("electric_energy_contract");
    GaugeVec::new(opts, VARIABLE_LABELS)
        .expect("failed to create instant consumed phase-R current gauge")
});
pub static INSTANT_CONSUMED_CURRENT_PHASE_T_GAUGE: Lazy<GaugeVec> = Lazy::new(|| {
    let opts = prometheus::Opts::new(
        "instant_consumed_current_phase_t_amperes",
        "Instant consumed phase-T current",
    )
    .namespace("house")
    .subsystem("electric_energy_contract");
    GaugeVec::new(opts, VARIABLE_LABELS)
        .expect("failed to create instant consumed phase-T current gauge")
});
