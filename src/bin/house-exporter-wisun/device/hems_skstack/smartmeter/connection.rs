//! Low-voltage smartmeter.
//!
//! See <https://echonet.jp/spec-en/> or
//! <https://echonet.jp/wp/wp-content/uploads/pdf/General/Standard/AIF/lvsm/lvsm_aif_ver1.01_e.pdf>.

use std::net::Ipv6Addr;

use anyhow::{bail, Context as _};
use elprops_smartmeter as el_props;
use skstack::{Event, SkPort, UdpSendSecurity};

use echonet_lite::frame::{FrameFormat1Builder, TransactionId};
use echonet_lite::packet::Object;

pub use self::el_props::SupportedProperty;

/// Object code for the low-voltage smart electric energy meters.
pub const EOJ_SMARTMETER: Object = Object::from_array([0x02, 0x88, 0x01]);

/// Object code for the HEMS controller.
pub const EOJ_HEMS_CONTROLLER: Object = Object::from_array([0x05, 0xff, 0x01]);

#[derive(Debug)]
pub struct Scanner(());

impl Scanner {
    pub async fn setup(skport: &mut SkPort, auth_id: &str, password: &str) -> anyhow::Result<Self> {
        // Disable echoback.
        skport.wisun_port().disable_echoback().await?;

        // Set auth ID.
        skport
            .set_route_b_id(auth_id)
            .await
            .context("failed to set smartmeter auth ID")?;
        tracing::trace!("successfully set smartmeter auth ID");

        // Set password.
        skport
            .set_password(password)
            .await
            .context("failed to set smartmeter connection password")?;
        tracing::trace!("successfully set smartmeter connection password");

        Ok(Self(()))
    }

    pub async fn scan(
        &mut self,
        skport: &mut SkPort,
        scan_duration: u8,
    ) -> anyhow::Result<Option<ConnectionBuilderParams>> {
        // Scan smartmeters.
        // TODO: Change duration shorter to longer on scan failure.
        let pandescs = skport
            .scan_active(None, scan_duration)
            .await
            .context("failed to scan smartmeters")?;
        tracing::trace!("pandescs = {pandescs:?}");
        let pandesc = if pandescs.is_empty() {
            tracing::debug!("smartmeters not found");
            return Ok(None);
        } else if pandescs.len() > 1 {
            bail!(
                "multiple smartmeters with the same auth info found unexpectedly ({:?})",
                pandescs
            );
        } else {
            pandescs[0]
        };
        tracing::info!(
            "LQI of the smartmeter is {} (= RSSI {} dBm)",
            pandesc.lqi,
            pandesc.rssi()
        );
        Ok(Some(ConnectionBuilderParams {
            channel: pandesc.channel,
            pan_id: pandesc.pan_id,
            eui64_addr: pandesc.addr,
        }))
    }
}

#[derive(Debug)]
pub struct ConnectionBuilderParams {
    channel: u8,
    pan_id: u16,
    eui64_addr: u64,
}

impl ConnectionBuilderParams {
    pub async fn setup_builder(&self, skport: &mut SkPort) -> anyhow::Result<ConnectionBuilder> {
        // Set channel.
        skport
            .set_register_u8("S2", self.channel)
            .await
            .context("failed to set channel")?;
        tracing::debug!("successfully set S2 (channel)");

        // Set pan ID.
        skport
            .set_register_u16("S3", self.pan_id)
            .await
            .context("failed to set pan ID")?;
        tracing::debug!("successfully set S3 (pan ID)");

        // Convert the EUI-64 addr of the smartmeter to IPv6 addr.
        let remote_addr = skport
            .conv_mac_addr_to_lla64(self.eui64_addr)
            .await
            .context("failed to convert EUI-64 addr to IPv6 addr")?;
        tracing::debug!("IPv6 addr of the smartmeter is {remote_addr:?}");

        Ok(ConnectionBuilder { remote_addr })
    }
}

#[derive(Debug)]
pub struct ConnectionBuilder {
    remote_addr: Ipv6Addr,
}

impl ConnectionBuilder {
    // TODO: Make cancellation safe?
    pub async fn connect(
        &self,
        skport: &mut Option<SkPort>,
    ) -> anyhow::Result<SmartmeterConnection> {
        // Start PANA auth.
        let skport_ = skport
            .as_mut()
            .context("[precondition] `skport` parameter should be `Some(_)`")?;
        skport_
            .join(self.remote_addr)
            .await
            .context("failed to connect to the smartmeter")?;
        tracing::trace!("successfully connected to the smartmeter");

        Ok(SmartmeterConnection {
            skport: skport
                .take()
                .expect("[consistency] `skport` is already checked to be `Some(_)`"),
            remote_addr: self.remote_addr,
            next_transaction_id: TransactionId::new(0),
        })
    }
}

#[derive(Debug)]
pub struct SmartmeterConnection {
    skport: SkPort,
    remote_addr: Ipv6Addr,
    next_transaction_id: TransactionId,
}

impl SmartmeterConnection {
    // TODO: Make cancel-safe.
    pub async fn reconnect(&mut self) -> anyhow::Result<()> {
        // TODO: Is it allowed to send `SKREJOIN` command after the session is expired or closed?
        self.skport
            .rejoin()
            .await
            .context("failed to extend lifetime of the PANA session between the smartmeter")?;
        tracing::debug!(
            "successfully extended lifetime of the PANA session between the smartmeter"
        );
        Ok(())
    }

    #[inline]
    pub async fn recv_event(&mut self) -> skstack::Result<Event> {
        self.skport.recv_event().await
    }

    #[inline]
    #[must_use]
    pub fn remote_addr(&self) -> Ipv6Addr {
        self.remote_addr
    }

    /// Generates a new transaction ID and updates the transaction ID counter.
    #[inline]
    #[must_use]
    pub fn generate_transaction_id(&mut self) -> TransactionId {
        let next = self.next_transaction_id.wrapping_add(1);
        std::mem::replace(&mut self.next_transaction_id, next)
    }

    pub async fn send_udp(&mut self, data: &[u8]) -> skstack::Result<()> {
        const ECHONET_LITE_PORT: u16 = 3610;
        let handle = 1;
        self.skport
            .send_udp(
                handle,
                self.remote_addr(),
                ECHONET_LITE_PORT,
                UdpSendSecurity::PreferSecure,
                data,
            )
            .await
    }

    pub async fn send_el_frame1(&mut self, request: &FrameFormat1Builder) -> anyhow::Result<()> {
        let mut buf = Vec::with_capacity(request.len());
        request
            .write_to(&mut buf)
            .context("failed to build an ECHONET Lite frame for request")?;
        self.send_udp(&buf).await.map_err(Into::into)
    }
}
