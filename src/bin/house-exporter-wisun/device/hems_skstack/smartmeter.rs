//! Home power management info from smart meter, using B-route.

mod connection;

use std::borrow::Cow;
use std::sync::Arc;
use std::time::Duration;

use anyhow::{anyhow, bail, Context as _};
use async_trait::async_trait;
use echonet_lite::frame::{FrameFormat1, FrameFormat1Builder, TransactionId};
use echonet_lite::packet::Packet;
use echonet_lite::property::{EmptyProperty, MaybeEmptyProperty};
use echonet_lite::service::KnownServiceCode;
use elprops_smartmeter::{
    CumulativeEnergyCoefficient, CumulativeEnergyEffectiveDigits, CumulativeEnergyNormal,
    CumulativeEnergyReverse, CumulativeEnergyUnit, InstantCurrents, InstantEnergy,
    KnownPropertyCode,
};
use futures::future::FutureExt as _;
use serde::Deserialize;
use skstack::{Event, EventNotification, EventNotificationContent, EventRxUdp, SkPort};
use thiserror::Error;
use tokio::time::Instant;

use self::connection::{SmartmeterConnection, SupportedProperty};

use crate::metrics::hems::powermeter::{
    INSTANT_CONSUMED_CURRENT_PHASE_R_GAUGE, INSTANT_CONSUMED_CURRENT_PHASE_T_GAUGE,
    INSTANT_CONSUMED_ENERGY_GAUGE, PURCHASED_ENERGY_GAUGE, SOLD_ENERGY_GAUGE,
};
use crate::Config;

/// The minimum time required for HEMS controller to wait for the property when OPC is 1.
///
/// OPC is properties count.
const MIN_WAIT_RESPONSE_SINGLE_PROP: Duration = Duration::from_secs(20);
/// The minimum time required for HEMS controller to wait for the property when OPC is more than 1.
///
/// OPC is properties count.
const MIN_WAIT_RESPONSE_MULTIPLE_PROPS: Duration = Duration::from_secs(60);

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct HemsSkstackSmartmeterConfig {
    house: String,
    device: String,
    baudrate: u32,
    auth_id: String,
    password: String,
}

pub async fn watch_retry(config: Arc<Config>) {
    if config.hems_skstack_smartmeter.is_empty() {
        return;
    }

    let tasks: Vec<_> = config
        .hems_skstack_smartmeter
        .iter()
        .map(|conf| watch_single_powermeter_retry(conf.clone()).boxed())
        .collect::<Vec<_>>();
    futures::future::select_all(tasks).await.0
}

async fn watch_single_powermeter_retry(config: HemsSkstackSmartmeterConfig) {
    const RETRY_DURATION: std::time::Duration = std::time::Duration::from_secs(3);
    loop {
        match watch_single_powermeter(config.clone()).await {
            Ok(()) => break,
            Err(e) => {
                tracing::error!("powermeter watcher failed: {e:?}");
                tokio::time::sleep(RETRY_DURATION).await;
                tracing::info!("restarting powermeter watcher");
            }
        }
    }
}

async fn watch_single_powermeter(config: HemsSkstackSmartmeterConfig) -> anyhow::Result<()> {
    let skport = SkPort::open(config.device.clone(), config.baudrate)
        .with_context(|| format!("failed to open WiSUN adapter {:?}", config.device))?;
    let mut conn = connect(skport, &config)
        .await
        .context("failed to connect to a smartmeter")?;
    tracing::debug!(connection = ?conn);

    // Check if the remote device is actually a power meter.
    ensure_remote_is_smartmeter(&mut conn)
        .await
        .context("expected smartmeter as the remote device")?;

    let params = InitialData::fetch(&mut conn)
        .await
        .context("failed to get parameters to interpret data from smartmeter")?;
    tracing::debug!("received initial data {:?}", params);

    let metrics_setter = Arc::new(MetricsSetter::new(config.house.clone(), params));
    let _outdated_metrics_dropper = tokio::task::spawn({
        let metrics_setter = metrics_setter.clone();
        async move {
            const EXPIRATION_CHECK_PERIOD: Duration = Duration::from_secs(5);

            let mut interval = tokio::time::interval(EXPIRATION_CHECK_PERIOD);
            while Arc::strong_count(&metrics_setter) > 1 {
                interval.tick().await;
                metrics_setter.expire_too_old().await;
            }
        }
    });
    drop(_outdated_metrics_dropper);

    // Start the main loop.
    let mut last_auth = Instant::now();
    let mut reauth_failure = 0;
    loop {
        const NO_REAUTH_DURATION: Duration = Duration::from_secs(18 * 3600);
        const MAX_REAUTH_TRY_COUNT: usize = 5;

        let data = match PowerMetrics::fetch(&mut conn).await {
            Ok(v) => v,
            Err(FetchPowerMetricsError::UdpSend) => {
                tracing::trace!("failed to send UDP packet. retrying soon");
                continue;
            }
            Err(e) => {
                metrics_setter.unregister_all();
                return Err(
                    anyhow::Error::new(e).context("failed to get power metrics from smartmeter")
                );
            }
        };
        metrics_setter.update(data).await;

        let since_last_auth = data
            .map_or_else(Instant::now, |data| data.time)
            .duration_since(last_auth);
        if since_last_auth > NO_REAUTH_DURATION {
            tracing::debug!(
                "attempt to extend the PANA session lifetime: \
                 {} seconds since the last PANA (re)auth",
                since_last_auth.as_secs()
            );
            match conn.reconnect().await {
                Ok(()) => {
                    reauth_failure = 0;
                    last_auth = Instant::now();
                }
                Err(_) => {
                    reauth_failure += 1;
                }
            }
        }
        if reauth_failure > MAX_REAUTH_TRY_COUNT {
            // Too many reauth attempts and all failed.
            bail!("cannot update the PANA session: {reauth_failure} attempts failed");
        }
    }
}

async fn connect(
    mut skport: SkPort,
    config: &HemsSkstackSmartmeterConfig,
) -> anyhow::Result<SmartmeterConnection> {
    // Set up for scan.
    let mut scanner = {
        const MAX_RETRIES: usize = 4;
        const TIMEOUT: Duration = Duration::from_secs(4);
        let auth_id = config
            .auth_id
            .chars()
            .filter(|c| c.is_ascii_hexdigit())
            .collect::<String>();
        let password = config
            .password
            .chars()
            .filter(|c| c.is_ascii_alphanumeric())
            .collect::<String>();
        let mut retry_count = 0;
        loop {
            match tokio::time::timeout(
                TIMEOUT,
                connection::Scanner::setup(&mut skport, &auth_id, &password),
            )
            .await
            {
                Ok(v) => break v?,
                Err(_) => {
                    retry_count += 1;
                    if retry_count > MAX_RETRIES {
                        bail!("failed to find the target smartmeter");
                    }
                }
            }
        }
    };

    // Scan.
    let params = {
        let mut retry_count = 0;
        const SCAN_DURATION_PARAMS: &[u8] = &[6, 6, 7, 7, 8, 8, 9, 10];
        let mut scan_duration_param_iter = SCAN_DURATION_PARAMS.iter().copied();
        loop {
            let scan_duration_param = match scan_duration_param_iter.next() {
                Some(v) => v,
                None => bail!("failed to find the target smartmeter"),
            };
            let scan_duration =
                Duration::from_secs_f64(f64::from((1 << scan_duration_param) + 1) / 100.0);
            tracing::debug!(
                retry_count,
                scan_duration_param,
                ?scan_duration,
                "scanning smartmeter"
            );
            // Currently, the scan is not cancellation safe.
            // This calculation seems to be wrong... Roughly:
            // * dur=6 : 9--40 sec
            // * dur=7 : 21--35 sec
            // * dur=8 : 129--129 sec
            let timeout = Duration::from_secs(150);
            match tokio::time::timeout(timeout, scanner.scan(&mut skport, scan_duration_param))
                .await
            {
                Ok(Ok(Some(v))) => break v,
                Ok(Err(e)) => return Err(e),
                Ok(Ok(None)) => {
                    retry_count += 1;
                }
                Err(_) => {
                    // Currently, the scan is not cancellation safe.
                    // If timeout happens, restart by closing the port.
                    // This can happen when the adapter returned no response
                    // (usually `OK\r\n`) to the sent command.
                    bail!("scan timeout, maybe the adapter sabotaged");
                }
            }
            // Wait before retrying immediately.
            // Immediate retry seems to make the smartmeter busy and make the
            // scan more fallible.
            // MEMO: 4 seconds delay are too small?
            let long_wait = Duration::from_secs(6);
            tracing::trace!("stopping scan for {} seconds", long_wait.as_secs_f64());
            tokio::time::sleep(long_wait).await;
        }
    };
    tracing::trace!(?params, "smartmeter found");

    // Set up for connection.
    let builder = {
        const MAX_RETRIES: usize = 4;
        const TIMEOUT: Duration = Duration::from_secs(3);
        let mut retry_count = 0;
        loop {
            match tokio::time::timeout(TIMEOUT, params.setup_builder(&mut skport)).await {
                Ok(v) => break v?,
                Err(_) => {
                    retry_count += 1;
                    if retry_count > MAX_RETRIES {
                        bail!("failed to set up for a new connection");
                    }
                }
            }
        }
    };

    let conn = {
        const MAX_RETRIES: usize = 4;
        //const TIMEOUT: Duration = Duration::from_secs(20);
        const TIMEOUT: Duration = Duration::from_secs(60);
        let mut retry_count = 0;
        let mut skport = Some(skport);
        loop {
            debug_assert!(skport.is_some());
            tracing::trace!(?retry_count, "connecting to the smartmeter");
            match tokio::time::timeout(TIMEOUT, builder.connect(&mut skport)).await {
                //Ok(v) => break v?,
                Ok(Ok(v)) => {
                    tracing::debug!("connected: {v:?}");
                    break v;
                }
                Ok(Err(e)) => {
                    tracing::debug!("failed to connect: {e}");
                    return Err(e);
                }
                Err(_) => {
                    tracing::debug!("connection start TIMEOUT");
                    retry_count += 1;
                    if retry_count > MAX_RETRIES {
                        bail!("failed to set up for a new connection");
                    }
                }
            }
        }
    };

    tracing::info!("successfully created a connection to a (maybe) powermeter device");

    Ok(conn)
}

pub async fn ensure_remote_is_smartmeter(conn: &mut SmartmeterConnection) -> anyhow::Result<()> {
    // TODO: How to increase transaction ID?

    // First try: receive instance list actively sent by the remote.
    let mut handler = CheckIfSmartmeter::new();
    {
        let first_try = async {
            loop {
                let ev = conn.recv_event().await.context("failed to receive event")?;
                match handler.sk_event(ev).await {
                    Some(res) => return res,
                    None => continue,
                }
            }
        };
        let first_timeout = Duration::from_secs(10);
        match tokio::time::timeout(first_timeout, first_try).await {
            Ok(Ok(Some(true))) => {
                tracing::debug!("the remote device can behave as a smartmeter");
                return Ok(());
            }
            Ok(Ok(Some(false))) => {
                bail!("the remote device is not a smartmeter");
            }
            Ok(Ok(None)) => {
                tracing::warn!(
                    "cannot check if the remote device can behave as a smartmeter. assuming it can"
                );
                return Ok(());
            }
            Ok(Err(e)) => return Err(e),
            Err(_) => {}
        }
    }

    // Could not receive automatically-sent instance list.
    // Send a request and wait for the response.
    tracing::debug!("instance list not received. sending request");

    // Second try: request the instance list, and wait for the response.
    let transaction_id = TransactionId::new(1);
    // Use `INF_REQ` to request instance list. See the section 6.6 of the
    // "ECHONET Lite System Design Guidelines 2nd edition".
    let mut request = FrameFormat1Builder::new(
        transaction_id,
        connection::EOJ_HEMS_CONTROLLER,
        connection::EOJ_SMARTMETER,
        KnownServiceCode::NotifyRequest,
    );
    request
        .push_prop(&EmptyProperty::new(KnownPropertyCode::InstanceList.into()))
        .context("failed to write a property into a packet")?;
    conn.send_el_frame1(&request).await?;

    handler.request_sent = true;
    {
        let second_try = async {
            loop {
                let ev = conn.recv_event().await.context("failed to receive event")?;
                match handler.sk_event(ev).await {
                    Some(res) => return res,
                    None => continue,
                }
            }
        };
        match tokio::time::timeout(MIN_WAIT_RESPONSE_SINGLE_PROP, second_try).await {
            Ok(Ok(Some(true))) => {
                tracing::debug!("the remote device can behave as a smartmeter");
                return Ok(());
            }
            Ok(Ok(Some(false))) => {
                bail!("the remote device is not a smartmeter");
            }
            Ok(Ok(None)) => {
                tracing::warn!(
                    "cannot check if the remote device can behave as a smartmeter. assuming it can"
                );
                return Ok(());
            }
            Ok(Err(e)) => return Err(e),
            Err(_) => {}
        }
    }

    Ok(())
}

#[derive(Debug)]
struct MetricsSetter {
    house: String,
    params: InitialData,
    prev_time: tokio::sync::Mutex<Instant>,
}

impl MetricsSetter {
    #[inline]
    #[must_use]
    fn new(house: String, params: InitialData) -> Self {
        Self {
            house,
            params,
            prev_time: tokio::sync::Mutex::new(Instant::now()),
        }
    }

    fn unregister_all(&self) {
        self.unregister_purchased_energy();
        self.unregister_sold_energy();
        self.unregister_instant_consumed_energy();
        self.unregister_instant_consumed_current_phase_r();
        self.unregister_instant_consumed_current_phase_t();
    }

    pub async fn expire_too_old(&self) {
        const NO_DATA_TIMEOUT: Duration = Duration::from_secs(30);

        // Not so important. Do not wait for long time.
        let get_prev_time = async {
            let lock = self.prev_time.lock().await;
            *lock
        };
        let prev_time =
            match tokio::time::timeout(Duration::from_millis(1_500), get_prev_time).await {
                Ok(v) => v,
                Err(_) => {
                    tracing::warn!("cannot lock `MetricsSetter::prev_time`");
                    return;
                }
            };

        if Instant::now().duration_since(prev_time) > NO_DATA_TIMEOUT {
            // Set all properties to "NO DATA".
            self.unregister_all();
        }
    }

    async fn update(&self, data: Option<PowerMetrics>) {
        let data = match data {
            Some(v) => v,
            None => {
                // Set all properties to "NO DATA".
                self.unregister_all();
                return;
            }
        };
        tracing::trace!("setting metrics for {:?}", data);
        let labels = &[self.house.as_str()];

        let ce_normal = data.ce_normal.to_joules(
            self.params.ce_coefficient,
            self.params.ce_unit,
            self.params.ce_effective_digits,
        );
        match ce_normal {
            Some(v) => PURCHASED_ENERGY_GAUGE
                .get_metric_with_label_values(labels)
                .expect("number of labels should be adequate")
                .set(v),
            None => self.unregister_purchased_energy(),
        }

        let ce_reverse = data.ce_reverse.and_then(|v| {
            v.to_joules(
                self.params.ce_coefficient,
                self.params.ce_unit,
                self.params.ce_effective_digits,
            )
        });
        match ce_reverse {
            Some(v) => SOLD_ENERGY_GAUGE
                .get_metric_with_label_values(labels)
                .expect("number of labels should be adequate")
                .set(v),
            None => self.unregister_sold_energy(),
        }

        match data.instant_energy.watt() {
            Some(v) => INSTANT_CONSUMED_ENERGY_GAUGE
                .get_metric_with_label_values(labels)
                .expect("number of labels should be adequate")
                .set(f64::from(v)),
            None => self.unregister_instant_consumed_energy(),
        }

        match data.instant_currents.r_ampere().into_available() {
            Some(v) => INSTANT_CONSUMED_CURRENT_PHASE_R_GAUGE
                .get_metric_with_label_values(labels)
                .expect("number of labels should be adequate")
                .set(v),
            None => self.unregister_instant_consumed_current_phase_r(),
        }

        match data.instant_currents.t_ampere().into_available() {
            Some(v) => INSTANT_CONSUMED_CURRENT_PHASE_T_GAUGE
                .get_metric_with_label_values(labels)
                .expect("number of labels should be adequate")
                .set(v),
            None => self.unregister_instant_consumed_current_phase_t(),
        }

        // Not so important. Do not wait for long time.
        let update_time = async {
            let mut lock = self.prev_time.lock().await;
            *lock = data.time;
        };
        if tokio::time::timeout(Duration::from_millis(1_500), update_time)
            .await
            .is_err()
        {
            tracing::warn!("cannot lock `MetricsSetter::prev_time`");
        }
    }
}

macro_rules! impl_unregister {
    ($unregisterer:ident, $collector:ident) => {
        impl MetricsSetter {
            fn $unregisterer(&self) {
                // Error will be returned when already unregistered.
                let _ = $collector.remove_label_values(&[self.house.as_str()]);
            }
        }
    };
}

impl_unregister!(unregister_purchased_energy, PURCHASED_ENERGY_GAUGE);
impl_unregister!(unregister_sold_energy, SOLD_ENERGY_GAUGE);
impl_unregister!(
    unregister_instant_consumed_energy,
    INSTANT_CONSUMED_ENERGY_GAUGE
);
impl_unregister!(
    unregister_instant_consumed_current_phase_r,
    INSTANT_CONSUMED_CURRENT_PHASE_R_GAUGE
);
impl_unregister!(
    unregister_instant_consumed_current_phase_t,
    INSTANT_CONSUMED_CURRENT_PHASE_T_GAUGE
);

#[async_trait]
trait HandleEvent {
    type Result: 'static;

    /// Returns the handler name and state.
    fn handler_name_state(&self) -> (Cow<'_, str>, Cow<'_, str>);

    async fn sk_event(&mut self, ev: Event) -> Option<Self::Result> {
        match ev {
            Event::RxUdp(ev) => self.sk_receive_udp(ev).await,
            Event::PanDesc(ev) => self.sk_pandesc(ev).await,
            Event::Event(ev) => self.sk_notification(ev).await,
        }
    }
    async fn sk_receive_udp(&mut self, ev: skstack::EventRxUdp) -> Option<Self::Result> {
        let EventRxUdp { data, .. } = ev;
        match FrameFormat1::parse(&data) {
            Ok((size, frame)) => {
                if size != data.len() {
                    let (name, state) = self.handler_name_state();
                    tracing::warn!(
                        handler = &*name,
                        state = &*state,
                        "extra data found after the ECHONET Lite frame format 1: data = {:02x?}",
                        data
                    );
                    // The prefix is still valid, so continue processing.
                }
                self.el_frame_format1(frame).await
            }
            Err(e) => self.el_frame_unknown(&data, e).await,
        }
    }
    async fn sk_pandesc(&mut self, ev: skstack::EventPanDesc) -> Option<Self::Result> {
        let (name, state) = self.handler_name_state();
        tracing::trace!(
            handler = &*name,
            state = &*state,
            "ignoring skstack PANDESC event {:?}",
            ev
        );
        None
    }
    async fn sk_notification(&mut self, ev: skstack::EventNotification) -> Option<Self::Result> {
        let (name, state) = self.handler_name_state();
        tracing::trace!(
            handler = &*name,
            state = &*state,
            "ignoring skstack EVENT notification {:?}",
            ev
        );
        let EventNotification { content, .. } = ev;
        match content {
            EventNotificationContent::Arib108SendRestricted => {
                tracing::warn!("sending data is not temporarily restricted due to ARIB STD T-108");
            }
            EventNotificationContent::Arib108SendRestrictionLifted => {
                tracing::warn!("ARIB STD T-108 send restriction is now lifted");
            }
            EventNotificationContent::PanaCloseRequested
            | EventNotificationContent::PanaCloseComplete
            | EventNotificationContent::PanaCloseTimeout
            | EventNotificationContent::SessionExpired => {
                // TODO: Handle the close requests and notifications.
                // For now simply ignore with log message.
                tracing::warn!("received Wi-SUN notification event {:?}", content);
            }
            EventNotificationContent::UdpSendComplete(status) => {
                return self.sk_udp_send_complete(status).await
            }
            _ => {
                // Nothing to do for now.
                tracing::trace!("ignoring Wi-SUN notification event {:?}", content);
            }
        }
        None
    }
    async fn sk_udp_send_complete(
        &mut self,
        ev: skstack::UdpSendCompleteStatus,
    ) -> Option<Self::Result> {
        let (name, state) = self.handler_name_state();
        tracing::trace!(
            handler = &*name,
            state = &*state,
            "ignoring skstack UDP send completion notification {:?}",
            ev
        );
        None
    }
    async fn el_frame_format1<'a>(&mut self, frame: FrameFormat1<'a>) -> Option<Self::Result> {
        let packet = *frame.packet();
        match packet.service().to_known_code() {
            Some(service) => self.el_props(frame.transaction_id(), service, packet).await,
            None => self.el_service_unknown(frame).await,
        }
    }
    async fn el_frame_unknown<'a>(
        &mut self,
        data: &[u8],
        parse_error: echonet_lite::Error,
    ) -> Option<Self::Result> {
        let (name, state) = self.handler_name_state();
        tracing::warn!(
            handler = &*name,
            state = &*state,
            "cannot decode as ECHONET Lite frame format 1 (data = {:02x?}): {}",
            data,
            parse_error
        );
        None
    }
    async fn el_service_unknown<'a>(&mut self, frame: FrameFormat1<'a>) -> Option<Self::Result> {
        let (name, state) = self.handler_name_state();
        tracing::warn!(
            handler = &*name,
            state = &*state,
            "unknown ECHONET Lite service code {:?}",
            frame.packet().service()
        );
        None
    }
    async fn el_props<'a>(
        &mut self,
        _tran_id: TransactionId,
        _service: KnownServiceCode,
        _packet: Packet<'a>,
    ) -> Option<Self::Result> {
        let (name, state) = self.handler_name_state();
        tracing::trace!(
            handler = &*name,
            state = &*state,
            "ignoring properties: service = {:?}, transaction ID = {:?}, packet = {:?}",
            _service,
            _tran_id,
            _packet
        );
        None
    }
}

#[derive(Default, Debug, Clone)]
struct CheckIfSmartmeter {
    request_sent: bool,
}

impl CheckIfSmartmeter {
    #[inline]
    #[must_use]
    fn new() -> Self {
        Self::default()
    }
}

#[async_trait]
impl HandleEvent for CheckIfSmartmeter {
    type Result = anyhow::Result<Option<bool>>;

    fn handler_name_state(&self) -> (Cow<'_, str>, Cow<'_, str>) {
        let state = match self.request_sent {
            false => "passive",
            true => "active",
        };
        (Cow::Borrowed("CheckIfSmartmeter"), Cow::Borrowed(state))
    }

    async fn el_props<'a>(
        &mut self,
        _tran_id: TransactionId,
        service: KnownServiceCode,
        packet: Packet<'a>,
    ) -> Option<Self::Result> {
        let (name, state) = self.handler_name_state();
        tracing::trace!(
            handler = &*name,
            state = &*state,
            "processing properties: service={:?}",
            service
        );
        match service {
            KnownServiceCode::NotifySuccess(_) => {}
            KnownServiceCode::NotifyFailure => {
                // Single value is requested.
                if let Some(prop) = packet.properties().next() {
                    match prop.decode_as::<EmptyProperty>() {
                        Ok(prop) if prop.prop_code() == KnownPropertyCode::InstanceList => {
                            // Use `INF_REQ` to request instance list. See the section 6.6 of the
                            // "ECHONET Lite System Design Guidelines 2nd edition".
                            tracing::warn!(
                                "the smartmeter does not support re-notification of instance list"
                            );
                            return Some(Ok(None));
                        }
                        Ok(prop) => {
                            tracing::trace!("ignoring property {prop:?}");
                            return None;
                        }
                        Err(e) => {
                            tracing::trace!("ignoring property {prop:?}: {e}");
                            return None;
                        }
                    }
                }
            }
            service => {
                tracing::trace!("ignoring service {service:?} (raw={:#02X})", service.raw());
                return None;
            }
        };
        // Single value is requested.
        let prop = packet.properties().next()?;
        match prop.decode_as::<SupportedProperty>() {
            Ok(SupportedProperty::InstanceList(list)) => {
                Some(Ok(Some(list.contains(connection::EOJ_SMARTMETER))))
            }
            Ok(ev) => {
                tracing::warn!("ignoring property {ev:?}");
                None
            }
            Err(e) => {
                tracing::warn!("ignoring unsupported property {prop:?}: {e}");
                None
            }
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct InitialData {
    ce_coefficient: Option<CumulativeEnergyCoefficient>,
    ce_effective_digits: CumulativeEnergyEffectiveDigits,
    ce_unit: CumulativeEnergyUnit,
}

impl InitialData {
    async fn fetch(conn: &mut SmartmeterConnection) -> anyhow::Result<Self> {
        let mut handler = FetchInitialData::new();
        let mut retry_count = 0;
        while retry_count < 4 {
            if retry_count != 0 {
                tracing::debug!(
                    "failed to fetch smartmeter data params. retrying (retry_count = {})",
                    retry_count
                );
            }
            let wait = async {
                handler.send_request(conn).await?;
                loop {
                    let ev = conn.recv_event().await.context("failed to receive event")?;
                    match handler.sk_event(ev).await {
                        Some(res) => return res,
                        None => continue,
                    }
                }
            };
            match tokio::time::timeout(MIN_WAIT_RESPONSE_MULTIPLE_PROPS, wait).await {
                Ok(Ok(params)) => {
                    tracing::debug!("got smartmeter data params: {params:?}");
                    return Ok(params);
                }
                Ok(Err(e)) => return Err(e),
                Err(_) => {
                    retry_count += 1;
                }
            }
        }

        bail!("failed to fetch smartmeter params");
    }
}

#[derive(Default, Debug, Clone)]
struct FetchInitialData {
    waiting_transaction_id: Option<TransactionId>,
    ce_coefficient: Option<CumulativeEnergyCoefficient>,
    ce_effective_digits: Option<CumulativeEnergyEffectiveDigits>,
    ce_unit: Option<CumulativeEnergyUnit>,
}

impl FetchInitialData {
    #[inline]
    #[must_use]
    fn new() -> Self {
        Self::default()
    }

    async fn send_request(&mut self, conn: &mut SmartmeterConnection) -> anyhow::Result<()> {
        let transaction_id = conn.generate_transaction_id();
        let mut request = FrameFormat1Builder::new(
            transaction_id,
            connection::EOJ_HEMS_CONTROLLER,
            connection::EOJ_SMARTMETER,
            KnownServiceCode::ReadRequest,
        );
        request
            .push_prop(&EmptyProperty::new(
                KnownPropertyCode::CumulativeEnergyCoefficient.into(),
            ))
            .context("failed to write a property into a packet")?;
        request
            .push_prop(&EmptyProperty::new(
                KnownPropertyCode::CumulativeEnergyEffectiveDigits.into(),
            ))
            .context("failed to write a property into a packet")?;
        request
            .push_prop(&EmptyProperty::new(
                KnownPropertyCode::CumulativeEnergyUnit.into(),
            ))
            .context("failed to write a property into a packet")?;
        conn.send_el_frame1(&request).await?;
        self.waiting_transaction_id = Some(transaction_id);
        Ok(())
    }
}

#[async_trait]
impl HandleEvent for FetchInitialData {
    type Result = anyhow::Result<InitialData>;

    fn handler_name_state(&self) -> (Cow<'_, str>, Cow<'_, str>) {
        (Cow::Borrowed("FetchDataParams"), Cow::Borrowed("default"))
    }

    async fn el_props<'a>(
        &mut self,
        tran_id: TransactionId,
        service: KnownServiceCode,
        packet: Packet<'a>,
    ) -> Option<Self::Result> {
        let (name, state) = self.handler_name_state();
        tracing::trace!(
            handler = &*name,
            state = &*state,
            "processing properties: service={:?}",
            service
        );
        if self.waiting_transaction_id != Some(tran_id) {
            // Ignore unrelated transactions.
            return None;
        }
        if !matches!(
            service,
            KnownServiceCode::ReadSuccess | KnownServiceCode::ReadFailure
        ) {
            // Possibly unrelated notification or something is actively sent
            // from the remote device, and transaction ID conflicted unfortunately.
            // Ignore as unrelated and keep waiting for the actual response.
            tracing::warn!("ignoring (possibly) unrelated packet with service {service:?}");
            return None;
        };
        let mut done = false;
        for prop in packet.properties() {
            match KnownPropertyCode::from_generic(prop.prop_code()) {
                Some(KnownPropertyCode::CumulativeEnergyCoefficient) => {
                    done = true;
                    let coeff = prop
                        .decode_as::<MaybeEmptyProperty<CumulativeEnergyCoefficient>>()
                        .map(MaybeEmptyProperty::into_value);
                    match coeff {
                        Ok(v) => self.ce_coefficient = v,
                        Err(e) => return Some(Err(e.into())),
                    }
                }
                Some(KnownPropertyCode::CumulativeEnergyEffectiveDigits) => {
                    done = true;
                    let digits = prop
                        .decode_as::<MaybeEmptyProperty<CumulativeEnergyEffectiveDigits>>()
                        .map(MaybeEmptyProperty::into_value);
                    match digits {
                        Ok(v) => self.ce_effective_digits = v,
                        Err(e) => return Some(Err(e.into())),
                    }
                }
                Some(KnownPropertyCode::CumulativeEnergyUnit) => {
                    done = true;
                    let unit = prop
                        .decode_as::<MaybeEmptyProperty<CumulativeEnergyUnit>>()
                        .map(MaybeEmptyProperty::into_value);
                    match unit {
                        Ok(v) => self.ce_unit = v,
                        Err(e) => return Some(Err(e.into())),
                    }
                }
                _ => {
                    // Received a property that we didn't request.
                    // Something bad might be happening...
                    return Some(Err(anyhow!(
                        "received unrelated property value (prop = {prop:?}"
                    )));
                }
            }
        }
        if !done {
            // Possibly unrelated notification or something is actively sent
            // from the remote device, and transaction ID conflicted unfortunately.
            // Ignore as unrelated and keep waiting for the actual response.
            return Some(Err(anyhow!(
                "received unexpected packet for the expected transaction ID"
            )));
        }

        let ce_effective_digits = match self.ce_effective_digits {
            Some(v) => v,
            None => {
                return Some(Err(anyhow!(
                    "`CumulativeEnergyEffectiveDigits` property is mandatory"
                )))
            }
        };
        let ce_unit = match self.ce_unit {
            Some(v) => v,
            None => return Some(Err(anyhow!("`CumulativeEnergyUnit` property is mandatory"))),
        };
        let data = InitialData {
            ce_coefficient: self.ce_coefficient,
            ce_effective_digits,
            ce_unit,
        };

        Some(Ok(data))
    }
}

#[derive(Debug, Clone, Copy)]
struct PowerMetrics {
    time: Instant,
    ce_normal: CumulativeEnergyNormal,
    ce_reverse: Option<CumulativeEnergyReverse>,
    instant_energy: InstantEnergy,
    instant_currents: InstantCurrents,
}

impl PowerMetrics {
    async fn fetch(
        conn: &mut SmartmeterConnection,
    ) -> Result<Option<Self>, FetchPowerMetricsError> {
        let wait = async {
            let mut handler = FetchPowerMetrics::new();
            handler.send_request(conn).await?;
            loop {
                let ev = conn.recv_event().await.context("failed to receive event")?;
                match handler.sk_event(ev).await {
                    Some(res) => return res,
                    None => continue,
                }
            }
        };
        match tokio::time::timeout(MIN_WAIT_RESPONSE_MULTIPLE_PROPS, wait).await {
            Ok(Ok(params)) => {
                tracing::debug!("got power metrics from smartmeter: {params:?}");
                Ok(Some(params))
            }
            Ok(Err(e)) => Err(e),
            Err(_) => Ok(None),
        }
    }
}

#[derive(Debug, Error)]
enum FetchPowerMetricsError {
    #[error(transparent)]
    Other(#[from] anyhow::Error),
    #[error("property error")]
    Property(#[from] elprops_smartmeter::PropertyError),
    #[error("UDP send error")]
    UdpSend,
}

#[derive(Default, Debug, Clone)]
struct FetchPowerMetrics {
    waiting_transaction_id: Option<TransactionId>,
    ce_normal: Option<CumulativeEnergyNormal>,
    ce_reverse: Option<CumulativeEnergyReverse>,
    instant_energy: Option<InstantEnergy>,
    instant_currents: Option<InstantCurrents>,
}

impl FetchPowerMetrics {
    #[inline]
    #[must_use]
    fn new() -> Self {
        Self::default()
    }

    async fn send_request(&mut self, conn: &mut SmartmeterConnection) -> anyhow::Result<()> {
        let transaction_id = conn.generate_transaction_id();
        let mut request = FrameFormat1Builder::new(
            transaction_id,
            connection::EOJ_HEMS_CONTROLLER,
            connection::EOJ_SMARTMETER,
            KnownServiceCode::ReadRequest,
        );
        request
            .push_prop(&EmptyProperty::new(
                KnownPropertyCode::CumulativeEnergyNormal.into(),
            ))
            .context("failed to write a property into a packet")?;
        request
            .push_prop(&EmptyProperty::new(
                KnownPropertyCode::CumulativeEnergyReverse.into(),
            ))
            .context("failed to write a property into a packet")?;
        request
            .push_prop(&EmptyProperty::new(KnownPropertyCode::InstantEnergy.into()))
            .context("failed to write a property into a packet")?;
        request
            .push_prop(&EmptyProperty::new(
                KnownPropertyCode::InstantCurrents.into(),
            ))
            .context("failed to write a property into a packet")?;
        conn.send_el_frame1(&request).await?;
        self.waiting_transaction_id = Some(transaction_id);
        Ok(())
    }
}

#[async_trait]
impl HandleEvent for FetchPowerMetrics {
    type Result = Result<PowerMetrics, FetchPowerMetricsError>;

    fn handler_name_state(&self) -> (Cow<'_, str>, Cow<'_, str>) {
        (Cow::Borrowed("FetchPowerMetrics"), Cow::Borrowed("default"))
    }

    async fn sk_udp_send_complete(
        &mut self,
        ev: skstack::UdpSendCompleteStatus,
    ) -> Option<Self::Result> {
        if ev == skstack::UdpSendCompleteStatus::Failed {
            return Some(Err(FetchPowerMetricsError::UdpSend));
        }
        return None;
    }

    async fn el_props<'a>(
        &mut self,
        tran_id: TransactionId,
        service: KnownServiceCode,
        packet: Packet<'a>,
    ) -> Option<Self::Result> {
        let (name, state) = self.handler_name_state();
        tracing::trace!(
            handler = &*name,
            state = &*state,
            "processing properties: service={:?}",
            service
        );
        let time = Instant::now();
        if self.waiting_transaction_id != Some(tran_id) {
            // Ignore unrelated transactions.
            return None;
        }
        if !matches!(
            service,
            KnownServiceCode::ReadSuccess | KnownServiceCode::ReadFailure
        ) {
            // Possibly unrelated notification or something is actively sent
            // from the remote device, and transaction ID conflicted unfortunately.
            // Ignore as unrelated and keep waiting for the actual response.
            tracing::warn!("ignoring (possibly) unrelated packet with service {service:?}");
            return None;
        };
        let mut done = false;
        for prop in packet.properties() {
            match KnownPropertyCode::from_generic(prop.prop_code()) {
                Some(KnownPropertyCode::CumulativeEnergyNormal) => {
                    done = true;
                    let v = prop
                        .decode_as::<MaybeEmptyProperty<CumulativeEnergyNormal>>()
                        .map(MaybeEmptyProperty::into_value);
                    match v {
                        Ok(v) => self.ce_normal = v,
                        Err(e) => return Some(Err(e.into())),
                    }
                }
                Some(KnownPropertyCode::CumulativeEnergyReverse) => {
                    done = true;
                    let v = prop
                        .decode_as::<MaybeEmptyProperty<CumulativeEnergyReverse>>()
                        .map(MaybeEmptyProperty::into_value);
                    match v {
                        Ok(v) => self.ce_reverse = v,
                        Err(e) => return Some(Err(e.into())),
                    }
                }
                Some(KnownPropertyCode::InstantEnergy) => {
                    done = true;
                    let v = prop
                        .decode_as::<MaybeEmptyProperty<InstantEnergy>>()
                        .map(MaybeEmptyProperty::into_value);
                    match v {
                        Ok(v) => self.instant_energy = v,
                        Err(e) => return Some(Err(e.into())),
                    }
                }
                Some(KnownPropertyCode::InstantCurrents) => {
                    done = true;
                    let v = prop
                        .decode_as::<MaybeEmptyProperty<InstantCurrents>>()
                        .map(MaybeEmptyProperty::into_value);
                    match v {
                        Ok(v) => self.instant_currents = v,
                        Err(e) => return Some(Err(e.into())),
                    }
                }
                _ => {
                    // Received a property that we didn't request.
                    // Something bad might be happening...
                    return Some(Err(anyhow!(
                        "received unrelated property value (prop = {prop:?})"
                    )
                    .into()));
                }
            }
        }
        if !done {
            // Possibly unrelated notification or something is actively sent
            // from the remote device, and transaction ID conflicted unfortunately.
            // Ignore as unrelated and keep waiting for the actual response.
            return Some(Err(anyhow!(
                "received unexpected packet for the expected transaction ID"
            )
            .into()));
        }

        let ce_normal = match self.ce_normal {
            Some(v) => v,
            None => {
                return Some(Err(anyhow!(
                    "`CumulativeEnergyNormal` property is mandatory"
                )
                .into()))
            }
        };
        let instant_energy = match self.instant_energy {
            Some(v) => v,
            None => return Some(Err(anyhow!("`InstantEnergy` property is mandatory").into())),
        };
        let instant_currents = match self.instant_currents {
            Some(v) => v,
            None => {
                return Some(Err(
                    anyhow!("`InstantCurrents` property is mandatory").into()
                ))
            }
        };
        let data = PowerMetrics {
            time,
            ce_normal,
            ce_reverse: self.ce_reverse,
            instant_energy,
            instant_currents,
        };

        Some(Ok(data))
    }
}
