//! Wi-SUN exporter.
#![warn(clippy::must_use_candidate)]

mod device;
mod metrics;

use std::net::SocketAddr;
use std::sync::Arc;

use anyhow::Context as _;
use axum::response::IntoResponse;
use axum::routing::get;
use axum::Extension;
use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Config {
    server: ServerConfig,
    #[serde(default)]
    hems_skstack_smartmeter: Vec<device::hems_skstack::smartmeter::HemsSkstackSmartmeterConfig>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ServerConfig {
    listen: String,
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt::init();

    // TODO: Make config path and filename customizable.
    let config: Config = {
        let path = std::path::Path::new("config.toml");
        let s = std::fs::read_to_string(path)
            .with_context(|| format!("failed to open config file {}", path.display()))?;
        toml::from_str(&s)
            .with_context(|| format!("failed to parse config file {}", path.display()))?
    };
    let config = Arc::new(config);

    let registry = setup_registry().context("failed to set up registry")?;

    let app = axum::Router::new()
        .route("/metrics", get(metrics))
        .layer(Extension(registry));

    let addr = config
        .server
        .listen
        .parse::<SocketAddr>()
        .with_context(|| {
            format!(
                "failed to parse {:?} as a socket address",
                config.server.listen
            )
        })?;

    let _hems_b_route_watcher_handle = tokio::spawn(device::hems_skstack::smartmeter::watch_retry(
        config.clone(),
    ));

    tracing::debug!("listening on {addr}");
    let listener = tokio::net::TcpListener::bind(addr)
        .await
        .with_context(|| "Failed to listen {addr}")?;
    axum::serve(listener, app)
        .await
        .context("failed to serve")?;

    unreachable!();
}

fn setup_registry() -> anyhow::Result<Arc<prometheus::Registry>> {
    let registry = Arc::new(prometheus::Registry::new());

    // For HEMS.
    registry
        .register(Box::new(
            metrics::hems::powermeter::PURCHASED_ENERGY_GAUGE.clone(),
        ))
        .context("failed to register purchased energy gauge")?;
    registry
        .register(Box::new(
            metrics::hems::powermeter::SOLD_ENERGY_GAUGE.clone(),
        ))
        .context("failed to register sold energy gauge")?;
    registry
        .register(Box::new(
            metrics::hems::powermeter::INSTANT_CONSUMED_ENERGY_GAUGE.clone(),
        ))
        .context("failed to register instant consumed energy gauge")?;
    registry
        .register(Box::new(
            metrics::hems::powermeter::INSTANT_CONSUMED_CURRENT_PHASE_R_GAUGE.clone(),
        ))
        .context("failed to register instant consumed phase-R current gauge")?;
    registry
        .register(Box::new(
            metrics::hems::powermeter::INSTANT_CONSUMED_CURRENT_PHASE_T_GAUGE.clone(),
        ))
        .context("failed to register instant consumed phase-T current gauge")?;

    Ok(registry)
}

async fn metrics(Extension(registry): Extension<Arc<prometheus::Registry>>) -> impl IntoResponse {
    let encoder = prometheus::TextEncoder::new();
    let metrics = registry.gather();
    // TODO: Add "expire too old data" processing.
    match encoder.encode_to_string(&metrics) {
        Ok(v) => v,
        Err(e) => {
            // TODO: Make error handling better.
            e.to_string()
        }
    }
}
