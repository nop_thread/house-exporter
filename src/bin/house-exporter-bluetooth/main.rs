#![warn(clippy::must_use_candidate)]

mod device;
mod metrics;

use std::net::SocketAddr;
use std::sync::Arc;

use anyhow::Context as _;
use axum::response::IntoResponse;
use axum::routing::get;
use axum::Extension;
use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Config {
    server: ServerConfig,
    #[serde(default)]
    switchbot_meter_plus: Vec<device::switchbot::meter_plus::SwitchbotMeterPlusConfig>,
    #[serde(default)]
    switchbot_plug_mini: Vec<device::switchbot::plug_mini::SwitchbotPlugMiniConfig>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ServerConfig {
    listen: String,
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt::init();

    let config: Config = {
        let path = std::path::Path::new("config.toml");
        let s = std::fs::read_to_string(path)
            .with_context(|| format!("failed to open config file {}", path.display()))?;
        toml::from_str(&s)
            .with_context(|| format!("failed to parse config file {}", path.display()))?
    };
    let config = Arc::new(config);

    let registry = setup_registry().context("failed to set up registry")?;

    let app = axum::Router::new()
        .route("/metrics", get(metrics))
        .layer(Extension(registry));

    let addr = config
        .server
        .listen
        .parse::<SocketAddr>()
        .with_context(|| {
            format!(
                "failed to parse {:?} as a socket address",
                config.server.listen
            )
        })?;

    let _switchbot_meter_plus_watcher_handle =
        tokio::spawn(device::switchbot::meter_plus::watch_retry(config.clone()));
    let _switchbot_plug_mini_watcher_handle =
        tokio::spawn(device::switchbot::plug_mini::watch_retry(config.clone()));
    tokio::spawn(device::switchbot::meter_plus::watch_retry(config.clone()));

    tracing::debug!("listening on {addr}");
    let listener = tokio::net::TcpListener::bind(addr)
        .await
        .with_context(|| "Failed to listen {addr}")?;
    axum::serve(listener, app)
        .await
        .context("failed to serve")?;

    unreachable!();
}

fn setup_registry() -> anyhow::Result<Arc<prometheus::Registry>> {
    let registry = Arc::new(prometheus::Registry::new());

    // For thermohydrometers.
    registry
        .register(Box::new(metrics::thermo_hygro::TEMPERATURE_GAUGE.clone()))
        .context("failed to register temperature gauge")?;
    registry
        .register(Box::new(
            metrics::thermo_hygro::RELATIVE_HUMIDITY_GAUGE.clone(),
        ))
        .context("failed to register relative humidity gauge")?;
    registry
        .register(Box::new(
            metrics::thermo_hygro::VOLUMETRIC_HUMIDITY_GAUGE.clone(),
        ))
        .context("failed to register volumetric humidity gauge")?;
    registry
        .register(Box::new(metrics::thermo_hygro::BATTERY_GAUGE.clone()))
        .context("failed to register thermohygrometer battery gauge")?;

    // For smart plug.
    registry
        .register(Box::new(
            metrics::smart_plug::INSTANT_CONSUMED_ENERGY_GAUGE.clone(),
        ))
        .context("failed to register instant consumed energy gauge")?;
    registry
        .register(Box::new(
            metrics::smart_plug::INSTANT_CONSUMED_CURRENT_GAUGE.clone(),
        ))
        .context("failed to register instant consumed current gauge")?;
    registry
        .register(Box::new(metrics::smart_plug::VOLTAGE_GAUGE.clone()))
        .context("failed to register voltage gauge")?;
    registry
        .register(Box::new(metrics::smart_plug::POWER_ON.clone()))
        .context("failed to register power-on gauge")?;

    Ok(registry)
}

async fn metrics(Extension(registry): Extension<Arc<prometheus::Registry>>) -> impl IntoResponse {
    let encoder = prometheus::TextEncoder::new();
    let metrics = registry.gather();
    // TODO: Add "expire too old data" processing.
    match encoder.encode_to_string(&metrics) {
        Ok(v) => v,
        Err(e) => {
            // TODO: Make error handling better.
            e.to_string()
        }
    }
}
