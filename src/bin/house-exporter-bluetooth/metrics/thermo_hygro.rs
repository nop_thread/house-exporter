//! Thermo-hygrometer.

use once_cell::sync::Lazy;
use prometheus::GaugeVec;

const VARIABLE_LABELS: &[&str] = &["house", "room"];
pub static TEMPERATURE_GAUGE: Lazy<GaugeVec> = Lazy::new(|| {
    let opts = prometheus::Opts::new("temperature_celsius", "Temperature")
        .namespace("house")
        .subsystem("thermo_hygro");
    GaugeVec::new(opts, VARIABLE_LABELS).expect("failed to create temperature gauge")
});
pub static RELATIVE_HUMIDITY_GAUGE: Lazy<GaugeVec> = Lazy::new(|| {
    let opts = prometheus::Opts::new("humidity_ratio", "Relative humidity")
        .namespace("house")
        .subsystem("thermo_hygro");
    GaugeVec::new(opts, VARIABLE_LABELS).expect("failed to create relative humidity gauge")
});
pub static VOLUMETRIC_HUMIDITY_GAUGE: Lazy<GaugeVec> = Lazy::new(|| {
    let opts = prometheus::Opts::new(
        "volumetric_humidity",
        "Volumetric humidity (gram per cubic meter)",
    )
    .namespace("house")
    .subsystem("thermo_hygro");
    GaugeVec::new(opts, VARIABLE_LABELS).expect("failed to create volumetric humidity gauge")
});
pub static BATTERY_GAUGE: Lazy<GaugeVec> = Lazy::new(|| {
    let opts = prometheus::Opts::new(
        "battery_ratio",
        "Remaining battery capacity of the thermohygrometer",
    )
    .namespace("house")
    .subsystem("thermo_hygro");
    GaugeVec::new(opts, VARIABLE_LABELS).expect("failed to create thermohygrometer battery gauge")
});

/// Returns the volumetric humidity (in `g/(m^3)`).
///
/// See <https://www.hatchability.com/Vaisala.pdf> and
/// <https://www.aqua-calc.com/calculate/humidity>.
#[must_use]
#[inline]
pub(crate) fn volumetric_humidity(relative_humidity_ratio: f64, temperature_celsius: f64) -> f64 {
    // Unit is `gK/J`.
    const COEFF: f64 = 2.16679;
    let temperature_kelvin = temperature_celsius + 273.15;
    let p_ws = vapor_saturation_pressure(temperature_kelvin);
    let p_w = p_ws * relative_humidity_ratio;
    COEFF * p_w / temperature_kelvin
}

/// Unit is Pa.
#[must_use]
#[inline]
fn vapor_saturation_pressure(temperature_kelvin: f64) -> f64 {
    if temperature_kelvin >= 273.15 {
        if temperature_kelvin > 373.0 + 273.15 {
            panic!("temperature too large ({temperature_kelvin} kelvin)");
        }
        vapor_saturation_pressure_high(temperature_kelvin)
    } else {
        vapor_saturation_pressure_low(temperature_kelvin)
    }
}

/// Unit is Pa.
#[must_use]
fn vapor_saturation_pressure_high(temperature_kelvin: f64) -> f64 {
    assert!(temperature_kelvin >= 273.15);
    // Unit is Kelvin.
    const T_CRITICAL: f64 = 647.096;
    /// Unit is Pa.
    const P_CRITICAL: f64 = 22_064_000.0;
    /// Coefficients.
    const COEFF: [f64; 6] = [
        -7.85951783,
        1.84408259,
        -11.7866497,
        22.6807411,
        -15.9618719,
        1.80122502,
    ];
    let v = 1.0 - (temperature_kelvin / T_CRITICAL);
    let sqrt_v = v.sqrt();
    let v_1_5 = v * sqrt_v;
    let v_2 = v * v;
    let v_3 = v_2 * v;
    let v_3_5 = v_3 * sqrt_v;
    let v_4 = v_2 * v_2;
    let v_7_5 = v_4 * v_3 * sqrt_v;

    let ln_pws_pc = (COEFF[0] * v
        + COEFF[1] * v_1_5
        + COEFF[2] * v_3
        + COEFF[3] * v_3_5
        + COEFF[4] * v_4
        + COEFF[5] * v_7_5)
        * T_CRITICAL
        / temperature_kelvin;
    ln_pws_pc.exp() * P_CRITICAL
}

/// Unit is Pa.
#[must_use]
fn vapor_saturation_pressure_low(temperature_kelvin: f64) -> f64 {
    assert!(temperature_kelvin <= 273.15);

    // Unit is Kelvin.
    const T_TRIPLEPOINT: f64 = 273.16;
    // Unit is Pa.
    const P_TRIPLEPOINT: f64 = 611.657;
    const COEFF: [f64; 2] = [-13.928169, 34.707823];
    let theta = temperature_kelvin / T_TRIPLEPOINT;
    let theta_sqrt = theta.sqrt();
    let theta_1_5 = theta * theta_sqrt;
    let theta_1_25 = theta * theta_sqrt.sqrt();

    let log_pwi_pn = COEFF[0] * (1.0 - (1.0 / theta_1_5)) + COEFF[1] * (1.0 - (1.0 / theta_1_25));
    log_pwi_pn.exp() * P_TRIPLEPOINT
}
