//! Smart power hub.

use once_cell::sync::Lazy;
use prometheus::GaugeVec;

const NAMESPACE: &str = "house";
const SUBSYSTEM: &str = "smart_plug";

const VARIABLE_LABELS: &[&str] = &["house", "name"];
pub static INSTANT_CONSUMED_ENERGY_GAUGE: Lazy<GaugeVec> = Lazy::new(|| {
    let opts = prometheus::Opts::new("instant_consumed_energy_watts", "Instant consumed energy")
        .namespace(NAMESPACE)
        .subsystem(SUBSYSTEM);
    GaugeVec::new(opts, VARIABLE_LABELS).expect("failed to create instant consumed energy gauge")
});
pub static INSTANT_CONSUMED_CURRENT_GAUGE: Lazy<GaugeVec> = Lazy::new(|| {
    let opts = prometheus::Opts::new(
        "instant_consumed_current_ampere",
        "Instant consumed current",
    )
    .namespace(NAMESPACE)
    .subsystem(SUBSYSTEM);
    GaugeVec::new(opts, VARIABLE_LABELS).expect("failed to create instant consumed current gauge")
});
pub static VOLTAGE_GAUGE: Lazy<GaugeVec> = Lazy::new(|| {
    let opts = prometheus::Opts::new("voltage_watts", "Voltage")
        .namespace(NAMESPACE)
        .subsystem(SUBSYSTEM);
    GaugeVec::new(opts, VARIABLE_LABELS).expect("failed to create voltage gauge")
});
pub static POWER_ON: Lazy<GaugeVec> = Lazy::new(|| {
    let opts = prometheus::Opts::new("power_on", "Whether the switch state is power on")
        .namespace(NAMESPACE)
        .subsystem(SUBSYSTEM);
    GaugeVec::new(opts, VARIABLE_LABELS).expect("failed to create power-on gauge")
});
//// not used for now.
//#[allow(dead_code)]
//pub static OVERLOADED: Lazy<GaugeVec> = Lazy::new(|| {
//    let opts = prometheus::Opts::new(
//        "overloaded",
//        "Whether the switch is powered off by overload",
//    )
//    .namespace(NAMESPACE)
//    .subsystem(SUBSYSTEM);
//    GaugeVec::new(opts, VARIABLE_LABELS).expect("failed to create overload gauge")
//});
