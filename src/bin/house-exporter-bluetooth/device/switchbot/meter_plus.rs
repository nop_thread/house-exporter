//! SwitchBot meter plus.
//!
//! About SwitchBot meter plus, see
//! <https://www.switchbot.jp/collections/all/products/switchbot-meter-plus>
//! (2022-08-01).

use std::collections::HashMap;
use std::sync::Arc;

use anyhow::{anyhow, bail, Context as _};
use futures::{pin_mut, FutureExt, StreamExt};
use serde::Deserialize;
use uuid::Uuid;

use crate::metrics::thermo_hygro::{
    volumetric_humidity, BATTERY_GAUGE, RELATIVE_HUMIDITY_GAUGE, TEMPERATURE_GAUGE,
    VOLUMETRIC_HUMIDITY_GAUGE,
};
use crate::Config;

#[derive(Debug, Clone, PartialEq, Eq, Deserialize)]
pub struct SwitchbotMeterPlusConfig {
    /// MAC address of the thermohygrometer.
    #[serde(rename = "mac-addr")]
    mac_addr: bluer::Address,
    /// House.
    house: String,
    /// Room.
    room: String,
}

pub async fn watch_retry(config: Arc<Config>) {
    const RETRY_DURATION: std::time::Duration = std::time::Duration::from_secs(3);

    if config.switchbot_meter_plus.is_empty() {
        // Nothing to watch.
        return;
    }

    loop {
        match watch(&config.switchbot_meter_plus).await {
            Ok(()) => break,
            Err(e) => {
                tracing::error!("thermohygrometer watcher failed: {e:?}");
                tokio::time::sleep(RETRY_DURATION).await;
            }
        }
    }
}

async fn watch(configs: &[SwitchbotMeterPlusConfig]) -> anyhow::Result<()> {
    // Set up Bluetooth.
    let session = bluer::Session::new().await?;
    let adapter = session.default_adapter().await?;
    adapter.set_powered(true).await?;

    let tasks: Vec<_> = configs
        .iter()
        .map(|conf| watch_single_thermohygrometer(adapter.clone(), conf.clone()).boxed())
        .collect::<Vec<_>>();
    futures::future::select_all(tasks).await.0
}

async fn watch_single_thermohygrometer(
    adapter: bluer::Adapter,
    config: SwitchbotMeterPlusConfig,
) -> anyhow::Result<()> {
    // The thermohygrometer returns the service data only when `REQ` packet was
    // sent from the BLE central. However, current `bluer` crate (as of
    // 2022-08-01) does not allow it to the developer directly.
    //
    // As a workaround, use discovery to keep sending `REQ` packets.
    // (See <https://stackoverflow.com/a/66469724>.)
    //
    // Connecting to the device might also be effective, but it takes long time
    // (about 20 seconds!) to connect, and bluez automatically disconnects a few
    // seconds later. It is quite inefficient.

    clear_metrics(&config);

    tracing::debug!(
        "Start discovery from Bluetooth adapter {} with address {}",
        adapter.name(),
        adapter.address().await?
    );
    let discover = adapter.discover_devices().await?;
    pin_mut!(discover);
    while let Some(event) = discover.next().await {
        if let bluer::AdapterEvent::DeviceAdded(addr) = event {
            if addr != config.mac_addr {
                continue;
            }
            let device = adapter.device(addr)?;
            tracing::trace!("device = {device:?}, config = {config:?}");
            tracing::trace!(
                "all properties for device {device:?}: {:#?}",
                device.all_properties().await
            );
            let meter = MeterPlusDevice::new(device);
            watch_property_changes(&meter, &config).await?;
        }
    }

    bail!("the stream for device discovery is unexpectedly stopped: config = {config:?}");
}

async fn watch_property_changes(
    meter: &MeterPlusDevice,
    config: &SwitchbotMeterPlusConfig,
) -> anyhow::Result<()> {
    let data = meter.data().await?;
    tracing::info!(
        "successfully connected to the device {:?} (config={:?})",
        meter.device(),
        config
    );
    set_metrics(config, &data)?;
    tracing::debug!("data (first) = {data:?}, config={config:?}");

    let events = meter.device().events().await?;
    pin_mut!(events);
    while let Some(ev) = events.next().await {
        let bluer::DeviceEvent::PropertyChanged(changed) = ev;
        if let bluer::DeviceProperty::ServiceData(service_data) = &changed {
            let data = MeterPlusSensorData::from_raw_service_data(service_data)?;
            tracing::debug!("data (changed) = {data:?}, config={config:?}");
            match data {
                Some(data) => set_metrics(config, &data)?,
                None => {
                    tracing::debug!("reconnect to the device {meter:?}");
                    meter
                        .device()
                        .connect()
                        .await
                        .with_context(|| format!("failed to reconnect to the device {meter:?}"))?;
                }
            }
        }
    }

    bail!("the stream for property change is unexpectedly stopped: config = {config:?}");
}

fn clear_metrics(config: &SwitchbotMeterPlusConfig) {
    let labels = &[config.house.as_str(), config.room.as_str()];
    let _ = TEMPERATURE_GAUGE.remove_label_values(labels);
    let _ = RELATIVE_HUMIDITY_GAUGE.remove_label_values(labels);
    let _ = VOLUMETRIC_HUMIDITY_GAUGE.remove_label_values(labels);
    let _ = BATTERY_GAUGE.remove_label_values(labels);
}

fn set_metrics(
    config: &SwitchbotMeterPlusConfig,
    data: &MeterPlusSensorData,
) -> anyhow::Result<()> {
    let labels = &[config.house.as_str(), config.room.as_str()];
    TEMPERATURE_GAUGE
        .get_metric_with_label_values(labels)
        .with_context(|| format!("failed to get the temperature gauge: config={config:?}"))?
        .set(data.temperature_celsius());
    RELATIVE_HUMIDITY_GAUGE
        .get_metric_with_label_values(labels)
        .with_context(|| format!("failed to get the humidity gauge: config={config:?}"))?
        .set(data.humidity_ratio());
    VOLUMETRIC_HUMIDITY_GAUGE
        .get_metric_with_label_values(labels)
        .with_context(|| format!("failed to get the humidity gauge: config={config:?}"))?
        .set(data.volumetric_humidity());
    BATTERY_GAUGE
        .get_metric_with_label_values(labels)
        .with_context(|| {
            format!("failed to get the thermohygrometer battery gauge: config={config:?}")
        })?
        .set(data.battery_ratio());

    Ok(())
}

/// Sensor data from SwitchBot meter plus.
#[derive(Debug, Clone, Copy)]
struct MeterPlusSensorData {
    /// Remaining battery in percent.
    battery_perc: u8,
    /// Temperature in Celcius degree, multiplied by the scale.
    temperature_x10: i16,
    /// Humidity in percent.
    humidity_perc: u8,
}

impl MeterPlusSensorData {
    #[inline]
    #[must_use]
    pub fn battery_percentage(&self) -> u8 {
        self.battery_perc
    }

    #[inline]
    #[must_use]
    pub fn battery_ratio(&self) -> f64 {
        f64::from(self.battery_percentage()) / 100.0
    }

    #[inline]
    #[must_use]
    pub const fn temperature_scale(&self) -> i16 {
        10
    }

    #[inline]
    #[must_use]
    pub fn temperature_scaled(&self) -> i16 {
        self.temperature_x10
    }

    #[inline]
    #[must_use]
    pub fn temperature_celsius(&self) -> f64 {
        f64::from(self.temperature_scaled()) / f64::from(self.temperature_scale())
    }

    #[inline]
    #[must_use]
    pub fn humidity_percentage(&self) -> u8 {
        self.humidity_perc
    }

    /// Returns the relative humidity between 0.0 to 1.0.
    #[inline]
    #[must_use]
    pub fn humidity_ratio(&self) -> f64 {
        f64::from(self.humidity_percentage()) / 100.0
    }

    /// Returns the volumetric humidity (in `g/(m^3)`).
    ///
    /// See <https://www.hatchability.com/Vaisala.pdf> and
    /// <https://www.aqua-calc.com/calculate/humidity>.
    #[inline]
    #[must_use]
    pub fn volumetric_humidity(&self) -> f64 {
        volumetric_humidity(self.humidity_ratio(), self.temperature_celsius())
    }

    fn from_raw_service_data(
        service_data: &HashMap<Uuid, Vec<u8>>,
    ) -> anyhow::Result<Option<Self>> {
        let raw_data = match service_data.get(&MeterPlusDevice::SERVICE_UUID) {
            Some(v) => v,
            None => return Ok(None),
        };
        let data = Self::from_raw_service_data_entry(raw_data)?;
        Ok(Some(data))
    }

    // See
    // <https://github.com/OpenWonderLabs/SwitchBotAPI-BLE/blob/4ad138bb09f0fbbfa41b152ca327a78c1d0b6ba9/devicetypes/meter.md>.
    fn from_raw_service_data_entry(raw: &[u8]) -> anyhow::Result<Self> {
        let raw: [u8; 6] = raw
            .try_into()
            .map_err(|_| anyhow!("expected 6 bytes of data but got {} bytes", raw.len()))?;
        let battery_perc = raw[2] & 0b_0111_1111;
        let temperature_decimal = raw[3] & 0b_1111;
        if temperature_decimal >= 10 {
            // Something is wrong!
            bail!(
                "decimal part of the temperature should be less than 10 but got {}",
                temperature_decimal
            );
        }
        let is_temperature_positive = (raw[4] & 0b_1000_0000) != 0;
        let temperature_integer = raw[4] & 0b_0111_1111;
        let humidity_perc = raw[5] & 0b_0111_1111;

        let temperature_abs = i16::from(temperature_integer) * 10 + i16::from(temperature_decimal);
        let temperature_x10 = if is_temperature_positive {
            temperature_abs
        } else {
            -temperature_abs
        };
        Ok(Self {
            battery_perc,
            temperature_x10,
            humidity_perc,
        })
    }
}

#[derive(Debug)]
struct MeterPlusDevice {
    /// Device.
    device: bluer::Device,
}

impl MeterPlusDevice {
    /// UUID for the service data of the sensors.
    #[allow(clippy::unusual_byte_groupings)]
    pub const SERVICE_UUID: Uuid = Uuid::from_u128(0x_0000fd3d_0000_1000_8000_00805f9b34fb);

    #[inline]
    pub fn new(device: bluer::Device) -> Self {
        Self { device }
    }

    #[inline]
    #[must_use]
    pub fn device(&self) -> &bluer::Device {
        &self.device
    }

    async fn service_data(&self) -> anyhow::Result<HashMap<Uuid, Vec<u8>>> {
        let service_data: Option<HashMap<Uuid, Vec<u8>>> =
            self.device.service_data().await.with_context(|| {
                format!("failed to fetch service data from the device {self:?}")
            })?;
        if let Some(service_data) = service_data {
            return Ok(service_data);
        }

        tracing::debug!("reconnect to the device {self:?}");
        // Reconnect and try again.
        self.device
            .connect()
            .await
            .with_context(|| format!("failed to connect to the device {self:?}"))?;
        self.device
            .service_data()
            .await
            .with_context(|| format!("failed to fetch service data from the device {self:?}"))?
            .ok_or_else(|| anyhow!("no service data available from device {self:?}"))
    }

    pub async fn data(&self) -> anyhow::Result<MeterPlusSensorData> {
        let service_data = self.service_data().await?;
        let raw_data = service_data
            .get(&MeterPlusDevice::SERVICE_UUID)
            .with_context(|| format!("no sensor data available from device {self:?}"))?;
        let data = MeterPlusSensorData::from_raw_service_data_entry(raw_data)?;
        Ok(data)
    }
}
