//! SwitchBot Plug Mini.
//!
//! About SwitchBot meter plus, see
//! <https://www.switchbot.jp/collections/all/products/switchbot-plug-mini>
//! (2023-02-25).

use std::collections::HashMap;
use std::num::{NonZeroU64, NonZeroU8};
use std::sync::Arc;

use anyhow::{anyhow, bail, Context as _};
use futures::{FutureExt, StreamExt};
use serde::Deserialize;
use uuid::Uuid;

use crate::metrics::smart_plug::{
    INSTANT_CONSUMED_CURRENT_GAUGE, INSTANT_CONSUMED_ENERGY_GAUGE, POWER_ON, VOLTAGE_GAUGE,
};
use crate::Config;

#[derive(Debug, Clone, PartialEq, Eq, Deserialize)]
pub struct SwitchbotPlugMiniConfig {
    /// Bluetooth MAC address of the smart plug.
    #[serde(rename = "bt-mac-addr")]
    bt_mac_addr: bluer::Address,
    /// House.
    house: String,
    /// Name.
    name: String,
    /// Sensing period in seconds.
    update_period: Option<NonZeroU64>,
}

pub async fn watch_retry(config: Arc<Config>) {
    const RETRY_DURATION: std::time::Duration = std::time::Duration::from_secs(3);
    loop {
        match watch(&config.switchbot_plug_mini).await {
            Ok(()) => break,
            Err(e) => {
                tracing::error!("smart plug watcher failed: {e:?}");
                tokio::time::sleep(RETRY_DURATION).await;
            }
        }
    }
}

#[inline]
#[must_use]
fn beginning_of_the_day(since_epoch: u64, offset_from_utc: i32) -> u64 {
    let offset_from_utc = i64::from(offset_from_utc) as u64;
    (since_epoch.wrapping_add(offset_from_utc) / 86400 * 86400).wrapping_sub(offset_from_utc)
}

async fn watch(configs: &[SwitchbotPlugMiniConfig]) -> anyhow::Result<()> {
    let tasks: Vec<_> = configs
        .iter()
        .map(|conf| watch_single_smart_plug(conf.clone()).boxed())
        .collect::<Vec<_>>();
    futures::future::select_all(tasks).await.0
}

// This function does not power on the adapter, so ensuring the adapter being
// powered on is the caller's responsibility.
async fn connect_device(
    adapter: &bluer::Adapter,
    addr: bluer::Address,
    addr_type: bluer::AddressType,
) -> bluer::Result<bluer::Device> {
    tracing::debug!("connecting to the device {addr:?} (address type: {addr_type:?})");
    match adapter.connect_device(addr, addr_type).await {
        Ok(device) => return Ok(device),
        Err(e) => match e.kind {
            bluer::ErrorKind::AlreadyExists => {
                tracing::debug!(
                    "falling back to discovery-and-connect since `Adapter::connect_device()` failed: {e}"
                );
                // Not sure, but maybe already connected?
                // Fallback to the legacy method (discovery and connect).
            }
            bluer::ErrorKind::Internal(bluer::InternalErrorKind::DBus(..)) => {
                // As of bluez-5.66, when `--experimental` is not specified for bluetoothd
                // command, the error below is returned:
                //
                // > internal error: D-Bus error org.freedesktop.DBus.Error.UnknownMethod:
                // > Method "ConnectDevice" with signature "a{sv}" on interface "org.bluez.Adapter1" doesn't exist
                //
                // In this case, fallback to the legacy method (discovery and connect).
                tracing::debug!(
                    "falling back to discovery-and-connect since `Adapter::connect_device()` failed: {e}"
                );
                tracing::debug!(
                    "hint: `Adapter::connect_device()` requires bluetoothd \
                     to start with `--experimental` CLI option"
                );
            }
            _ => return Err(e),
        },
    };

    let mut discover = adapter.discover_devices().await?;
    //pin_mut!(discover);
    while let Some(event) = discover.next().await {
        if let bluer::AdapterEvent::DeviceAdded(ev_addr) = event {
            if ev_addr != addr {
                tracing::trace!("skipping addr = {addr:?}");
                continue;
            }

            let device = adapter.device(addr)?;
            // TODO: check if RSSI is available in order to ensure the device is present.
            tracing::trace!("found device {device:?}");
            tracing::trace!(
                "all properties for device {device:?}: {:?}",
                device.all_properties().await
            );

            device.connect().await?;
            return Ok(device);
        }
    }

    Err(bluer::Error {
        kind: bluer::ErrorKind::DoesNotExist,
        message: format!("the device {addr:?} with address type {addr_type:?} was not found"),
    })
}

async fn watch_single_smart_plug(config: SwitchbotPlugMiniConfig) -> anyhow::Result<()> {
    // No firm reason for this "3" seconds.
    const CONNECT_RETRY_WAIT: std::time::Duration = std::time::Duration::from_secs(3);

    loop {
        // Set up Bluetooth.
        let session = bluer::Session::new().await?;
        let adapter = session.default_adapter().await?;
        adapter.set_powered(true).await?;

        let device = match connect_device(
            &adapter,
            config.bt_mac_addr,
            bluer::AddressType::LePublic,
        )
        .await
        {
            Ok(device) => device,
            Err(e) => match e.kind {
                bluer::ErrorKind::DoesNotExist => {
                    tracing::debug!("device not found. retry connecting...");
                    tokio::time::sleep(CONNECT_RETRY_WAIT).await;
                    continue;
                }
                _ => {
                    return Err(e).with_context(|| {
                        format!("failed to connect to the device {:?}", config.bt_mac_addr)
                    })
                }
            },
        };

        tracing::debug!("device found: {device:?}");
        tracing::trace!("device property: {:#?}", device.all_properties().await);
        if !device
            .is_connected()
            .await
            .with_context(|| format!("failed to get device connection status for {device:?}"))?
        {
            // Maybe disconnected right after connected, due to other session or hardware reasons?
            tracing::error!(
                "`connect_device()` succeeded but the device is already disconnected.\
                 retry connecting..."
            );
            tokio::time::sleep(CONNECT_RETRY_WAIT).await;
            continue;
        }

        let device = PlugMiniDevice::new(device);
        if let Err(e) = watch_single_smart_plug_by_connection(&device, &config).await {
            tracing::error!("failed to watch the smart plug: {e}");
        }

        device
            .device()
            .disconnect()
            .await
            .with_context(|| format!("failed to disconnect to the device {device:?}"))?;
    }
}

// It is caller's responsibility to connect to the device.
async fn watch_single_smart_plug_by_connection(
    device: &PlugMiniDevice,
    config: &SwitchbotPlugMiniConfig,
) -> anyhow::Result<()> {
    // No firm reasons for this "2" seconds.
    const DEFAULT_UPDATE_PERIOD: u64 = 2;

    if !device
        .device()
        .is_connected()
        .await
        .with_context(|| format!("failed to get device connection status for {device:?}"))?
    {
        // This function requires the device to be connected.
        bail!("the device should be connected but not: device={device:?}");
    }
    let update_period = tokio::time::Duration::from_secs(
        config
            .update_period
            .map_or(DEFAULT_UPDATE_PERIOD, NonZeroU64::get),
    );

    let (gatt_tx, gatt_rx) = device
        .gatt_channel()
        .await
        .context("failed to get GATT characteristics")?;

    let notify_io = gatt_rx
        .notify_io()
        .await
        .context("failed to start notify I/O")?;
    loop {
        let next_loop_start = tokio::time::Instant::now() + update_period;

        // Fetch detailed data.
        {
            let unix_time = std::time::UNIX_EPOCH
                .elapsed()
                .context("failed to get the current time")?
                .as_secs();
            let start_time = beginning_of_the_day(unix_time, 9 * 3600);
            let mut command = [0_u8; 12];
            command[0..4].copy_from_slice(&[0x57, 0x0f, 0x51, 0x06]);
            command[4..8].copy_from_slice(&(unix_time as u32).to_be_bytes());
            command[8..12].copy_from_slice(&(start_time as u32).to_be_bytes());
            gatt_tx.write(&command).await.context("failed to write")?;

            let raw = notify_io
                .recv()
                .await
                .context("failed to read GATT characteristic")?;
            let data = PlugMiniDetailedData::from_raw(&raw);
            tracing::debug!("parsed data = {data:?}");
            let data = data.context("failed to get detailed data")?;
            data.set_metrics(config).context("failed to set metrics")?;
        }

        // Fetch power state.
        {
            gatt_tx
                .write(&[0x57, 0x0f, 0x51, 0x01])
                .await
                .context("failed to write")?;

            let raw = notify_io
                .recv()
                .await
                .context("failed to read GATT characteristic")?;
            let data = PlugMiniSwitchStateData::from_raw(&raw);
            tracing::debug!("parsed data = {data:?}");
            let data = data.context("failed to get switch state data")?;
            data.set_metrics(config).context("failed to set metrics")?;
        }

        // Wait before starting the next loop.
        tokio::time::sleep_until(next_loop_start).await;
    }
}

/// Sensor data from SwitchBot meter plus.
// Not used for now.
#[allow(dead_code)]
#[derive(Debug, Clone, Copy)]
struct PlugMiniBasicData {
    packet_seq: NonZeroU8,
    power_is_on: bool,
    delay_is_set: bool,
    timer_is_set: bool,
    utc_time_is_synced: bool,
    wifi_rssi: u8,
    overloaded: bool,
    // Unit is 0.1 Watt. For example, the raw value 42 indicates 4.2 W.
    load_x10: u16,
}

// Not used for now.
#[allow(dead_code)]
impl PlugMiniBasicData {
    fn from_raw_manufacturer_data(
        service_data: &HashMap<u16, Vec<u8>>,
    ) -> anyhow::Result<Option<Self>> {
        let raw_data = match service_data.get(&PlugMiniDevice::MANUFACTURER_ID) {
            Some(v) => v,
            None => return Ok(None),
        };
        let data = Self::from_raw_manufacturer_data_entry(raw_data)?;
        Ok(Some(data))
    }

    // See
    // <https://github.com/OpenWonderLabs/SwitchBotAPI-BLE/blob/0a6cf38cd4d5539962b24440008f87aaa61eebec/devicetypes/plugmini.md#plug-mini-broadcast-message>.
    fn from_raw_manufacturer_data_entry(raw: &[u8]) -> anyhow::Result<Self> {
        let raw: [u8; 12] = raw
            .try_into()
            .map_err(|_| anyhow!("expected 12 bytes of data but got {} bytes", raw.len()))?;
        //let _mac_addr = [raw[0], raw[1], raw[2], raw[3], raw[4], raw[5]];
        let packet_seq = NonZeroU8::new(raw[6]).ok_or_else(|| {
            anyhow!("expected packet sequence number to be non-zero, but got zero")
        })?;
        if ![0x00, 0x80].contains(&raw[7]) {
            // Something is wrong!
            bail!("plug state should be 0x00 or 0x80, but got {:#02?}", raw[7]);
        }
        let power_is_on = raw[7] != 0; // 0x00=power off, 0x80=power on;
        let delay_is_set = (raw[8] & 0x80) != 0;
        let timer_is_set = (raw[8] & 0x40) != 0;
        let utc_time_is_synced = (raw[8] & 0x20) != 0;
        let wifi_rssi = raw[9];
        let overloaded = (raw[10] & 0x80) != 0;
        let load_x10 = (u16::from(raw[10] & 0x7f) << 8) + u16::from(raw[11]);

        Ok(Self {
            packet_seq,
            power_is_on,
            delay_is_set,
            timer_is_set,
            utc_time_is_synced,
            wifi_rssi,
            overloaded,
            load_x10,
        })
    }
}

/// Detailed power data from SwitchBot meter plus.
#[derive(Debug, Clone, Copy)]
struct PlugMiniDetailedData {
    // Not sure how this is useful and how this is intended to be used, since
    // this data is obtained by non-public API with no official documentation.
    #[allow(dead_code)]
    uptime_minutes: u16,
    voltage_x10: u16,
    current_x1000: u16,
    energy_x10: u16,
}

impl PlugMiniDetailedData {
    fn from_raw(raw: &[u8]) -> anyhow::Result<Self> {
        if raw.len() != 15 {
            bail!("expected 15 bytes but got {} bytes", raw.len());
        }
        let uptime_minutes = u16::from_be_bytes([raw[7], raw[8]]);
        let voltage_x10 = u16::from_be_bytes([raw[9], raw[10]]);
        let current_x1000 = u16::from_be_bytes([raw[11], raw[12]]);
        let energy_x10 = u16::from_be_bytes([raw[13], raw[14]]);
        Ok(Self {
            uptime_minutes,
            voltage_x10,
            current_x1000,
            energy_x10,
        })
    }

    #[inline]
    #[must_use]
    fn voltage(&self) -> f64 {
        f64::from(self.voltage_x10) / 10.0
    }

    #[inline]
    #[must_use]
    fn current(&self) -> f64 {
        f64::from(self.current_x1000) / 1000.0
    }

    #[inline]
    #[must_use]
    fn energy(&self) -> f64 {
        f64::from(self.energy_x10) / 10.0
    }

    fn set_metrics(&self, config: &SwitchbotPlugMiniConfig) -> anyhow::Result<()> {
        let labels = &[config.house.as_str(), config.name.as_str()];
        INSTANT_CONSUMED_ENERGY_GAUGE
            .get_metric_with_label_values(labels)
            .with_context(|| {
                format!("failed to get the instant consumed energy gauge: config={config:?}")
            })?
            .set(self.energy());
        INSTANT_CONSUMED_CURRENT_GAUGE
            .get_metric_with_label_values(labels)
            .with_context(|| {
                format!("failed to get the instant consumed current gauge: config={config:?}")
            })?
            .set(self.current());
        VOLTAGE_GAUGE
            .get_metric_with_label_values(labels)
            .with_context(|| format!("failed to get the voltage gauge: config={config:?}"))?
            .set(self.voltage());

        Ok(())
    }
}

/// Switch state data from SwitchBot meter plus.
#[derive(Debug, Clone, Copy)]
struct PlugMiniSwitchStateData {
    power_is_on: bool,
}

impl PlugMiniSwitchStateData {
    fn from_raw(raw: &[u8]) -> anyhow::Result<Self> {
        if raw.len() != 2 {
            bail!("expected 2 bytes but got {} bytes", raw.len());
        }
        if raw[0] != 0x01 {
            bail!(
                "unexpected data: expected raw[0] to be 0x01, but got {:#02x} (raw={raw:02x?})",
                raw[0],
            );
        }

        let power_is_on = raw[1] != 0;
        if ![0x00, 0x80].contains(&raw[1]) {
            tracing::warn!("unexpected switch state data: raw={raw:02x?}");
        }

        Ok(Self { power_is_on })
    }

    fn set_metrics(&self, config: &SwitchbotPlugMiniConfig) -> anyhow::Result<()> {
        let labels = &[config.house.as_str(), config.name.as_str()];
        POWER_ON
            .get_metric_with_label_values(labels)
            .with_context(|| format!("failed to get the power-on gauge: config={config:?}"))?
            .set(self.power_is_on as u8 as f64);

        Ok(())
    }
}

#[derive(Debug)]
struct PlugMiniDevice {
    /// Device.
    device: bluer::Device,
}

impl PlugMiniDevice {
    /// ID for the manufacturer data of the plug.
    // Not used for now.
    #[allow(dead_code)]
    const MANUFACTURER_ID: u16 = 0x0969;
    #[allow(clippy::unusual_byte_groupings)]
    const CHARACTERISTIC_SERVICE: Uuid = Uuid::from_u128(0x_cba20d00_224d_11e6_9fb8_0002a5d5c51b);
    #[allow(clippy::unusual_byte_groupings)]
    const CHARACTERISTIC_SEND: Uuid = Uuid::from_u128(0x_cba20002_224d_11e6_9fb8_0002a5d5c51b);
    #[allow(clippy::unusual_byte_groupings)]
    const CHARACTERISTIC_RECV: Uuid = Uuid::from_u128(0x_cba20003_224d_11e6_9fb8_0002a5d5c51b);

    #[inline]
    #[must_use]
    fn new(device: bluer::Device) -> Self {
        Self { device }
    }

    #[inline]
    #[must_use]
    fn device(&self) -> &bluer::Device {
        &self.device
    }

    // Not used for now.
    #[allow(dead_code)]
    async fn manufacturer_data(&self) -> anyhow::Result<HashMap<u16, Vec<u8>>> {
        let manufacturer_data: Option<HashMap<u16, Vec<u8>>> =
            self.device.manufacturer_data().await.with_context(|| {
                format!("failed to fetch manufacturer data from the device {self:?}")
            })?;
        if let Some(manufacturer_data) = manufacturer_data {
            return Ok(manufacturer_data);
        }

        tracing::debug!("reconnect to the device {self:?}");
        // Reconnect and try again.
        //self.device
        //    .connect()
        //    .await
        //    .with_context(|| format!("failed to connect to the device {self:?}"))?;
        self.device
            .manufacturer_data()
            .await
            .with_context(|| format!("failed to fetch manufacturer data from the device {self:?}"))?
            .with_context(|| format!("no manufacturer data available from device {self:?}"))
    }

    // Not used for now.
    #[allow(dead_code)]
    async fn basic_data(&self) -> anyhow::Result<PlugMiniBasicData> {
        let manufacturer_data = self.manufacturer_data().await?;
        let raw_data = manufacturer_data
            .get(&Self::MANUFACTURER_ID)
            .with_context(|| format!("no power data available from device {self:?}"))?;
        let data = PlugMiniBasicData::from_raw_manufacturer_data_entry(raw_data)?;
        Ok(data)
    }

    /// Returns the sender and receiver.
    async fn gatt_channel(
        &self,
    ) -> anyhow::Result<(
        bluer::gatt::remote::Characteristic,
        bluer::gatt::remote::Characteristic,
    )> {
        let mut service = None;
        for s in self.device.services().await? {
            if s.uuid().await? == Self::CHARACTERISTIC_SERVICE {
                service = Some(s);
                break;
            }
        }
        let Some(service) = service else {
            bail!("service {:?} not found", Self::CHARACTERISTIC_SERVICE)
        };
        tracing::debug!("service found: {service:?}");

        let mut gatt_char_send = None;
        for ch in service.characteristics().await? {
            if ch.uuid().await? == Self::CHARACTERISTIC_SEND {
                gatt_char_send = Some(ch);
                break;
            }
        }
        let Some(gatt_char_send) = gatt_char_send else {
            bail!(
                "GATT characteristic (send) {:?} not found",
                Self::CHARACTERISTIC_SEND
            )
        };
        tracing::debug!("GATT characteristic (send) found: {gatt_char_send:?}");

        let mut gatt_char_recv = None;
        for ch in service.characteristics().await? {
            if ch.uuid().await? == Self::CHARACTERISTIC_RECV {
                gatt_char_recv = Some(ch);
                break;
            }
        }
        let Some(gatt_char_recv) = gatt_char_recv else {
            bail!(
                "GATT characteristic (recv) {:?} not found",
                Self::CHARACTERISTIC_RECV
            )
        };
        tracing::debug!("GATT characteristic (recv) found: {gatt_char_recv:?}");

        Ok((gatt_char_send, gatt_char_recv))
    }
}
