# Specifications

## Formal specs

### SKSTACK, Wi-SUN
* BP35A1 (ROHM Co., Ltd.): `BP35A1 コマンドリファレンスマニュアル (SE 版)` (ja)
    + Maybe only purchaser can access the resource.
* [RL7023 Stick-D/IPS 開発者向け マニュアル](https://www.tessera.co.jp/Download/RL7023_Stick-D_IPS_UM_J2.pdf) (ja)
    + Only purchaser can access the resource.

The USB device that implements Wi-SUN and provides the feature through SKSTACK IP on USB serial connection.
In order to communicate to smart meters, the program should access the USB serial device (usually `/dev/ttyUSB0`), and send (write) SKSTACK commands and receive (read) SKSTACK events from the device.

About SKSTACK IP:

* [Skyley Networks　/ Wi-SUN準拠プロトコル・スタック　SKSTACK IP](http://www.skyley.com/products/skstack_ip.html) (ja)
* [Skyley Networks　/　Bルートやってみた](http://www.skyley.com/products/b-route.html) (ja)

### ECHONET Lite

* [エコーネット規格 | ECHONET](https://echonet.jp/spec_v113_lite/) (ja), [ECHONET Specifications | ECHONET](https://echonet.jp/spec_v113_lite_en/) (en)
    + This defines frame and packet format, and also defines constants where needed.
    + [第2部 ECHONET Lite 通信ミドルウェア仕様](https://echonet.jp/wp/wp-content/uploads/pdf/General/Standard/ECHONET_lite_V1_13_jp/ECHONET-Lite_Ver.1.13_02.pdf) (ja)
    + [Part 2 ECHONET Lite Communications Middleware Specifications](https://echonet.jp/wp/wp-content/uploads/pdf/General/Standard/ECHONET_lite_V1_13_en/ECHONET-Lite_Ver.1.13%2802%29_E.pdf)
* [エコーネット規格 | ECHONET](https://echonet.jp/spec_object_rq/) (ja), [ECHONET Specifications | ECHONET](https://echonet.jp/spec_object_rq_en/) (en)
    + This defines detailed properties specific to device types, including its format and semantics.
    + [APPENDIX ECHONET機器オブジェクト詳細規定Release Q](https://echonet.jp/wp/wp-content/uploads/pdf/General/Standard/Release/Release_Q/Appendix_Release_Q.pdf) (ja)
    + [APPENDIX Detailed Requirements for ECHONET Device objects, Release Q](https://echonet.jp/wp/wp-content/uploads/pdf/General/Standard/Release/Release_Q/Appendix_Release_Q_E.pdf) (en)

This spec defines generic protocol (including data format) to send and receive objects.
Protocol specific to device types (such as smartmeters) are defined based on ECHONET Lite protocol.

### ECHONET Lite, Interface Specification for Application Layer Communication between Smart Electric Energy Meters and HEMS Controllers

* [規格書・仕様書など | ECHONET](https://echonet.jp/spec_g/) (ja), [ECHONET Specifications | ECHONET](https://echonet.jp/spec-en/) (en)
    + [低圧スマート電力量メータ・HEMSコントローラ間](https://echonet.jp/wp/wp-content/uploads/pdf/General/Standard/AIF/lvsm/lvsm_aif_ver1.01.pdf) (ja)
    + [Low-Voltage Smart Electric Energy Meters](https://echonet.jp/wp/wp-content/uploads/pdf/General/Standard/AIF/lvsm/lvsm_aif_ver1.01_e.pdf) (en)

This spec defines protocols specific between HEMS controllers and smart meters.
The spec is defined on ECHONET Lite.

### ECHONET Lite system design guidelines

* [ECHONET Lite システム設計指針 | ECHONET](https://echonet.jp/el_design_guide_2nd/) (ja), [ECHONET Lite System Design Guidelines | ECHONET](https://echonet.jp/el_design_guide_2nd_en/) (en)
    + [ECHONET Liteシステム設計指針 第2版（日本語版)](https://echonet.jp/wp/wp-content/uploads/pdf/General/Standard/design_guideline/ECHONET-Lite%20System%20Design%20Guidelines_2nd%20edition.pdf) (ja)
    + [ECHONET Lite System Design Guidelines 2nd edition](https://echonet.jp/wp/wp-content/uploads/pdf/General/Standard/design_guideline_en/ECHONET-Lite_System_Design_Guidelines_2nd_en.pdf) (en)

This document contains some useful information for HEMS controller implementers.

## Additional (informal) resources

* [SpresenseとBP35A1でスマートメーターと通信する · Hello, (forgotten) world ☂](https://aquarite.info/blog/2022/01/spresense-broute/) (ja)

### My notes
* <https://mastodon.cardina1.red/@lo48576/108806382019380557> (ja)
